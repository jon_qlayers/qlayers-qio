/**************************************************************************************************************************************
Copyright - QLAYERS.B.V

This File is a confidential property of QLayers B.V. Any redistribution or use without prior legal contract will be a legal concern. 

queue_mgmt.c - This file contains the run time logic of Queues and its management between different threads.

Revision - Version x.x - Author
***************************************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ql_qio_error.h"
#include "ql_types.h"
#include "ql_qcode.h"
#include "ql_i2c.h"
#include "ql_qcode_004.h"
#include <i2c_t3.h>
#include <Arduino.h>

static const unsigned short CRC_CCITT_TABLE[256] =
{
    0x0000, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7,
    0x8108, 0x9129, 0xA14A, 0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF,
    0x1231, 0x0210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,
    0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE,
    0x2462, 0x3443, 0x0420, 0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485,
    0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D,
    0x3653, 0x2672, 0x1611, 0x0630, 0x76D7, 0x66F6, 0x5695, 0x46B4,
    0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC,
    0x48C4, 0x58E5, 0x6886, 0x78A7, 0x0840, 0x1861, 0x2802, 0x3823,
    0xC9CC, 0xD9ED, 0xE98E, 0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B,
    0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0x1A71, 0x0A50, 0x3A33, 0x2A12,
    0xDBFD, 0xCBDC, 0xFBBF, 0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A,
    0x6CA6, 0x7C87, 0x4CE4, 0x5CC5, 0x2C22, 0x3C03, 0x0C60, 0x1C41,
    0xEDAE, 0xFD8F, 0xCDEC, 0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49,
    0x7E97, 0x6EB6, 0x5ED5, 0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0x0E70,
    0xFF9F, 0xEFBE, 0xDFDD, 0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,
    0x9188, 0x81A9, 0xB1CA, 0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F,
    0x1080, 0x00A1, 0x30C2, 0x20E3, 0x5004, 0x4025, 0x7046, 0x6067,
    0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E,
    0x02B1, 0x1290, 0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256,
    0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D,
    0x34E2, 0x24C3, 0x14A0, 0x0481, 0x7466, 0x6447, 0x5424, 0x4405,
    0xA7DB, 0xB7FA, 0x8799, 0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C,
    0x26D3, 0x36F2, 0x0691, 0x16B0, 0x6657, 0x7676, 0x4615, 0x5634,
    0xD94C, 0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB,
    0x5844, 0x4865, 0x7806, 0x6827, 0x18C0, 0x08E1, 0x3882, 0x28A3,
    0xCB7D, 0xDB5C, 0xEB3F, 0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A,
    0x4A75, 0x5A54, 0x6A37, 0x7A16, 0x0AF1, 0x1AD0, 0x2AB3, 0x3A92,
    0xFD2E, 0xED0F, 0xDD6C, 0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9,
    0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0x0CC1,
    0xEF1F, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B, 0xBFBA, 0x8FD9, 0x9FF8,
    0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, 0x0ED1, 0x1EF0
};


static unsigned char ql_i2c_uchar_slave_add_space[QL_I2C_ADDRESS_SPACE];
static char ql_i2c_char_slave_add_space[QL_I2C_ADDRESS_SPACE];
static QL_QIO_I2C_CMD_MGMT ql_qio_i2c_cmd_mgmt;
QL_IO_DEVICE_I2C_ADD ql_io_device_i2c_add[256];


STATUS ql_i2c_slave_process_frame(void)
{
      QL_QIO_I2C_CMD_MGMT *_ql_qio_i2c_cmd_mgmt;
      STATUS ql_status = 0;
      ql_status = ql_qio_i2c_get_cmd_mgmt(&_ql_qio_i2c_cmd_mgmt);
      if(ql_status != QL_STATUS_OK)
          return ql_status;
      else
          ;
#ifdef QL_PRINT_ENABLE
      Serial.printf("%d",_ql_qio_i2c_cmd_mgmt->ql_qio_i2c_packet_number);
      Serial.printf("%d\n",_ql_qio_i2c_cmd_mgmt->ql_qio_i2c_cmd );
#endif
}



STATUS ql_qio_i2c_allocate_address(unsigned int ql_qio_block_no, unsigned int ql_qio_i2c_address_type, unsigned int ql_qio_i2c_address_req, void **ql_qio_i2c_address_ret)
{
     
     if((!ql_qio_i2c_address_ret) || (ql_qio_i2c_address_req > 255)) //Change the hardcoding later!Jonathan
          return -10;
     else
          ;
          
     switch(ql_qio_i2c_address_type)
     {
         case QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE:
            if(ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_uchar.ql_i2c_config_blk != 0)
                return -21;
            else
                ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_uchar.ql_i2c_config_blk = ql_qio_block_no;
            *ql_qio_i2c_address_ret = (void*) &ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_uchar.ql_io_device_uc_reg ;
            break;
         case QL_IO_DEVICE_SIGNED_CHAR_ADDRESS_TYPE:
            if(ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_char.ql_i2c_config_blk != 0)
                return -21;
            else
                ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_char.ql_i2c_config_blk = ql_qio_block_no;
            *ql_qio_i2c_address_ret = (void*) &ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_char.ql_io_device_c_reg ;
            break;
         case QL_IO_DEVICE_UNSIGNED_SHORT_ADDRESS_TYPE:
            if(ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_ushort.ql_i2c_config_blk != 0)
                return -21;
            else
                ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_ushort.ql_i2c_config_blk = ql_qio_block_no;
            *ql_qio_i2c_address_ret = (void*) &ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_ushort.ql_io_device_us_reg ;
            break;
         case QL_IO_DEVICE_SIGNED_SHORT_ADDRESS_TYPE:
            if(ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_short.ql_i2c_config_blk != 0)
                return -21;
            else
                ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_short.ql_i2c_config_blk = ql_qio_block_no;
            *ql_qio_i2c_address_ret = (void*) &ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_short.ql_io_device_s_reg ;
            break;
         case QL_IO_DEVICE_UNSIGNED_INT_ADDRESS_TYPE:
            if(ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_uint.ql_i2c_config_blk != 0)
                return -21;
            else
                ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_uint.ql_i2c_config_blk = ql_qio_block_no;
            *ql_qio_i2c_address_ret = (void*) &ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_uint.ql_io_device_ui_reg ;
            break;
         case QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE :
            if(ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_int.ql_i2c_config_blk != 0)
                return -21;
            else
                ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_int.ql_i2c_config_blk = ql_qio_block_no;
            *ql_qio_i2c_address_ret = (void*) &ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_int.ql_io_device_i_reg ;
            break;
         case QL_IO_DEVICE_FLOAT_ADDRESS_TYPE:
             if(ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_float.ql_i2c_config_blk != 0)
                return -21;
            else
                ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_float.ql_i2c_config_blk = ql_qio_block_no;
            *ql_qio_i2c_address_ret = (void*) &ql_io_device_i2c_add[ql_qio_i2c_address_req].ql_io_device_i2c_float.ql_io_device_f_reg ;
            break;
         default:
            Serial.printf("Undefined Address Type!\n");
            return -20;
         
    }
    return QL_STATUS_OK; 
}

STATUS ql_qio_i2c_update_address(unsigned int ql_qio_i2c_address_type, unsigned int ql_qio_i2c_address, void *ql_qio_i2c_update_value)
{
     if((!ql_qio_i2c_update_value) || (ql_qio_i2c_address > 255)) //Change the hardcoding later!Jonathan
          return -10;
     else
          ;
          
     switch(ql_qio_i2c_address_type)
     {
         case QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE:
            ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_uchar.ql_io_device_uc_reg = *(unsigned char*)ql_qio_i2c_update_value;
            break;
         case QL_IO_DEVICE_SIGNED_CHAR_ADDRESS_TYPE:
            ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_char.ql_io_device_c_reg = *(char*)ql_qio_i2c_update_value;
            break;
         case QL_IO_DEVICE_UNSIGNED_SHORT_ADDRESS_TYPE:
            ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_ushort.ql_io_device_us_reg = *(unsigned short*)ql_qio_i2c_update_value ;
            break;
         case QL_IO_DEVICE_SIGNED_SHORT_ADDRESS_TYPE:
            ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_short.ql_io_device_s_reg = *(short*)ql_qio_i2c_update_value ;
            break;
         case QL_IO_DEVICE_UNSIGNED_INT_ADDRESS_TYPE:
            ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_uint.ql_io_device_ui_reg = *(unsigned int*)ql_qio_i2c_update_value ;
            break;
         case QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE :
            ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_int.ql_io_device_i_reg = *(int*)ql_qio_i2c_update_value ;
            break;
         case QL_IO_DEVICE_FLOAT_ADDRESS_TYPE:
            ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_float.ql_io_device_f_reg  = *(float*)ql_qio_i2c_update_value ;
            break;
         default:
            Serial.printf("Undefined Address Type!\n");
            return -20;
    }
    return QL_STATUS_OK; 
}

STATUS ql_i2c_gen_crc_ccitt(unsigned char* ql_data, int data_size, unsigned short* ql_crc)
{
    unsigned short tbl_index = 0;
    unsigned short crc = 0xffff;
    unsigned int i = 0;
    if((!ql_data) || (!ql_crc))
         return -1;
    else
         ;
   
    for (i=0; i < data_size ; i++)
    {
        tbl_index = (crc >> 8) ^ ql_data[i];
        
        crc = (crc << 8) ^ CRC_CCITT_TABLE[tbl_index];
        
    }

    *ql_crc = crc;
    return QL_STATUS_OK;
}
STATUS ql_i2c_get_unsigned_char_i2c_table(unsigned char **ql_slave_i2c_table)
{
    if(!ql_slave_i2c_table)
        return -10;
    else
        ;
    *ql_slave_i2c_table = ql_i2c_uchar_slave_add_space;
    return QL_STATUS_OK;
}
STATUS ql_i2c_get_signed_char_i2c_table(char **ql_slave_i2c_table)
{
    if(!ql_slave_i2c_table)
        return -10;
    else
        ;
    *ql_slave_i2c_table = ql_i2c_char_slave_add_space;
    return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_unsigned_char_write(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
     unsigned char* ql_slave_char_i2c_table;
     STATUS ql_status;
     QL_I2C_CMD* ql_i2c_cmd = NULL;
     unsigned char ql_i2c_data = 0;
     
     ql_i2c_cmd = (QL_I2C_CMD*) ql_i2c_received_data;
     
     ql_i2c_data = *(unsigned char *)&ql_i2c_cmd->ql_i2c_data;

     ql_status = ql_qio_i2c_update_address(QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE, ql_i2c_cmd->ql_i2c_register_address, &ql_i2c_data);
     
     return ql_status;
}

STATUS ql_i2c_slave_process_signed_char_write(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
    
     STATUS ql_status;
     QL_I2C_CMD* ql_i2c_cmd = NULL;
     char ql_i2c_data = 0;
     
     ql_i2c_cmd = (QL_I2C_CMD*) ql_i2c_received_data;
     
     ql_i2c_data = *(char *)&ql_i2c_cmd->ql_i2c_data;

     ql_status = ql_qio_i2c_update_address(QL_IO_DEVICE_SIGNED_CHAR_ADDRESS_TYPE, ql_i2c_cmd->ql_i2c_register_address, &ql_i2c_data);
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_unsigned_short_write(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
    
     STATUS ql_status;
     QL_I2C_CMD* ql_i2c_cmd = NULL;
     unsigned short ql_i2c_data = 0;
     ql_i2c_cmd = (QL_I2C_CMD*) ql_i2c_received_data;     
     ql_i2c_data = *(unsigned short *)&ql_i2c_cmd->ql_i2c_data;
     ql_status = ql_qio_i2c_update_address(QL_IO_DEVICE_UNSIGNED_SHORT_ADDRESS_TYPE, ql_i2c_cmd->ql_i2c_register_address, &ql_i2c_data);
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_signed_short_write(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
     STATUS ql_status;
     QL_I2C_CMD* ql_i2c_cmd = NULL;
     short ql_i2c_data = 0;
     ql_i2c_cmd = (QL_I2C_CMD*) ql_i2c_received_data;
     ql_i2c_data = *(short *)&ql_i2c_cmd->ql_i2c_data;
     ql_status = ql_qio_i2c_update_address(QL_IO_DEVICE_SIGNED_SHORT_ADDRESS_TYPE, ql_i2c_cmd->ql_i2c_register_address, &ql_i2c_data);
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_unsigned_int_write(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
     STATUS ql_status;
     QL_I2C_CMD* ql_i2c_cmd = NULL;
     unsigned int ql_i2c_data = 0;
     ql_i2c_cmd = (QL_I2C_CMD*) ql_i2c_received_data;
     ql_i2c_data = *(unsigned int *)&ql_i2c_cmd->ql_i2c_data;
     ql_status = ql_qio_i2c_update_address(QL_IO_DEVICE_UNSIGNED_INT_ADDRESS_TYPE, ql_i2c_cmd->ql_i2c_register_address, &ql_i2c_data);
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_signed_int_write(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
     STATUS ql_status;
     QL_I2C_CMD* ql_i2c_cmd = NULL;
     int ql_i2c_data = 0;
     ql_i2c_cmd = (QL_I2C_CMD*) ql_i2c_received_data;
     ql_i2c_data = *(int *)&ql_i2c_cmd->ql_i2c_data;
     ql_status = ql_qio_i2c_update_address(QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE, ql_i2c_cmd->ql_i2c_register_address, &ql_i2c_data);
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_float_write(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
     STATUS ql_status;
     QL_I2C_CMD* ql_i2c_cmd = NULL;
     float ql_i2c_data = 0;
     ql_i2c_cmd = (QL_I2C_CMD*) ql_i2c_received_data;
     ql_i2c_data = *(float *)&ql_i2c_cmd->ql_i2c_data;
     Serial.printf("Here:%f\n",ql_i2c_data);
     ql_status = ql_qio_i2c_update_address(QL_IO_DEVICE_FLOAT_ADDRESS_TYPE, ql_i2c_cmd->ql_i2c_register_address, &ql_i2c_data);
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_unsigned_char_read(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_signed_char_read(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_unsigned_short_read(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_signed_short_read(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_unsigned_int_read(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_signed_int_read(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_float_read(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{ 
     return QL_STATUS_OK;
}



STATUS ql_i2c_slave_decode_respond_update_i2c_table(QL_QIO_I2C_CMD_MGMT *_ql_qio_i2c_cmd_mgmt, unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
     unsigned char *ql_slave_i2c_table;    
     QL_I2C_CMD* ql_i2c_cmd = NULL; 
     QL_I2C_POLL* ql_i2c_poll = NULL;
     ql_i2c_cmd = (QL_I2C_CMD*)ql_i2c_received_data; //First we assume it is a command
     ql_i2c_poll = (QL_I2C_POLL*)ql_i2c_received_data;
     unsigned char ql_slave_read_data[4]; // Need to change this!Jonathan
     switch(ql_i2c_cmd->ql_i2c_cmd)
     {
         case QL_I2C_CMD_UNSIGNED_CHAR_REGISTER_WRITE:
              ql_i2c_slave_process_unsigned_char_write(ql_i2c_received_data, ql_i2c_receive_length);
              break;
         case QL_I2C_CMD_SIGNED_CHAR_REGISTER_WRITE:
              ql_i2c_slave_process_signed_char_write(ql_i2c_received_data, ql_i2c_receive_length);
              break;
         case QL_I2C_CMD_UNSIGNED_SHORT_REGISTER_WRITE:
              ql_i2c_slave_process_unsigned_short_write(ql_i2c_received_data, ql_i2c_receive_length);
              break;
         case QL_I2C_CMD_SIGNED_SHORT_REGISTER_WRITE:
              ql_i2c_slave_process_signed_short_write(ql_i2c_received_data, ql_i2c_receive_length);
              break;
         case QL_I2C_CMD_UNSIGNED_INT_REGISTER_WRITE:
              ql_i2c_slave_process_unsigned_int_write(ql_i2c_received_data, ql_i2c_receive_length);
              break;
         case QL_I2C_CMD_SIGNED_INT_REGISTER_WRITE:
              ql_i2c_slave_process_signed_int_write(ql_i2c_received_data, ql_i2c_receive_length);
              break;
         case QL_I2C_CMD_FLOAT_REGISTER_WRITE:
             ql_i2c_slave_process_float_write(ql_i2c_received_data, ql_i2c_receive_length);
             break;
         case QL_I2C_CMD_UNSIGNED_CHAR_REGISTER_READ:
             _ql_qio_i2c_cmd_mgmt->ql_qio_register_address = ql_i2c_poll->ql_i2c_register_address;
             ql_i2c_slave_process_unsigned_char_read(ql_i2c_received_data, ql_i2c_receive_length);
             break;
         case QL_I2C_CMD_SIGNED_CHAR_REGISTER_READ:
              _ql_qio_i2c_cmd_mgmt->ql_qio_register_address = ql_i2c_poll->ql_i2c_register_address;
             ql_i2c_slave_process_signed_char_read(ql_i2c_received_data, ql_i2c_receive_length);
             break;
         case QL_I2C_CMD_UNSIGNED_SHORT_REGISTER_READ:
              _ql_qio_i2c_cmd_mgmt->ql_qio_register_address = ql_i2c_poll->ql_i2c_register_address;
             ql_i2c_slave_process_unsigned_short_read(ql_i2c_received_data, ql_i2c_receive_length);
             break;
         case QL_I2C_CMD_SIGNED_SHORT_REGISTER_READ:
              _ql_qio_i2c_cmd_mgmt->ql_qio_register_address = ql_i2c_poll->ql_i2c_register_address;
             ql_i2c_slave_process_signed_short_read(ql_i2c_received_data, ql_i2c_receive_length);
             break;
         case QL_I2C_CMD_UNSIGNED_INT_REGISTER_READ:
              _ql_qio_i2c_cmd_mgmt->ql_qio_register_address = ql_i2c_poll->ql_i2c_register_address;
             ql_i2c_slave_process_unsigned_int_read(ql_i2c_received_data, ql_i2c_receive_length);
             break;
         case QL_I2C_CMD_SIGNED_INT_REGISTER_READ:
              _ql_qio_i2c_cmd_mgmt->ql_qio_register_address = ql_i2c_poll->ql_i2c_register_address;
             ql_i2c_slave_process_signed_int_read(ql_i2c_received_data, ql_i2c_receive_length);
             break;
         case QL_I2C_CMD_FLOAT_REGISTER_READ:
              _ql_qio_i2c_cmd_mgmt->ql_qio_register_address = ql_i2c_poll->ql_i2c_register_address;
             ql_i2c_slave_process_float_read(ql_i2c_received_data, ql_i2c_receive_length);
             break;
         default:
             return QL_I2C_NON_SUPPORTED_CODE;
             break;
     }
         
     return QL_STATUS_OK;
}

STATUS ql_qio_i2c_get_cmd_mgmt(QL_QIO_I2C_CMD_MGMT **ql_qio_I2C_cmd_mgmt)
{
    if(!ql_qio_I2C_cmd_mgmt)
         return -1;
    else
         ;
    *ql_qio_I2C_cmd_mgmt = &ql_qio_i2c_cmd_mgmt;
    return QL_STATUS_OK;
}

STATUS ql_i2c_slave_process_frame(unsigned char* ql_i2c_received_data, int ql_i2c_receive_length)
{
    QL_I2C_CMD *ql_i2c_cmd;
    unsigned short ql_i2c_crc_received;
    unsigned short* ql_i2c_crc_received_ptr;
    unsigned short ql_i2c_crc_calculated;
    unsigned int i= 0;
    QL_QIO_I2C_CMD_MGMT *_ql_qio_i2c_cmd_mgmt;
    STATUS ql_status;
    ql_i2c_cmd = (QL_I2C_CMD*) ql_i2c_received_data;
      
    memcpy(&ql_i2c_crc_received, (&ql_i2c_received_data[ ql_i2c_receive_length - sizeof(ql_i2c_crc_received)]), sizeof(ql_i2c_crc_received));
    memset(((&ql_i2c_received_data[ ql_i2c_receive_length - sizeof(ql_i2c_crc_received)])), 0x00, sizeof(ql_i2c_crc_received));
      
    ql_i2c_gen_crc_ccitt(ql_i2c_received_data, ql_i2c_receive_length, &ql_i2c_crc_calculated);
    if(ql_i2c_crc_calculated != ql_i2c_crc_received)
        return -1;
    else
        ;

    ql_status = ql_qio_i2c_get_cmd_mgmt(&_ql_qio_i2c_cmd_mgmt);
    if(ql_status != QL_STATUS_OK)
        return ql_status;
    else
        ;
    _ql_qio_i2c_cmd_mgmt->ql_qio_i2c_packet_number = ql_i2c_cmd->ql_packet_number;
    _ql_qio_i2c_cmd_mgmt->ql_qio_i2c_cmd = ql_i2c_cmd->ql_i2c_cmd;
    ql_status = ql_i2c_slave_decode_respond_update_i2c_table(_ql_qio_i2c_cmd_mgmt, ql_i2c_received_data, ql_i2c_receive_length);   
    return ql_status;
}


STATUS ql_qio_i2c_read_slave_register_value(unsigned int ql_qio_i2c_address_type, unsigned int ql_qio_i2c_address, void *ql_qio_i2c_read_value)
{
    if((!ql_qio_i2c_read_value) || (ql_qio_i2c_address > 255)) //Change the hardcoding later!Jonathan
         return -10;
    else
         ;
          
    switch(ql_qio_i2c_address_type)
    {
        case QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE:            
           *(unsigned char*)ql_qio_i2c_read_value = ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_uchar.ql_io_device_uc_reg ;
           break;
        case QL_IO_DEVICE_SIGNED_CHAR_ADDRESS_TYPE:
           *(char*)ql_qio_i2c_read_value = ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_char.ql_io_device_c_reg ;
           break;
        case QL_IO_DEVICE_UNSIGNED_SHORT_ADDRESS_TYPE:

           *(unsigned short*)ql_qio_i2c_read_value = ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_ushort.ql_io_device_us_reg;
           break;
        case QL_IO_DEVICE_SIGNED_SHORT_ADDRESS_TYPE:

           *(short*)ql_qio_i2c_read_value = ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_short.ql_io_device_s_reg ;
           break;
        case QL_IO_DEVICE_UNSIGNED_INT_ADDRESS_TYPE:

           *(unsigned int*)ql_qio_i2c_read_value = ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_uint.ql_io_device_ui_reg;
           break;
        case QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE :

           *(int*)ql_qio_i2c_read_value = ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_int.ql_io_device_i_reg ;
           break;
        case QL_IO_DEVICE_FLOAT_ADDRESS_TYPE:
           *(float*)ql_qio_i2c_read_value = ql_io_device_i2c_add[ql_qio_i2c_address].ql_io_device_i2c_float.ql_io_device_f_reg  ;
           break;
        default:
           Serial.printf("Undefined Address Type!\n");
           return -20;    
    }
    return QL_STATUS_OK; 
}


/*Read Cycle*/
STATUS ql_i2c_slave_read_unsigned_char(void* ql_i2c_read_data, int ql_i2c_base_address, int ql_i2c_read_length)
{
     unsigned char i = 0;
     unsigned char ql_qio_i2c_read_value;
     STATUS ql_status = 0;
     for(i = 0;i<ql_i2c_read_length;i++ )
     {
          ql_status = ql_qio_i2c_read_slave_register_value(QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE,  ql_i2c_base_address + i, &ql_qio_i2c_read_value);   
          memcpy((void *)((unsigned char*) ql_i2c_read_data + i), &ql_qio_i2c_read_value, sizeof(unsigned char) );
     }   
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_read_signed_char(void* ql_i2c_read_data, int ql_i2c_base_address, int ql_i2c_read_length)
{
     unsigned char i = 0;
     char ql_qio_i2c_read_value;
     STATUS ql_status = 0;
     for(i = 0;i<ql_i2c_read_length;i++ )
     {
          ql_status = ql_qio_i2c_read_slave_register_value(QL_IO_DEVICE_SIGNED_CHAR_ADDRESS_TYPE,  ql_i2c_base_address + i, &ql_qio_i2c_read_value);   
          memcpy((void *)((char*) ql_i2c_read_data + i), &ql_qio_i2c_read_value, sizeof(char) );
     } 
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_read_unsigned_short(void* ql_i2c_read_data, int ql_i2c_base_address, int ql_i2c_read_length)
{ 
     unsigned char i = 0;
     unsigned short ql_qio_i2c_read_value;
     STATUS ql_status = 0;
     for(i = 0;i<ql_i2c_read_length;i++ )
     {
          ql_status = ql_qio_i2c_read_slave_register_value(QL_IO_DEVICE_UNSIGNED_SHORT_ADDRESS_TYPE,  ql_i2c_base_address + i, &ql_qio_i2c_read_value);   
          memcpy((void *)((unsigned short*) ql_i2c_read_data + i), &ql_qio_i2c_read_value, sizeof(unsigned short) );
     } 
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_read_signed_short(void* ql_i2c_read_data, int ql_i2c_base_address, int ql_i2c_read_length)
{ 
     unsigned char i = 0;
     short ql_qio_i2c_read_value;
     STATUS ql_status = 0;
     for(i = 0;i<ql_i2c_read_length;i++ )
     {
          ql_status = ql_qio_i2c_read_slave_register_value(QL_IO_DEVICE_SIGNED_SHORT_ADDRESS_TYPE,  ql_i2c_base_address + i, &ql_qio_i2c_read_value);   
          memcpy((void *)((short*) ql_i2c_read_data + i), &ql_qio_i2c_read_value, sizeof(short) );
     } 
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_read_unsigned_int(void* ql_i2c_read_data, int ql_i2c_base_address, int ql_i2c_read_length)
{ 
     unsigned char i = 0;
     unsigned int ql_qio_i2c_read_value;
     STATUS ql_status = 0;
     for(i = 0;i<ql_i2c_read_length;i++ )
     {
          ql_status = ql_qio_i2c_read_slave_register_value(QL_IO_DEVICE_UNSIGNED_INT_ADDRESS_TYPE,  ql_i2c_base_address + i, &ql_qio_i2c_read_value);   
          memcpy((void *)((unsigned int*) ql_i2c_read_data + i), &ql_qio_i2c_read_value, sizeof(unsigned int) );
     } 
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_read_signed_int(void* ql_i2c_read_data, int ql_i2c_base_address, int ql_i2c_read_length)
{ 
     unsigned char i = 0;
     int ql_qio_i2c_read_value;
     STATUS ql_status = 0;
     for(i = 0;i<ql_i2c_read_length;i++ )
     {
          ql_status = ql_qio_i2c_read_slave_register_value(QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE,  ql_i2c_base_address + i, &ql_qio_i2c_read_value);   
          memcpy((void *)((int*) ql_i2c_read_data + i), &ql_qio_i2c_read_value, sizeof(int) );
     }
     return QL_STATUS_OK;
}

STATUS ql_i2c_slave_read_float(void* ql_i2c_read_data, int ql_i2c_base_address, int ql_i2c_read_length)
{  
     unsigned char i = 0;
     float ql_qio_i2c_read_value;
     STATUS ql_status = 0;
     
     for(i = 0;i<ql_i2c_read_length;i++ )
     {
          ql_status = ql_qio_i2c_read_slave_register_value(QL_IO_DEVICE_FLOAT_ADDRESS_TYPE,  ql_i2c_base_address + i, &ql_qio_i2c_read_value);   
          memcpy((void *)((float*) ql_i2c_read_data + i), &ql_qio_i2c_read_value, sizeof(float) );
          //Serial.printf("ql_qio_i2c_read_value:%f, ql_i2c_base_address + i:%d\n",ql_qio_i2c_read_value,ql_i2c_base_address + i);
     }
     return QL_STATUS_OK;
}



STATUS ql_i2c_slave_read_frame(unsigned char* ql_i2c_read_frame, unsigned char* ql_i2c_read_frame_length)
{
      QL_I2C_CMD *ql_i2c_cmd;
      unsigned short ql_i2c_crc_received;
      unsigned short* ql_i2c_crc_received_ptr;
      unsigned short ql_i2c_crc_calculated;
      unsigned int i= 0;
      QL_QIO_I2C_CMD_MGMT *_ql_qio_i2c_cmd_mgmt;
      STATUS ql_status;
      unsigned char ql_qio_i2c_frame_size = 0;
      QL_I2C_DATA *ql_i2c_data;
      unsigned char* a ;
      
      ql_i2c_cmd = (QL_I2C_CMD*) ql_i2c_read_frame;
      ql_i2c_data = (QL_I2C_DATA *)ql_i2c_read_frame;
      ql_status = ql_qio_i2c_get_cmd_mgmt(&_ql_qio_i2c_cmd_mgmt);
      if(ql_status != QL_STATUS_OK)
          return ql_status;
      else
          ;

      switch(_ql_qio_i2c_cmd_mgmt->ql_qio_i2c_cmd)
      {
        
           case QL_I2C_CMD_UNSIGNED_CHAR_REGISTER_READ:
             
              ql_i2c_slave_read_unsigned_char(&ql_i2c_data->ql_i2c_data, _ql_qio_i2c_cmd_mgmt->ql_qio_register_address, 1); //One data point for now! Need to change later to make it dynamic
              ql_qio_i2c_frame_size = sizeof(QL_I2C_DATA) - sizeof(ql_i2c_data->ql_i2c_data) + 1*sizeof(unsigned char);
              break;
           
           case QL_I2C_CMD_UNSIGNED_CHAR_REGISTER_WRITE:
           case QL_I2C_CMD_SIGNED_CHAR_REGISTER_WRITE:
           case QL_I2C_CMD_UNSIGNED_SHORT_REGISTER_WRITE:
           case QL_I2C_CMD_SIGNED_SHORT_REGISTER_WRITE:
           case QL_I2C_CMD_UNSIGNED_INT_REGISTER_WRITE:
           case QL_I2C_CMD_SIGNED_INT_REGISTER_WRITE:
           case QL_I2C_CMD_FLOAT_REGISTER_WRITE:
              
              ql_status = ql_qio_i2c_generate_write_ack_frame(ql_i2c_read_frame, &ql_qio_i2c_frame_size,(unsigned char) _ql_qio_i2c_cmd_mgmt->ql_qio_i2c_packet_number);
             
              break;

           case QL_I2C_CMD_SIGNED_CHAR_REGISTER_READ:
                ql_i2c_slave_read_signed_char(&ql_i2c_data->ql_i2c_data, _ql_qio_i2c_cmd_mgmt->ql_qio_register_address, 1); //One data point for now! Need to change later to make it dynamic
                ql_qio_i2c_frame_size = sizeof(QL_I2C_DATA) - sizeof(ql_i2c_data->ql_i2c_data) + 1*sizeof(char);
                break;
           case QL_I2C_CMD_UNSIGNED_SHORT_REGISTER_READ:
                ql_i2c_slave_read_unsigned_short(&ql_i2c_data->ql_i2c_data, _ql_qio_i2c_cmd_mgmt->ql_qio_register_address, 1); //One data point for now! Need to change later to make it dynamic
                ql_qio_i2c_frame_size = sizeof(QL_I2C_DATA) - sizeof(ql_i2c_data->ql_i2c_data) + 1*sizeof(unsigned short);
              break;
           case QL_I2C_CMD_SIGNED_SHORT_REGISTER_READ:
                ql_i2c_slave_read_signed_short(&ql_i2c_data->ql_i2c_data, _ql_qio_i2c_cmd_mgmt->ql_qio_register_address, 1); //One data point for now! Need to change later to make it dynamic
                ql_qio_i2c_frame_size = sizeof(QL_I2C_DATA) - sizeof(ql_i2c_data->ql_i2c_data) + 1*sizeof(short);
              break;
           case QL_I2C_CMD_UNSIGNED_INT_REGISTER_READ:
                ql_i2c_slave_read_unsigned_int(&ql_i2c_data->ql_i2c_data, _ql_qio_i2c_cmd_mgmt->ql_qio_register_address, 1); //One data point for now! Need to change later to make it dynamic
                ql_qio_i2c_frame_size = sizeof(QL_I2C_DATA) - sizeof(ql_i2c_data->ql_i2c_data) + 1*sizeof(unsigned int);
              break;
           case QL_I2C_CMD_SIGNED_INT_REGISTER_READ:
                ql_i2c_slave_read_signed_int(&ql_i2c_data->ql_i2c_data, _ql_qio_i2c_cmd_mgmt->ql_qio_register_address, 1); //One data point for now! Need to change later to make it dynamic
                ql_qio_i2c_frame_size = sizeof(QL_I2C_DATA) - sizeof(ql_i2c_data->ql_i2c_data) + 1*sizeof(int);
              break;
           case QL_I2C_CMD_FLOAT_REGISTER_READ:
                ql_i2c_slave_read_float(&ql_i2c_data->ql_i2c_data, _ql_qio_i2c_cmd_mgmt->ql_qio_register_address, 1); //One data point for now! Need to change later to make it dynamic
                ql_qio_i2c_frame_size = sizeof(QL_I2C_DATA) - sizeof(ql_i2c_data->ql_i2c_data) + 1*sizeof(float);
              break;
              
           default:
              Serial.printf("Unsupported Mgmt Cmd!\n");
              break;
      }
      switch(_ql_qio_i2c_cmd_mgmt->ql_qio_i2c_cmd)
      {
           case QL_I2C_CMD_UNSIGNED_CHAR_REGISTER_READ:
           case QL_I2C_CMD_SIGNED_CHAR_REGISTER_READ:
           case QL_I2C_CMD_UNSIGNED_SHORT_REGISTER_READ:
           case QL_I2C_CMD_SIGNED_SHORT_REGISTER_READ:
           case QL_I2C_CMD_UNSIGNED_INT_REGISTER_READ:
           case QL_I2C_CMD_SIGNED_INT_REGISTER_READ:
           case QL_I2C_CMD_FLOAT_REGISTER_READ:
               ql_i2c_data->ql_packet_size = ql_qio_i2c_frame_size;
               ql_i2c_data->ql_packet_number = _ql_qio_i2c_cmd_mgmt->ql_qio_i2c_packet_number;
               memset((void*)(((unsigned char*) ql_i2c_data) + ql_qio_i2c_frame_size - sizeof(unsigned short)), 0x00, sizeof(unsigned short));
               ql_i2c_gen_crc_ccitt(ql_i2c_read_frame, ql_qio_i2c_frame_size, &ql_i2c_crc_calculated);
               *(unsigned short*)(((unsigned char*) ql_i2c_data) + ql_qio_i2c_frame_size - sizeof(unsigned short)) = ql_i2c_crc_calculated;
               //Serial.printf("ql_i2c_crc_calculated:%d,ql_qio_i2c_frame_size:%d\n",*(unsigned short*)(((unsigned char*) ql_i2c_data) + ql_qio_i2c_frame_size - sizeof(unsigned short)),ql_qio_i2c_frame_size);
               break;
           
           default:
               break;
      }

      *ql_i2c_read_frame_length = (unsigned int)ql_qio_i2c_frame_size;
      return ql_status;

}

STATUS ql_qio_i2c_generate_write_ack_frame(unsigned char* ql_i2c_read_frame, unsigned char *ql_qio_i2c_frame_size, unsigned char ql_i2c_packet_number)
{
     QL_I2C_ACK *ql_i2c_ack = 0;
     unsigned short ql_i2c_crc_calculated = 0;
     if(!ql_i2c_read_frame)
         return -18;
     else
         ;
     
     ql_i2c_ack = (QL_I2C_ACK *) ql_i2c_read_frame;
     memset(ql_i2c_ack, 0x00, sizeof(QL_I2C_ACK));
     ql_i2c_ack->ql_i2c_status = QL_STATUS_OK; // Need to Change this!!
     ql_i2c_ack->ql_packet_number = ql_i2c_packet_number;
     ql_i2c_ack->ql_i2c_packet_size = sizeof(QL_I2C_ACK);
     ql_i2c_gen_crc_ccitt(ql_i2c_read_frame, sizeof(QL_I2C_ACK), &ql_i2c_crc_calculated);

     ql_i2c_ack->ql_i2c_crc = ql_i2c_crc_calculated;
     *ql_qio_i2c_frame_size = sizeof(QL_I2C_ACK);
     //Serial.printf("ql_i2c_packet_number:%d\n",ql_i2c_packet_number);
     return QL_STATUS_OK;     
}

/***************************I2C********************************************/


void ql_i2c_receive_handle(unsigned int numBytes)
{
    unsigned char ql_i2c_receive[40];
    unsigned char ql_data_index = 0;

    memset(ql_i2c_receive, 0x00, sizeof(ql_i2c_receive));
    while(Wire.available())    // slave may send less than requested
    { 
      
       char c = Wire.read();    // receive a byte as character
       String thisString = String(c, HEX);

       ql_i2c_receive[ql_data_index] = c;
       ql_data_index += 1;
    }
    #if 1
    ql_i2c_slave_process_frame(ql_i2c_receive, numBytes);
    #endif
#ifdef QL_PRINT_ENABLE
    Serial.printf("WD!%d\n",ql_data_index);
#endif
   
}


void ql_init_i2c_env(void)
{
    memset(ql_io_device_i2c_add, 0x00, sizeof(ql_io_device_i2c_add));
    memset(&ql_qio_i2c_cmd_mgmt, 0x00, sizeof(QL_QIO_I2C_CMD_MGMT));
    
}

void requestEvent(void) {
  
    unsigned char ql_i2c_read_frame[256];
    unsigned char ql_data_index = 0;
    unsigned char ql_i2c_read_frame_length;
    unsigned int i = 0;
    
    while(Wire.available())    // slave may send less than requested
    { 
      
       char c = Wire.read();    // receive a byte as character
       String thisString = String(c, HEX);

       ql_i2c_read_frame[ql_data_index] = c;
       ql_data_index += 1;
    }
    
#if 1
    for(i=0;i<ql_data_index;i++)
    {
       char c = ql_i2c_read_frame[i];    // receive a byte as character
       String thisString = String(c, HEX);
       Serial.print("\n");
       Serial.print(thisString);
    }
    memset(ql_i2c_read_frame, 0x00, sizeof(ql_i2c_read_frame));
    ql_i2c_slave_read_frame(ql_i2c_read_frame, &ql_i2c_read_frame_length);
    #endif
       
    for(i=0;i<ql_i2c_read_frame_length;i++)
    {
       char c = ql_i2c_read_frame[i];    // receive a byte as character
       String thisString = String(c, HEX);
#ifdef QL_PRINT_ENABLE
       Serial.print("\n");
       Serial.print(thisString);
#endif
    }

    Wire.write(ql_i2c_read_frame, ql_i2c_read_frame_length);
    
#ifdef QL_PRINT_ENABLE
    Serial.printf("RR%d\n",ql_i2c_read_frame_length);
#endif
}


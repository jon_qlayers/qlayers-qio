
typedef struct ql_qcode_004_DATA {
     unsigned int ql_qcode_output;
     //void* ql_qcode_output;
     void* ql_qcode_data; /*Incase of scratchpad!*/
} QL_QCODE_004_DATA;

typedef struct ql_qcode_004_EXEC_DATA {
     unsigned int ql_qio_i2c_data_type;
     unsigned int ql_qio_mtr_unit;
     void* ql_mtr_rev;
     void* ql_mtr_rev_rate;
     void* ql_mtr_rev_update;
     void* ql_mtr_trigger;
} QL_QCODE_004_EXEC_DATA;

typedef struct ql_io_motor_ctrl_04_config {
  unsigned int ql_i2c_reg_type;
  unsigned int ql_mtr_unit;
  unsigned int ql_mtr_rev;
  unsigned int ql_mtr_rev_rate;
  unsigned int ql_mtr_rev_update;
  unsigned int ql_mtr_trigger;
  
} QL_IO_MOTOR_CTRL_04_CONFIG;

#define QL_MTR_01_STEP_PIN 0x03
#define QL_MTR_01_DIR_PIN  0x04
#define QL_MTR_02_STEP_PIN 0x05
#define QL_MTR_02_DIR_PIN  0x06
STATUS ql_qcode_004_config_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_004_config_size);

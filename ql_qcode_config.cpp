/**************************************************************************************************************************************
Copyright - QLAYERS.B.V

This File is a confidential property of QLayers B.V. Any redistribution or use without prior legal contract will be a legal concern. 

queue_mgmt.c - This file contains the run time logic of Queues and its management between different threads.

Revision - Version x.x - Author
***************************************************************************************************************************************/
#include <stdlib.h>
#include "ql_types.h"
#include "ql_i2c.h"

/*****************************************************************************************

Configuration rules (Current):
a. The input ports are numbered from 0
b. The input to block 050 should be unsigned int.
c. It is necessary to check the data type compatibility of the block connected to a pin, 
   to be the same data type of the block containing the pin.
d. Any improper configuration order can lead to unpredicatable outcome. Firmware to check 
   such conditions is not required to be developed at this point of time

********************************************************************************************/
unsigned int* ql_get_qcode_configuration(void)
{
    unsigned int *block_input_table_stub;
    block_input_table_stub = (unsigned int *)malloc(200*sizeof(unsigned int));
    block_input_table_stub[0] = 1;//Number of blocks totally connected
    
#if 0
    block_input_table_stub[1] = 1;//Block Number
    block_input_table_stub[2] = 2;//Mass Flow Control//Block Type
    block_input_table_stub[3] = QL_IO_DEVICE_FLOAT_ADDRESS_TYPE;//Address Register type
    block_input_table_stub[4] = 1;//Mass Flow Unit
    block_input_table_stub[5] = 2;//Mass Flow Map address
    block_input_table_stub[6] = 1;//Mass Flow Switch Map address
    block_input_table_stub[7] = 23;//Mass Flow Control Pin 
#endif
#if 0
    block_input_table_stub[1] = 1;//Block Number
    block_input_table_stub[2] = 5;//Particle Processing Unit//Block Type
    block_input_table_stub[3] = QL_IO_DEVICE_FLOAT_ADDRESS_TYPE;//Address Register type l_qio_part_proc_reg_type
    block_input_table_stub[4] = 1;//ql_qio_part_proc_unit;
    block_input_table_stub[5] = 3;//ql_qio_particle_size Map Address;
    block_input_table_stub[6] = 4;//ql_qio_part_proc_switch Map Address;
    block_input_table_stub[7] = 16;//ql_qio_part_sensor_pin;
    block_input_table_stub[8] = 20;//ql_qio_part_led_pin;
    block_input_table_stub[9] = 22;//ql_qio_air_flow_ctrl_pin;
    block_input_table_stub[10] = 390;//ql_qio_nozzle_radius;
    block_input_table_stub[11] = 3;//ql_qio_nozzle;
    block_input_table_stub[12] = 15;//ql_qio_min_particle_size;
    block_input_table_stub[13] = 25;//ql_qio_max_particle_size;
    block_input_table_stub[14] = 1620;//ql_qio_paint_density;
    block_input_table_stub[15] = 181;//ql_qio_air_viscosity;
#endif
#if 0
    block_input_table_stub[1] = 1;//Block Number
    block_input_table_stub[2] = 1;//Temperature Control//Block Type
    block_input_table_stub[3] = QL_IO_DEVICE_FLOAT_ADDRESS_TYPE;//Address Register type
    block_input_table_stub[4] = 1;//Temperature Unit
    block_input_table_stub[5] = 5;//Temperature Map address
    block_input_table_stub[6] = 7;//Temperature Feedback address
    block_input_table_stub[7] = 6;//Temperature Switch Map address
    block_input_table_stub[8] = 14;//Thermister Pin 
    block_input_table_stub[9] = 23;//Heater Pin 
#endif



#if 1
    block_input_table_stub[1] = 1;//Block Number
    block_input_table_stub[2] = 3;//Sh Distance Ctrl//Block Type
    block_input_table_stub[3] = QL_IO_DEVICE_FLOAT_ADDRESS_TYPE;//Address Register type
    block_input_table_stub[4] = 1;//Sh Distance Ctrl Unit
    block_input_table_stub[5] = 1;//Sh Distance Ctrl Map address
    block_input_table_stub[6] = 2;//Sh Distance Ctrl Switch Map address
    block_input_table_stub[7] = 14;//Sh Distance Ctrl Control Pin 
    block_input_table_stub[8] = 23;//Sh Dist Ctrl Mtr Dir Control Pin 
    block_input_table_stub[9] = 22;//Sh Dist Ctrl Mtr Step Control Pin  
    block_input_table_stub[10] = 21;//Sh Dist Ctrl Mtr Pwr Control Pin  
#endif
/*
    block_input_table_stub[1] = 1;//Block Number
    block_input_table_stub[2] = 4;//Motor Control//Block Type
    block_input_table_stub[3] = QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE;//Address Register type
    block_input_table_stub[4] = 1;//Motor Unit
    block_input_table_stub[5] = 1;//Motor Revolutions Map address
    block_input_table_stub[6] = 2;//Motor Revolutions Rate Map address
    block_input_table_stub[7] = 100;//Motor Revolutions Update Map address
    block_input_table_stub[8] = 20;//Motor Trigger
    
    
    block_input_table_stub[9] = 2;//Block Number
    block_input_table_stub[10] = 4;//Motor Control//Block Type
    block_input_table_stub[11] = QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE;//Address Register type
    block_input_table_stub[12] = 2;//Motor Unit
    block_input_table_stub[13] = 3;//Motor Revolutions Map address
    block_input_table_stub[14] = 4;//Motor Revolutions Rate Map address
    block_input_table_stub[15] = 101;//Motor Revolutions Update Map address 
    block_input_table_stub[16] = 21;//Motor Trigger


    block_input_table_stub[17] = 3;//Block Number
    block_input_table_stub[18] = 1;//Temperature Control//Block Type
    block_input_table_stub[19] = QL_IO_DEVICE_FLOAT_ADDRESS_TYPE;//Address Register type
    block_input_table_stub[20] = 1;//Temperature Unit
    block_input_table_stub[21] = 1;//Temperature Map address
    block_input_table_stub[22] = 5;//Temperature Switch Map address
    block_input_table_stub[23] = 1;//Temperature Control Pin 

    
    block_input_table_stub[24] = 4;//Block Number
    block_input_table_stub[25] = 2;//Mass Flow Control//Block Type
    block_input_table_stub[26] = QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE;//Address Register type
    block_input_table_stub[27] = 1;//Mass Flow Unit
    block_input_table_stub[28] = 2;//Mass Flow Map address
    block_input_table_stub[29] = 6;//Mass Flow Switch Map address
    block_input_table_stub[30] = 2;//Mass Flow Control Pin 

    block_input_table_stub[31] = 5;//Block Number
    block_input_table_stub[32] = 2;//Mass Flow Control//Block Type
    block_input_table_stub[33] = QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE;//Address Register type
    block_input_table_stub[34] = 2;//Mass Flow Unit
    block_input_table_stub[35] = 3;//Mass Flow Map address
    block_input_table_stub[36] = 7;//Mass Flow Switch Map address
    block_input_table_stub[37] = 10;//Mass Flow Control Pin 
   */
    /*
    block_input_table_stub[38] = 6;//Block Number
    block_input_table_stub[39] = 3;//Sh Distance Ctrl//Block Type
    block_input_table_stub[40] = QL_IO_DEVICE_FLOAT_ADDRESS_TYPE;//Address Register type
    block_input_table_stub[41] = 1;//Sh Distance Ctrl Unit
    block_input_table_stub[42] = 4;//Sh Distance Ctrl Map address
    block_input_table_stub[43] = 8;//Sh Distance Ctrl Switch Map address
    block_input_table_stub[44] = 14;//Sh Distance Ctrl Control Pin 
    block_input_table_stub[45] = 11;//Sh Dist Ctrl Mtr Dir Control Pin 
    block_input_table_stub[46] = 12;//Sh Dist Ctrl Mtr Step Control Pin  
    block_input_table_stub[47] = 3;//Sh Dist Ctrl Mtr Pwr Control Pin  

    */
 
    /*
     * typedef struct ql_io_temperature_ctrl_01_config {
          unsigned int ql_qio_temp_reg_type;
          unsigned int ql_qio_temp_unit;
          unsigned int ql_qio_temperature;
          unsigned int ql_qio_temp_switch;
          unsigned int ql_qio_temp_pin;
        } QL_IO_TEMP_CTRL_01_CONFIG;
    */
    //return block_input_table_stub;
    return block_input_table_stub;
}

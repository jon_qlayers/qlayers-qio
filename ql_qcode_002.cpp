/**************************************************************************************************************************************
Copyright - QLAYERS.B.V

This File is a confidential property of QLayers B.V. Any redistribution or use without prior legal contract will be a legal concern. 

queue_mgmt.c - This file contains the run time logic of Queues and its management between different threads.

Revision - Version x.x - Author
***************************************************************************************************************************************/
#include <SPI.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ql_qio_error.h"
#include "ql_types.h"
#include "ql_qcode.h"
#include "ql_i2c.h"
#include "ql_qcode_002.h"
//#include <i2c_t3.h>
#include <Arduino.h>
//#include <HardwareSerial.h>
//#include "ql_debug.h"


extern STATUS ql_qio_i2c_allocate_address(unsigned int ql_qio_block_no, unsigned int ql_qio_i2c_address_type, unsigned int ql_qio_i2c_address_req, void **ql_qio_i2c_address_ret);
//extern "C" {
extern void digitalPotWrite(int address, int value);
//}
/*Mass Flow Control*/



STATUS ql_qcode_002_parse_config(unsigned int *parse_next_block, unsigned int** parse_next_block_add, QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit)
{
     unsigned int i = 0, j = 0;
     unsigned int ql_data_index = 0;
     STATUS status = QL_STATUS_OK;
     unsigned int ql_qio_block_no; //device type
     unsigned int ql_qio_block_type; //functionality
     
     unsigned int ql_i2c_reg_type;
     unsigned int ql_qio_mass_flow_reg_type;
     unsigned int ql_qio_mass_flow;
     unsigned int ql_qio_mass_flow_switch;
     void* ql_i2c_data_address;
     unsigned int ql_qio_mass_flow_unit = 0;
     unsigned int ql_qio_mass_flow_pin = 0;
     QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit_002 = NULL;
     
     QL_QCODE_002_EXEC_DATA *ql_qio_002_exec_data;
    
     QL_IO_MASS_FLOW_CTRL_02_CONFIG* ql_io_mass_flow_ctrl_02_config;
     QL_IO_DEVICE_CONFIG_BLK* ql_io_device_config_blk;

     unsigned int ql_qcode_config_dynm_size = 0; 

     ql_qio_002_exec_data = (QL_QCODE_002_EXEC_DATA*) ql_qcode_exec_unit->ql_qcode_exec_data;
     ql_io_device_config_blk = (QL_IO_DEVICE_CONFIG_BLK*) parse_next_block;

     
     ql_io_mass_flow_ctrl_02_config = (QL_IO_MASS_FLOW_CTRL_02_CONFIG *) &ql_io_device_config_blk->ql_qio_device;
     ql_qio_mass_flow_reg_type = ql_io_mass_flow_ctrl_02_config->ql_qio_mass_flow_reg_type;
     ql_qio_mass_flow = ql_io_mass_flow_ctrl_02_config->ql_qio_mass_flow;
     ql_qio_mass_flow_switch = ql_io_mass_flow_ctrl_02_config->ql_qio_mass_flow_switch;
     ql_qio_mass_flow_pin = ql_io_mass_flow_ctrl_02_config->ql_qio_mass_flow_pin;
     ql_qio_mass_flow_unit = ql_io_mass_flow_ctrl_02_config->ql_qio_mass_flow_unit;
     ql_qio_002_exec_data->ql_qio_mass_flow_reg_type = ql_qio_mass_flow_reg_type;
     
     switch (ql_qio_mass_flow_reg_type)
     {
            case QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE:
            case QL_IO_DEVICE_SIGNED_CHAR_ADDRESS_TYPE:
            case QL_IO_DEVICE_UNSIGNED_SHORT_ADDRESS_TYPE:
            case QL_IO_DEVICE_SIGNED_SHORT_ADDRESS_TYPE:
            case QL_IO_DEVICE_UNSIGNED_INT_ADDRESS_TYPE:
            case QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE:
            case QL_IO_DEVICE_FLOAT_ADDRESS_TYPE:
                 break;
            default :
#ifdef QL_PRINT_ENABLE 
                 Serial.printf("qblock_data_type:%d\n", ql_qio_mass_flow_reg_type);
#endif
                 return QL_QCODE_INVALID_DATA_TYPE;
     }

     /*Allocate the memory for the i2c data interface*/
     //Serial.printf("ql_qio_mass_flow_reg_type%d\n",ql_qio_mass_flow);
     status = ql_qio_i2c_allocate_address(ql_io_device_config_blk->ql_qio_block_no, ql_qio_mass_flow_reg_type,ql_qio_mass_flow, &ql_i2c_data_address); 
     if(status != QL_STATUS_OK) 
         return status;
     else
         ;

     ql_qio_002_exec_data->ql_qio_mass_flow_unit = ql_qio_mass_flow_unit;
     ql_qio_002_exec_data->ql_qio_mass_flow = ql_i2c_data_address;
     //Cant use an OR on the status because the error codes are not one hot encoding!
     status = ql_qio_i2c_allocate_address(ql_io_device_config_blk->ql_qio_block_no ,QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE,ql_qio_mass_flow_switch, &ql_i2c_data_address); 
     if(status != QL_STATUS_OK)
         return status;
     else
         ;
     ql_qio_002_exec_data->ql_qio_mass_flow_switch = (unsigned char *) ql_i2c_data_address;
     
     /*Pins*/
     ql_qio_002_exec_data->ql_qio_mass_flow_pin = ql_qio_mass_flow_pin;
     pinMode(23,OUTPUT);
     pinMode(22,OUTPUT);
     pinMode(21,OUTPUT);
     pinMode(20,OUTPUT);
     
     digitalWrite(23,HIGH);
     digitalWrite(21,HIGH);
     digitalWrite(20,HIGH);
     
     digitalWrite(ql_qio_002_exec_data->ql_qio_mass_flow_pin,HIGH); //Need to change this later!
     SPI.begin();
     int channel = 2;
     int level= 1;
     digitalPotWrite(channel, level);

     
     *parse_next_block_add = parse_next_block + (sizeof(QL_IO_MASS_FLOW_CTRL_02_CONFIG)/(sizeof(unsigned int))) + (sizeof(QL_IO_DEVICE_CONFIG_BLK) - sizeof(ql_io_device_config_blk->ql_qio_device))/(sizeof(unsigned int));

     return QL_STATUS_OK;
}

STATUS ql_qcode_002_allocate_exec_data_mem(void** ql_qcode_002_data_mem)
{
     void *ql_qcode_data_mem = NULL;
     if(!ql_qcode_002_data_mem)
          return QL_ARG_ERROR;
     else
          ;
     
     ql_qcode_data_mem = malloc(sizeof(QL_QCODE_002_EXEC_DATA));
     if(! ql_qcode_data_mem)
          return -1;
     else
          ;
     memset(ql_qcode_data_mem, 0x00, sizeof(QL_QCODE_002_EXEC_DATA));
     *ql_qcode_002_data_mem = ql_qcode_data_mem;
     return QL_STATUS_OK;
}

/*Float is not supported at the moment!*/



/*Switch is always of type Char*/

STATUS  ql_qcode_002_execute_unit(QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit)
{
    
    float ql_qio_mass_flow;
    unsigned int ql_qio_mass_flow_switch;

    
    QL_QCODE_002_EXEC_DATA *ql_qio_002_exec_data;
    
    if(!ql_qcode_exec_unit)
         return -2;
    else
         ;
    
    ql_qio_002_exec_data = (QL_QCODE_002_EXEC_DATA*) ql_qcode_exec_unit->ql_qcode_exec_data;
    ql_qio_mass_flow_switch = (int) *((unsigned char *)ql_qio_002_exec_data->ql_qio_mass_flow_switch);
    switch(ql_qio_002_exec_data->ql_qio_mass_flow_reg_type)
    {
         case QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE:
                ql_qio_mass_flow = (float) *((unsigned char *)ql_qio_002_exec_data->ql_qio_mass_flow);
                Serial.printf("Here:%d",(int)*((unsigned char *)ql_qio_002_exec_data->ql_qio_mass_flow));
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_002_exec_data->ql_qio_mass_flow = 0;
                     //Serial.printf("ql_qio_002_exec_data->ql_qio_mass_flow:%d\n",*(unsigned char*)ql_qio_002_exec_data->ql_qio_mass_flow);
                     *((unsigned char *)ql_qio_002_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_SIGNED_CHAR_ADDRESS_TYPE:
                ql_qio_mass_flow = *((char *)ql_qio_002_exec_data->ql_qio_mass_flow);
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(char *)ql_qio_002_exec_data->ql_qio_mass_flow = 0;
                     *((unsigned char *)ql_qio_002_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_UNSIGNED_SHORT_ADDRESS_TYPE:
                ql_qio_mass_flow = *((unsigned short *)ql_qio_002_exec_data->ql_qio_mass_flow);
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(unsigned short *)ql_qio_002_exec_data->ql_qio_mass_flow = 0;
                     *((unsigned char *)ql_qio_002_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_SIGNED_SHORT_ADDRESS_TYPE:
                ql_qio_mass_flow = *((short *)ql_qio_002_exec_data->ql_qio_mass_flow);
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(short *)ql_qio_002_exec_data->ql_qio_mass_flow = 0;
                     *((unsigned char *)ql_qio_002_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_UNSIGNED_INT_ADDRESS_TYPE:
                ql_qio_mass_flow = *((unsigned int *)ql_qio_002_exec_data->ql_qio_mass_flow);
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(unsigned int *)ql_qio_002_exec_data->ql_qio_mass_flow = 0;
                     *((unsigned char *)ql_qio_002_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE:
                ql_qio_mass_flow = *((int *)ql_qio_002_exec_data->ql_qio_mass_flow);
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(int *)ql_qio_002_exec_data->ql_qio_mass_flow = 0;
                     *((unsigned char *)ql_qio_002_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_FLOAT_ADDRESS_TYPE: 
                ql_qio_mass_flow =  *((float *)ql_qio_002_exec_data->ql_qio_mass_flow);
                //Serial.printf("Here:%f\n", *(float*)ql_qio_002_exec_data->ql_qio_mass_flow);
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(float *)ql_qio_002_exec_data->ql_qio_mass_flow = 0;
                     *((unsigned char *)ql_qio_002_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;            
         default :
             
             return -30;
    }
    /*We dont expect the rev to be too much for now (1st version)*/
#ifdef QL_PRINT_ENABLE
    Serial.printf("ql_qio_mass_flow_unit:%d, ql_qio_mass_flow_switch:%d, ql_qio_mass_flow:%f! ",ql_qio_002_exec_data->ql_qio_mass_flow_unit, ql_qio_mass_flow_switch,ql_qio_mass_flow);
#endif
    if(ql_qio_mass_flow_switch == QL_QIO_MASS_FLOW_MODULE_ON)
    {
#ifdef QL_PRINT_ENABLE
        Serial.printf("Performing Mass Flow Control\n");
#endif
        //Serial.printf("Pin:%d:Val:%d\n",ql_qio_002_exec_data->ql_qio_mass_flow_pin, (int)*(float *)ql_qio_002_exec_data->ql_qio_mass_flow);
        analogWrite(ql_qio_002_exec_data->ql_qio_mass_flow_pin, (int)*(float *)ql_qio_002_exec_data->ql_qio_mass_flow);
          
#ifdef QL_PRINT_ENABLE
       Serial.printf("Completed!\n");
#endif
    }
#ifdef QL_PRINT_ENABLE
    Serial.printf("Code 02 Executed!\n");
#endif
    return QL_STATUS_OK;
}
STATUS ql_qcode_002_config_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_002_config_size)
{
     unsigned int qblock_inputs = 0;
     unsigned int qblock_outputs = 0;
     unsigned int qblock_number = 0;
     unsigned int qblock_parameters = 0;
     unsigned int qblock_inputs_config_size = 0;
     unsigned int qblock_config_param_size = 0;
     QL_QCODE_CONFIG_BLOCK *ql_qcode_config_block = NULL;
     if((!ql_qcode_config) || (!ql_qcode_002_config_size))
           return QL_ARG_ERROR;
     else
           ;
     *ql_qcode_002_config_size = sizeof(QL_IO_MASS_FLOW_CTRL_02_CONFIG);
     
     return QL_STATUS_OK;
}


typedef struct ql_qcode_002_DATA {
     unsigned int ql_qcode_output;
     void* ql_qcode_data; /*Incase of scratchpad!*/
} QL_QCODE_002_DATA;

typedef struct ql_qcode_002_EXEC_DATA {
     
     unsigned int ql_qio_mass_flow_reg_type;
     unsigned int ql_qio_mass_flow_unit;
     void* ql_qio_mass_flow;
     unsigned char* ql_qio_mass_flow_switch;  /*On and Off*/
     unsigned int ql_qio_mass_flow_pin; /*ADC Pin*/
} QL_QCODE_002_EXEC_DATA;

typedef struct ql_io_mass_flow_ctrl_02_config {
  unsigned int ql_qio_mass_flow_reg_type;
  unsigned int ql_qio_mass_flow_unit;
  unsigned int ql_qio_mass_flow;
  unsigned int ql_qio_mass_flow_switch;
  unsigned int ql_qio_mass_flow_pin;
} QL_IO_MASS_FLOW_CTRL_02_CONFIG;


#define QL_QIO_MASS_FLOW_MODULE_OFF   0x00
#define QL_QIO_MASS_FLOW_MODULE_ON   0x01


#define QL_QIO_TEMP_SAMPLES   5 

typedef struct ql_qcode_001_DATA {
     unsigned int ql_qcode_output;
     void* ql_qcode_data; /*Incase of scratchpad!*/
} QL_QCODE_001_DATA;



typedef struct ql_qcode_001_EXEC_DATA {
     
     unsigned int ql_qio_temp_reg_type;
     unsigned int ql_qio_temp_unit;
     void* ql_qio_temp_set;
     void* ql_qio_temp_fb;
     unsigned char* ql_qio_temp_switch;  /*On and Off*/
     unsigned short ql_qio_temp_thermistor_pin; /*ADC Pin*/
     unsigned short ql_qio_temp_heater_pin;
     int ql_qio_Tc;
     unsigned short ql_qio_temp_samples[QL_QIO_TEMP_SAMPLES];
     
     
} QL_QCODE_001_EXEC_DATA;

typedef struct ql_io_temperature_ctrl_01_config {
  unsigned int ql_qio_temp_reg_type;
  unsigned int ql_qio_temp_unit;
  unsigned int ql_qio_temp_set;
  unsigned int ql_qio_temp_fb;
  unsigned int ql_qio_temp_switch;
  unsigned int ql_qio_temp_thermistor_pin;
  unsigned int ql_qio_temp_heater_pin;
} QL_IO_TEMP_CTRL_01_CONFIG;


#define THERMISTORPIN 14        
    // resistance at 25 degrees C
#define THERMISTORNOMINAL 112000      
    // temp. for nominal resistance (almost always 25 C)
#define TEMPERATURENOMINAL 21   
    // how many samples to take and average, more takes longer
    // but is more 'smooth'
#define NUMSAMPLES 5
    // The beta coefficient of the thermistor (usually 3000-4000)
#define BCOEFFICIENT 1500
    // the value of the 'other' resistor
#define SERIESRESISTOR 99000   



#define QL_QIO_TEMPERATURE_MODULE_OFF   0x00
#define QL_QIO_TEMPERATURE_MODULE_ON   0x01

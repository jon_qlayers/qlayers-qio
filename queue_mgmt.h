

#define QL_DEFAULT_QUEUE_BLOCKS  10
#define QL_DEFAULT_QUEUE_DATA_SIZE   32  

typedef struct queue_block {
     struct queue_block* queue_next_block;
     unsigned char queue_mem_buff[QL_DEFAULT_QUEUE_DATA_SIZE];
} QUEUE_BLOCK;


typedef struct queue_status {
     unsigned int queue_blocks_pool;
     unsigned int queue_allocated;
     unsigned int queue_mgmt_error_status;
} QUEUE_STATUS;

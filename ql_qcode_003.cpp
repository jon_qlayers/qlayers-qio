/**************************************************************************************************************************************
Copyright - QLAYERS.B.V

This File is a confidential property of QLayers B.V. Any redistribution or use without prior legal contract will be a legal concern. 

queue_mgmt.c - This file contains the run time logic of Queues and its management between different threads.

Revision - Version x.x - Author
***************************************************************************************************************************************/

#include "ql_qio_error.h"
#include "ql_types.h"
#include "ql_qcode.h"
#include "ql_i2c.h"
#include "ql_qcode_003.h"
//#include <i2c_t3.h>
#include <Arduino.h>
//#include <HardwareSerial.h>
//#include "ql_debug.h"

#if 1


extern STATUS ql_init_qcode_timer(void);
extern STATUS ql_add_timer_exec_list(void* ql_qtimer_exec_qfunc, void* ql_qtimer_qfunc_arg,unsigned int ql_qtimer_interval/*, QTIMER_BLOCK** ql_qtimer_exec_list*/);
extern STATUS ql_qio_i2c_allocate_address(unsigned int ql_qio_block_no, unsigned int ql_qio_i2c_address_type, unsigned int ql_qio_i2c_address_req, void **ql_qio_i2c_address_ret);


void ql_qcode_003_mtr_cntrl_routine(void *ql_qio_003_mtr_ctrl_arg);

/*Distance Control*/

STATUS ql_qcode_003_parse_config(unsigned int *parse_next_block, unsigned int** parse_next_block_add, QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit)
{
     unsigned int i = 0, j = 0;
     unsigned int ql_data_index = 0;
     STATUS status = QL_STATUS_OK;
     unsigned int ql_qio_block_no; //device type
     unsigned int ql_qio_block_type; //functionality
     
     unsigned int ql_i2c_reg_type;
     unsigned int ql_qio_shld_dist_ctrl_reg_type;
     unsigned int ql_qio_shld_distance;
     unsigned int ql_qio_shld_dist_ctrl_switch;
     void* ql_i2c_data_address;
     unsigned int ql_qio_shld_dist_ctrl_unit = 0;
     //unsigned int ql_qio_shld_dist_ctrl_pin = 0;
     QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit_003 = NULL;
     
     QL_QCODE_003_EXEC_DATA *ql_qio_003_exec_data;
    
     QL_IO_SHLD_DISTANCE_CTRL_03_CONFIG* ql_io_shld_dist_ctrl_03_config;
     QL_IO_DEVICE_CONFIG_BLK* ql_io_device_config_blk;

     unsigned int ql_qcode_config_dynm_size = 0; 

     ql_qio_003_exec_data = (QL_QCODE_003_EXEC_DATA*) ql_qcode_exec_unit->ql_qcode_exec_data;
     ql_io_device_config_blk = (QL_IO_DEVICE_CONFIG_BLK*) parse_next_block;

     
     ql_io_shld_dist_ctrl_03_config = (QL_IO_SHLD_DISTANCE_CTRL_03_CONFIG *) &ql_io_device_config_blk->ql_qio_device;
     ql_qio_shld_dist_ctrl_reg_type = ql_io_shld_dist_ctrl_03_config->ql_qio_shld_dist_ctrl_reg_type;
     ql_qio_shld_distance = ql_io_shld_dist_ctrl_03_config->ql_qio_shld_distance;
     ql_qio_shld_dist_ctrl_switch = ql_io_shld_dist_ctrl_03_config->ql_qio_shld_dist_ctrl_switch;
     //ql_qio_shld_dist_ctrl_pin = ql_io_shld_dist_ctrl_03_config->ql_qio_shld_dist_ctrl_pin;
     ql_qio_shld_dist_ctrl_unit = ql_io_shld_dist_ctrl_03_config->ql_qio_shld_dist_ctrl_unit;
     ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_reg_type = ql_qio_shld_dist_ctrl_reg_type;
     /*Pins*/
     ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_pin = ql_io_shld_dist_ctrl_03_config->ql_qio_shld_dist_ctrl_pin;
     ql_qio_003_exec_data->ql_qio_shld_dist_mtr_dir_pin = ql_io_shld_dist_ctrl_03_config->ql_qio_shld_dist_mtr_dir_pin;
     ql_qio_003_exec_data->ql_qio_shld_dist_mtr_step_pin = ql_io_shld_dist_ctrl_03_config->ql_qio_shld_dist_mtr_step_pin;
     ql_qio_003_exec_data->ql_qio_shld_dist_mtr_pwr_pin = ql_io_shld_dist_ctrl_03_config->ql_qio_shld_dist_mtr_pwr_pin;
     //Serial.printf("ql_qio_003_exec_data->ql_qio_shld_dist_mtr_step_pin:%d\n",ql_qio_003_exec_data->ql_qio_shld_dist_mtr_step_pin);
          
     switch (ql_qio_shld_dist_ctrl_reg_type)
     {
            case QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE:
            case QL_IO_DEVICE_SIGNED_CHAR_ADDRESS_TYPE:
            case QL_IO_DEVICE_UNSIGNED_SHORT_ADDRESS_TYPE:
            case QL_IO_DEVICE_SIGNED_SHORT_ADDRESS_TYPE:
            case QL_IO_DEVICE_UNSIGNED_INT_ADDRESS_TYPE:
            case QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE:
            case QL_IO_DEVICE_FLOAT_ADDRESS_TYPE:
                 break;
            default :
#ifdef QL_PRINT_ENABLE
                 Serial.printf("qblock_data_type:%d\n", ql_qio_shld_dist_ctrl_reg_type);
#endif
                 return QL_QCODE_INVALID_DATA_TYPE;
     }

     /*Allocate the memory for the i2c data interface*/
     status = ql_qio_i2c_allocate_address(ql_io_device_config_blk->ql_qio_block_no, ql_qio_shld_dist_ctrl_reg_type,ql_qio_shld_distance, &ql_i2c_data_address); 
     if(status != QL_STATUS_OK) 
         return status;
     else
         ;

     ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_unit = ql_qio_shld_dist_ctrl_unit;
     ql_qio_003_exec_data->ql_qio_shld_distance = ql_i2c_data_address;
     //Cant use an OR on the status because the error codes are not one hot encoding!
     status = ql_qio_i2c_allocate_address(ql_io_device_config_blk->ql_qio_block_no ,QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE,ql_qio_shld_dist_ctrl_switch, &ql_i2c_data_address); 
     if(status != QL_STATUS_OK)
         return status;
     else
         ;
     ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_switch = (unsigned char *) ql_i2c_data_address;
     
     ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_state = QL_DIST_CTRL_RESET;
     ql_qio_003_exec_data->ql_qio_qcode_003_timer_state = QL_QTIMER_QCODE_RESET;
     *parse_next_block_add = parse_next_block + (sizeof(QL_IO_SHLD_DISTANCE_CTRL_03_CONFIG)/(sizeof(unsigned int))) + (sizeof(QL_IO_DEVICE_CONFIG_BLK) - sizeof(ql_io_device_config_blk->ql_qio_device))/(sizeof(unsigned int));

     return QL_STATUS_OK;
}

STATUS ql_qcode_003_allocate_exec_data_mem(void** ql_qcode_003_data_mem)
{
     void *ql_qcode_data_mem = NULL;
     if(!ql_qcode_003_data_mem)
          return QL_ARG_ERROR;
     else
          ;
     
     ql_qcode_data_mem = malloc(sizeof(QL_QCODE_003_EXEC_DATA));
     if(! ql_qcode_data_mem)
          return -1;
     else
          ;
     memset(ql_qcode_data_mem, 0x00, sizeof(QL_QCODE_003_EXEC_DATA));
     *ql_qcode_003_data_mem = ql_qcode_data_mem;
     return QL_STATUS_OK;
}

/*Float is not supported at the moment!*/



/*Switch is always of type Char*/

STATUS  ql_qcode_003_execute_unit(QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit)
{
    
    float ql_qio_shld_distance;
    float ql_shld_dist_readout;
    unsigned int ql_qio_shld_dist_ctrl_switch;
    int ql_shld_dist = 0;    
    QL_QCODE_003_EXEC_DATA *ql_qio_003_exec_data;
    STATUS ql_status;
    if(!ql_qcode_exec_unit)
         return -2;
    else
         ;
    
    ql_qio_003_exec_data = (QL_QCODE_003_EXEC_DATA*) ql_qcode_exec_unit->ql_qcode_exec_data;
    ql_qio_shld_dist_ctrl_switch = (int) *((unsigned char *)ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_switch);
    switch(ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_reg_type)
    {
         case QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE:
                ql_qio_shld_distance = (float) *((unsigned char *)ql_qio_003_exec_data->ql_qio_shld_distance);
                if(ql_qio_shld_dist_ctrl_switch != QL_QIO_SHLD_DISTANCE_CTRL_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_003_exec_data->ql_qio_shld_distance = 0;
                     *((unsigned char *)ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_SIGNED_CHAR_ADDRESS_TYPE:
                ql_qio_shld_distance = (float) *((char *)ql_qio_003_exec_data->ql_qio_shld_distance);
                if(ql_qio_shld_dist_ctrl_switch != QL_QIO_SHLD_DISTANCE_CTRL_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_003_exec_data->ql_qio_shld_distance = 0;
                     *((unsigned char *)ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_UNSIGNED_SHORT_ADDRESS_TYPE:
                ql_qio_shld_distance = (float) *((unsigned short *)ql_qio_003_exec_data->ql_qio_shld_distance);
                if(ql_qio_shld_dist_ctrl_switch != QL_QIO_SHLD_DISTANCE_CTRL_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_003_exec_data->ql_qio_shld_distance = 0;
                     *((unsigned char *)ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_SIGNED_SHORT_ADDRESS_TYPE:
                ql_qio_shld_distance = (float) *((short *)ql_qio_003_exec_data->ql_qio_shld_distance);
                if(ql_qio_shld_dist_ctrl_switch != QL_QIO_SHLD_DISTANCE_CTRL_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_003_exec_data->ql_qio_shld_distance = 0;
                     *((unsigned char *)ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_UNSIGNED_INT_ADDRESS_TYPE:
                ql_qio_shld_distance = (float) *((unsigned int *)ql_qio_003_exec_data->ql_qio_shld_distance);
                if(ql_qio_shld_dist_ctrl_switch != QL_QIO_SHLD_DISTANCE_CTRL_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_003_exec_data->ql_qio_shld_distance = 0;
                     *((unsigned char *)ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE:
                ql_qio_shld_distance = (float) *((int *)ql_qio_003_exec_data->ql_qio_shld_distance);
                if(ql_qio_shld_dist_ctrl_switch != QL_QIO_SHLD_DISTANCE_CTRL_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_003_exec_data->ql_qio_shld_distance = 0;
                     *((unsigned char *)ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_FLOAT_ADDRESS_TYPE: 
                ql_qio_shld_distance =  *((float *)ql_qio_003_exec_data->ql_qio_shld_distance);
                if(ql_qio_shld_dist_ctrl_switch != QL_QIO_SHLD_DISTANCE_CTRL_MODULE_ON)
                {
                     *(float *)ql_qio_003_exec_data->ql_qio_shld_distance = 0;
                     *((unsigned char *)ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_switch) = 0;
                }
                else
                     ;
                break;            
         default :
             
             return -30;
    }
    /*We dont expect the rev to be too much for now (1st version)*/
#ifdef QL_PRINT_ENABLE
    Serial.printf("ql_qio_shld_dist_ctrl_unit:%d, ql_qio_shld_dist_ctrl_switch:%d, ql_qio_shld_distance:%d!\n",ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_unit, ql_qio_shld_dist_ctrl_switch,ql_qio_shld_distance);
#endif
    if(ql_qio_shld_dist_ctrl_switch == QL_QIO_SHLD_DISTANCE_CTRL_MODULE_ON)
    {
//#ifdef QL_PRINT_ENABLE
         Serial.printf("Performing Distance Ctrl Control\n");
//#endif
         switch(ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_state)
         {
             case QL_DIST_CTRL_RESET:
                 //digitalWrite( ql_qio_003_exec_data->ql_qio_shld_dist_mtr_pwr_pin, LOW );
                 //TODO check for a error and then transit to the next state
                 ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_state = QL_DIST_CTRL_CONFIG;
                 //break;
             case QL_DIST_CTRL_CONFIG:
                 pinMode(ql_qio_003_exec_data->ql_qio_shld_dist_mtr_dir_pin , OUTPUT );    //Set outputs
                 pinMode( ql_qio_003_exec_data->ql_qio_shld_dist_mtr_step_pin  , OUTPUT ); 
                 pinMode( ql_qio_003_exec_data->ql_qio_shld_dist_mtr_pwr_pin, OUTPUT );
                 ql_status = ql_init_qcode_timer();    
                 if(ql_status != QL_STATUS_OK)
                     return ql_status;
                 else
                     ;             
                 ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_state = QL_DIST_CTRL_ACTIVE;
                 //break;
             case QL_DIST_CTRL_ACTIVE:
                
                 //Serial.println(D);

                 switch(ql_qio_003_exec_data->ql_qio_qcode_003_timer_state)
                 {
                      case QL_QTIMER_QCODE_RESET:
                         ql_qio_003_exec_data->ql_qio_micro_steps = 0; // change the hard coding
                         ql_status = ql_add_timer_exec_list((void *)ql_qcode_003_mtr_cntrl_routine, ql_qio_003_exec_data, 1);
                         if(ql_status != QL_STATUS_OK)
                         {
                             ql_qio_003_exec_data->ql_qio_qcode_003_timer_state = QL_QTIMER_ERROR;
                              return ql_status;
                         }
                          else
                              ;
                         //break;
                      case QL_QTIMER_QCODE_WAITING:
                       /*Do the control logic*/
                         //Serial.printf("ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_pin:%d",ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_pin);
                         ql_shld_dist_readout = analogRead(ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_pin);
                         ql_shld_dist = 100*(0.02*ql_shld_dist_readout-1.67); //Convert raw signal value to distance (in mm, but times 100 to avoid float numbers)
                         //ql_shld_dist = 500;
                          if((ql_shld_dist >= 800) &&(ql_shld_dist  <=1200))
                          { 
                              //So the ideal value is now set on 10 mm, with a lower and upper limit of 8 and 12 mm respectively
                              digitalWrite( ql_qio_003_exec_data->ql_qio_shld_dist_mtr_pwr_pin, LOW ); //If set value is reached, turn off power
                          }                         /*New task can be completed!*/
                          if((ql_shld_dist < 800) || (ql_shld_dist > 1200))
                          {
                              ql_qio_003_exec_data->ql_qio_shld_curr_dist = ql_shld_dist;
                              
                              ql_qio_003_exec_data->ql_qio_qcode_003_timer_state = QL_QTIMER_START_PROCESSING;
                          }
                          else
                              ;
                         break;
                      case QL_QTIMER_PROCESSING:
                      /**/
                         
                         break;
                      case QL_QTIMER_START_PROCESSING:
                      case QL_QTIMER_ERROR:
                         break;
                      default:
                          ;
                 }
                 break;
             default:
                 break;    
         }
#ifdef QL_PRINT_ENABLE
         Serial.printf("Completed!\n");
#endif
    }
#ifdef QL_PRINT_ENABLE
    Serial.printf("Code 03 Executed!\n");
#endif
    return QL_STATUS_OK;
}
STATUS ql_qcode_003_config_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_003_config_size)
{
     if((!ql_qcode_config) || (!ql_qcode_003_config_size))
           return QL_ARG_ERROR;
     else
           ;
     *ql_qcode_003_config_size = sizeof(QL_IO_SHLD_DISTANCE_CTRL_03_CONFIG);
     
     return QL_STATUS_OK;
}
void ql_qcode_003_mtr_cntrl_routine(void *ql_qio_003_mtr_ctrl_arg)
{
     QL_QCODE_003_EXEC_DATA *ql_qio_003_exec_data;
     int i = 0;
     //Serial.printf("HERE\n");
     ql_qio_003_exec_data = (QL_QCODE_003_EXEC_DATA *)ql_qio_003_mtr_ctrl_arg;
     //Serial.printf("ql_qio_shld_dist_ctrl_unit:%d, ql_qio_003_exec_data->ql_qio_shld_dist_mtr_pwr_pin:%d\n",ql_qio_003_exec_data->ql_qio_shld_dist_ctrl_unit, ql_qio_003_exec_data->ql_qio_shld_dist_mtr_pwr_pin);
     //Serial.printf("ql_qio_003_exec_data->ql_qio_shld_dist_mtr_step_pin:%d,ql_qio_003_exec_data->ql_qio_shld_dist_mtr_dir_pin:%d\n",ql_qio_003_exec_data->ql_qio_shld_dist_mtr_step_pin,ql_qio_003_exec_data->ql_qio_shld_dist_mtr_dir_pin );
     //return;
     switch(ql_qio_003_exec_data ->ql_qio_qcode_003_timer_state)
     {
         case QL_QTIMER_START_PROCESSING:
             if (ql_qio_003_exec_data->ql_qio_shld_curr_dist >1200)
             {
                
                 digitalWrite( ql_qio_003_exec_data->ql_qio_shld_dist_mtr_pwr_pin, HIGH); 
                 digitalWrite( ql_qio_003_exec_data->ql_qio_shld_dist_mtr_dir_pin, HIGH );
             }       
             if (ql_qio_003_exec_data->ql_qio_shld_curr_dist < 800)
             {
                 //Serial.printf("HERE1\n");
                 digitalWrite( ql_qio_003_exec_data->ql_qio_shld_dist_mtr_pwr_pin, HIGH); 
                 digitalWrite( ql_qio_003_exec_data->ql_qio_shld_dist_mtr_dir_pin, LOW );       
             }
             ql_qio_003_exec_data->ql_qio_qcode_003_timer_state = QL_QTIMER_PROCESSING;
             ql_qio_003_exec_data->ql_qio_micro_steps = 0;
             
            //break;
         case QL_QTIMER_PROCESSING:
            if(ql_qio_003_exec_data->ql_qio_micro_steps < 80*4*2)
            {
              if(ql_qio_003_exec_data->ql_qio_micro_steps % 2)
              {
                //Serial.printf("L1\n");
                
                 digitalWrite( ql_qio_003_exec_data->ql_qio_shld_dist_mtr_step_pin, HIGH );
              }
              else
              {
                //Serial.printf("L2\n");
                for(i=0;i<2*ql_qio_003_exec_data->ql_qio_micro_steps & 0x03;i++)
                   __asm__("nop\n\t");
                 digitalWrite( ql_qio_003_exec_data->ql_qio_shld_dist_mtr_step_pin, LOW ); 
              }
            }
            else
            {
                ql_qio_003_exec_data->ql_qio_micro_steps = 0;
                ql_qio_003_exec_data->ql_qio_qcode_003_timer_state = QL_QTIMER_QCODE_WAITING;
            }
            ql_qio_003_exec_data->ql_qio_micro_steps++;
            break;
         default:
            break;
     }
   return;
}
#endif


typedef struct ql_qcode_005_DATA {
     unsigned int ql_qcode_output;
     void* ql_qcode_data; /*Incase of scratchpad!*/
} QL_QCODE_005_DATA;



#define QL_QIO_PART_PROC_AIR_FLOW_INCR_STEPS   1
#define QL_QIO_PART_PROC_MEASUREMENT_BUFFER  256*2 //Choose this w.r.t a convinientr number for FFT. i.e., in 256, 1024.

#define QL_QIO_PART_PROCESSING_FFT_SIZE               QL_QIO_PART_PROC_MEASUREMENT_BUFFER/2
typedef struct ql_qcode_005_EXEC_DATA {
     
     unsigned int ql_qio_part_proc_reg_type;
     unsigned int ql_qio_part_proc_flow_unit;
     unsigned int ql_qio_part_sensor_ctr;
     unsigned int ql_qio_paint_density;
     unsigned int ql_qio_air_viscosity;
     unsigned int ql_qio_meas_steps;
     unsigned int ql_qio_air_flow_set;
     unsigned int ql_qio_air_flow_reg_ctr;
     
     void* ql_qio_particle_size;
     unsigned char* ql_qio_part_proc_switch;  /*On and Off*/
	   unsigned short ql_qio_part_sensor_idx;
     unsigned short ql_qio_nozzle;
     unsigned short ql_qio_min_particle_size;
     unsigned short ql_qio_max_particle_size;
     unsigned short ql_qio_part_proc_meas_state;
     unsigned short ql_qio_part_proc_exec_state;
     unsigned short ql_qio_part_bin_ctr;
     float ql_qio_pid_residual_error;
     float ql_qio_air_flow;
     float ql_qio_min_air_flow;
     float ql_qio_max_air_flow;
     float ql_qio_nozzle_radius;
	   float ql_qio_part_proc_flow_incr;
     float ql_qio_pid_integral;
	   float ql_qio_part_sensor_meas[QL_QIO_PART_PROC_MEASUREMENT_BUFFER];
     unsigned char ql_qio_part_sensor_pin;
     unsigned char ql_qio_part_led_pin;
     unsigned char ql_qio_air_flow_ctrl_pin;
     unsigned char ql_qio_part_proc_unit;
     
     float ql_qio_part_size_est[QL_QIO_PART_PROC_AIR_FLOW_INCR_STEPS];
} QL_QCODE_005_EXEC_DATA;

typedef struct ql_io_mass_flow_ctrl_05_config {
    unsigned int ql_qio_part_proc_reg_type;
    unsigned int ql_qio_part_proc_unit;
    unsigned int ql_qio_particle_size;
    unsigned int ql_qio_part_proc_switch;
    unsigned int ql_qio_part_sensor_pin;
    unsigned int ql_qio_part_led_pin;
    unsigned int ql_qio_air_flow_ctrl_pin;
    unsigned int ql_qio_nozzle_radius;
    unsigned int ql_qio_nozzle;
    unsigned int ql_qio_min_particle_size;
    unsigned int ql_qio_max_particle_size;
    unsigned int ql_qio_paint_density;
    unsigned int ql_qio_air_viscosity;
} QL_IO_PART_PROC_CTRL_05_CONFIG;

enum {
  QL_PART_PROC_RESET = 1,
  QL_PART_PROC_CONFIG,
  QL_PART_PROC_ACTIVE,
  QL_PART_PROC_AIR_FLOW_CTRL,
  QL_PART_PROC_REGULATE_AIR_FLOW,
  QL_PART_PROC_MEASUREMENT_STATE,

};

enum {
   QL_QPMEAS_QCODE_RESET,
   QL_QPMEAS_QCODE_WAITING,
   QL_QPMEAS_COUNT_MGMT_STATE,
   QL_QPMEAS_SWITCH_ON_LED,
   QL_QPMEAS_SWITCH_OFF_LED,
   QL_QPMEAS_START_PROCESSING,
   QL_QPMEAS_ERROR
};


#define ADR 0x049

#define QL_QIO_PART_PROC_TIMER_COUNT    1000 //Used to create 10ms delay /*20 microseconds per timer shot, 50 times is necessary to convert into 1 milli second*/
#define QL_QIO_PART_PROC_LED_TIME_COUNTS   45 //500us //28/2 



#define QL_QIO_PART_PROC_MODULE_OFF   0x00
#define QL_QIO_PART_PROC_MODULE_ON   0x01
#define QL_QIO_AIR_FLOW_REG_MAX_COUNTS 1000 //Make this value as a function of loop execution time. We assume now a loop delay of 5 ms, and we timeout in 5 seconds
#define QL_QIO_PART_PROC_MEAS_PER_CYCLE QL_QIO_PART_PROC_MEASUREMENT_BUFFER


STATUS ql_qio_PID(float ql_qio_ctrl_set_point, float ql_qio_ctrl_process_var, unsigned int ql_qio_ctrl_pin,  float* ql_qio_pid_integral, float* ql_qio_pid_residual_error);
void ql_qcode_005_part_meas_routine(void *ql_qio_exec_data);

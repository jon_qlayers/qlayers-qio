

#define QL_DEFAULT_QTIMER_BLOCKS  10
  

typedef struct qtimer_block {
     struct qtimer_block* qtimer_next_block;
     void (*ql_qtimer_exec_qfunc) (void*);
     //void* ql_qtimer_exec_qfunc;
     void* ql_qtimer_qfunc_arg;
     unsigned int ql_qtimer_interval;
} QTIMER_BLOCK;

typedef struct qtimer_mgmt {
     unsigned int qtimer_interval;
     unsigned int qtimer_time_elapsed;
} QTIMER_MGMT;
#define QL_QCODE_TIMER_INTERVAL      125 //For Shld Distance Module
//#define QL_QCODE_TIMER_INTERVAL      10 //For Particle Sensor Unit
#define QL_QCODE_TIMER_PRIORITY      128

#include "arm_math.h"
#include "ql_types.h"
#include "ql_qio_error.h"
#include <stdio.h>
#include <stdlib.h>
#include <Arduino.h>
/* ------------------------------------------------------------------- 
* External Input and Output buffer Declarations for FFT Bin Example 
* ------------------------------------------------------------------- */ 
#define QL_SIG_EST_FFT_OUTPUT_SIZE     1024*2
#define QL_QIO_FFT_LIB_UNLOCK    0
#define QL_QIO_FFT_LIB_LOCK    1

static float32_t ql_fft_output[QL_SIG_EST_FFT_OUTPUT_SIZE]; 
static float32_t ql_fft_cmplx_mag[QL_SIG_EST_FFT_OUTPUT_SIZE];
static unsigned int ql_qio_fft_lock = QL_QIO_FFT_LIB_UNLOCK;
//This code is not suitable for multi thread! The first version in Teensy, we know that it is a single threaded application. 
//Only one block will access this code. Nevertheless we keep a lock and key mechanism. 

static uint32_t bit_reverse = 1; 
static uint32_t ifft_flag = 0; 
static arm_cfft_radix4_instance_f32 S; 
static arm_rfft_instance_f32 S_R;
unsigned int ql_qio_lib_fft_size = 0;

STATUS ql_qio_initialize_fft_lib(unsigned int fft_size)
{
     arm_status status;
     
     delay(1000);
     
     status = arm_cfft_radix4_init_f32(&S, fft_size, ifft_flag, bit_reverse); 
     if(status != ARM_MATH_SUCCESS)
     {
         return QL_FFT_INIT_ERROR;
     }
     else
         ;
     
     status = arm_rfft_init_f32 (&S_R, &S, fft_size, ifft_flag, bit_reverse);
     //Serial.printf("HereFFT2:%d,%d\n",status, ARM_MATH_SUCCESS);
     /*if(status != ARM_MATH_SUCCESS)
         return -2;
     else
         ;
       */  
  
     ql_qio_lib_fft_size = fft_size;
     return QL_STATUS_OK;
}

STATUS ql_estimate_dominant_signal(float32_t *signal_vector,uint32_t fft_size, float32_t *signal_estimate)
{
     arm_status status; 
     uint32_t max_index; 


     if((!signal_vector) || (!signal_estimate) || (!ql_fft_output) || (fft_size > 1024))
          return -1;
     else
          ;
     if(ql_qio_lib_fft_size != fft_size)
         return -20;
     else
         ;
         
     if(ql_qio_fft_lock != QL_QIO_FFT_LIB_UNLOCK)
          return QL_FFT_LIB_LOCK_ERROR;
     else
          ;

     ql_qio_fft_lock = QL_QIO_FFT_LIB_LOCK;
     //Serial.printf("8\n");
     int i =0;
     for(i=0;i<20;i++)
        Serial.printf("Sample i:%d,Value:%f\n",i,signal_vector[i]);
     //arm_rfft_fast_f32(&S, signal_vector, ql_fft_output);
     memset(ql_fft_output, 0x00, QL_SIG_EST_FFT_OUTPUT_SIZE*sizeof(float));
     memset(ql_fft_cmplx_mag, 0x00, QL_SIG_EST_FFT_OUTPUT_SIZE*sizeof(float));
     arm_rfft_f32 (&S_R, signal_vector, ql_fft_output);
//     arm_cfft_f32 (&S, signal_vector, ql_fft_output);
     //Serial.printf("3\n");
     arm_cmplx_mag_f32(ql_fft_output, ql_fft_cmplx_mag, fft_size);
     //Serial.printf("4\n");
     arm_max_f32(ql_fft_cmplx_mag, fft_size, signal_estimate, &max_index);
     *signal_estimate = *signal_estimate;
     ql_qio_fft_lock = QL_QIO_FFT_LIB_UNLOCK;
     return QL_STATUS_OK;
}

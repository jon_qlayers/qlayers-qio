
typedef struct ql_qcode
{
    unsigned int ql_qcode_type;
    struct ql_qcode* ql_qcode;
} QL_QCODE;

typedef struct ql_qcode_exec_unit {
    unsigned int ql_qcode_block_number;
    QL_QCODE* ql_qcode;
    unsigned int ql_qcode_block_type;
    unsigned int ql_qcode_data_type;
    struct ql_qcode_exec_unit* ql_qcode_exec_next;
    struct ql_qcode_exec_data* ql_qcode_exec_data;
} QL_QCODE_EXEC_UNIT;

typedef struct ql_qcode_block_output_db {
    struct ql_qcode_block_output_db *ql_qcode_block_list_next_node;
    unsigned int ql_qcode_block_number;
    QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit;
    unsigned int* ql_data_point;
} QL_QCODE_BLOCK_OUTPUT_DB;

typedef struct ql_qcode_DATA {
     unsigned int ql_qcode_output;
     unsigned int* ql_qcode_data; /*Incase of scratchpad!*/
} QL_QCODE_DATA;

typedef struct ql_qcode_exec_data {
     QL_QCODE_EXEC_UNIT *ql_qcode_exec_input_1;
     QL_QCODE_EXEC_UNIT *ql_qcode_exec_input_2;
     QL_QCODE_DATA  ql_qcode_data;      
} QL_QCODE_EXEC_DATA;

typedef struct ql_qcode_onfiguration {
    unsigned int qcode_total_blocks;
    unsigned int qcode_blocks_start;
} QL_QCODE_CONFIGURATION;

/**
//Total Blocks
//Block:Number
//Block Type
//Number of Inputs
//Number of Outputs
//Inputs
//Outputs

**/
#define QL_QCODE_UNSIGNED_INT              1
#define QL_QCODE_INT                       2
#define QL_QCODE_FLOAT                     3
#define QL_QCODE_UNSIGNED_SHORT            4
#define QL_QCODE_SHORT                     5
#define QL_QCODE_UNSIGNED_CHAR             6
#define QL_QCODE_CHAR                      7



typedef struct ql_qcode_config_block {
      unsigned int ql_qcode_block_number;
      unsigned int ql_qcode_block_type;
      unsigned int ql_qcode_inputs;
      unsigned int ql_qcode_outputs;
      unsigned int ql_qcode_data_type;
      unsigned int ql_qcode_parameters;
      unsigned int* ql_qcode_block_config;
} QL_QCODE_CONFIG_BLOCK;

typedef struct ql_qcode_config_input_port {
      unsigned int ql_qcode_input_block;
      unsigned int ql_qcode_input_port;
} QL_QCODE_CONFIG_INPUT_PORT;
/**QCODE ENGINE CALLS**/

typedef struct ql_io_device_exec_blk {
  struct ql_io_device_exec_blk *next_exec_blk;
  unsigned char ql_device_type; //device type
  unsigned char ql_device_func; //functionality
  void* ql_io_func_blocks;
} QL_IO_DEVICE_EXEC_BLK;

typedef struct __attribute__((__packed__)) ql_io_device_config_blk {
  unsigned int ql_qio_block_no; //device type
  unsigned int ql_qio_block_type; //functionality
  //unsigned int ql_ctrl_comm_intr_type;
  unsigned int ql_qio_device;
} QL_IO_DEVICE_CONFIG_BLK;


QL_QCODE_BLOCK_OUTPUT_DB ** ql_get_parse_block_db(void);
QL_QCODE_EXEC_UNIT ** ql_get_exec_unit_db(void);
STATUS ql_execute_exection_lane(QL_QCODE_EXEC_UNIT* ql_qcode_execution_lane);
STATUS ql_qcode_insert_block_execution_list(QL_QCODE_EXEC_UNIT **ql_qcode_exec_unit_list,QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit);
STATUS ql_qcode_parse_block_store_database(QL_QCODE_BLOCK_OUTPUT_DB **ql_qcode_block_db, unsigned int block_number, QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit);
STATUS ql_qcode_parse_search_retrieve_blocks_db (QL_QCODE_BLOCK_OUTPUT_DB **ql_qcode_block_db,  QL_QCODE_EXEC_UNIT** ql_qcode_exec_unit, unsigned int ql_qcode_search_block_number);
STATUS ql_qcode_parse_config_update_db(QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit, unsigned int *parse_next_block, unsigned int** parse_next_block_add);
STATUS ql_qcode_fetch_output(QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit, void* ql_qcode_output);
STATUS ql_qcode_update_data_type(void* source_data, void* dest_data, unsigned int ql_qcode_data_type);

unsigned int* ql_get_qcode_configuration(void);
STATUS ql_parse_qcode(unsigned int *ql_qcodes);
STATUS ql_parse_qcode_form_logical_connections(unsigned int *ql_qcodes);
STATUS ql_parse_qcode_form_execution_lane(unsigned int *ql_qcodes);

/**QCODE 004**/
//STATUS ql_qcode_004_execute_unit(QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit);
//STATUS ql_qcode_004_fetch_output(QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit, void* ql_qcode_004_output);
//STATUS ql_qcode_004_parse_config(unsigned int *parse_next_block, unsigned int** parse_next_block_add);
//STATUS ql_qcode_004_fetch_output_update_point(QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit, unsigned int ql_qcode_004_output_index, void** ql_qcode_004_output);
//STATUS ql_qcode_004_config_dynamic_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_004_config_dynm_size);


/**QCODE 005**/
STATUS ql_qcode_005_fetch_output(QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit, void* ql_qcode_005_output);
STATUS ql_qcode_005_execute_unit(QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit);
STATUS ql_qcode_005_parse_config(unsigned int *parse_next_block, unsigned int** parse_next_block_add);
STATUS ql_qcode_005_fetch_output_point(QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit, unsigned int ql_qcode_004_output_index, void** ql_qcode_005_output);
STATUS ql_qcode_005_config_dynamic_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_005_config_dynm_size);

/***QCODE 050*******/

STATUS ql_qcode_050_config_dynamic_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_050_config_dynm_size);




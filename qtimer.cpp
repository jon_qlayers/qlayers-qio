/**************************************************************************************************************************************
Copyright - QLAYERS.B.V

This File is a confidential property of QLayers B.V. Any redistribution or use without prior legal contract will be a legal concern. 

queue_mgmt.c - This file contains the run time logic of Queues and its management between different threads.

Revision - Version x.x - Author
***************************************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ql_qio_error.h"
#include "ql_types.h"
#include "qtimer.h"
#include <arduino.h>  

typedef struct qtimer_status {
     unsigned int qtimer_block_pool;
     unsigned int qtimer_block_allocated;
     unsigned int qtimer_mgmt_error_status;
} QTIMER_STATUS;

static QTIMER_BLOCK *ql_qtimer_pool=NULL;
static QTIMER_BLOCK *ql_qtimer_list=NULL;
static QTIMER_BLOCK ql_qtimer_blocks[QL_DEFAULT_QTIMER_BLOCKS];
static QTIMER_STATUS ql_qtimer_mgmt_status;
static QTIMER_BLOCK* ql_qtimer_exec_list = NULL;
/*Initialize the Queue memory blocks and the counters*/
 STATUS ql_qtimer_mem_init (void)
{
     QL_UINT i = 0; 
     if(ql_qtimer_pool != NULL)
        return QL_QTIMER_INIT_ERROR;
     else
        ;
     for (i=0; i<QL_DEFAULT_QTIMER_BLOCKS; i++)
     {
         memset( &ql_qtimer_blocks[i],0, sizeof(ql_qtimer_blocks[i]));
         ql_qtimer_blocks[i].qtimer_next_block = ql_qtimer_pool;
         /*Set the block to point at the previously inserted queue*/
      //   memset( &ql_qtimer_blocks[i].qtimer_mem_buff[0],0, sizeof(ql_qtimer_blocks[i].qtimer_mem_buff));
        /*Clear the memory*/ 
	
        /*Inset into the queue!*/
        ql_qtimer_pool = &ql_qtimer_blocks[i];
         
     }
     ql_qtimer_mgmt_status.qtimer_block_pool = i;
     ql_qtimer_mgmt_status.qtimer_block_allocated = 0;
     ql_qtimer_mgmt_status.qtimer_mgmt_error_status = QL_QTIMER_STATUS_OK;
     return QL_STATUS_OK;
}

STATUS qtimer_get_block(QTIMER_BLOCK** qtimer_block)
{
     if(qtimer_block == NULL)
         return QL_GET_QTIMER_BLOCK_PARAM_ERROR;
     else
         ;
     if(ql_qtimer_pool == NULL)
     {
         /*Queue Blocks have been allocated! No memory Left!*/
         return QL_GET_QTIMER_BLOCK_MEMORY_ERROR;
         
     }
     else
         ;
     //TODO!! LOCK with semaphore!

     *qtimer_block = (QTIMER_BLOCK*)ql_qtimer_pool;

     //printf("Debug:%p\n", qtimer_block);
     ql_qtimer_pool = ql_qtimer_pool->qtimer_next_block;
     ql_qtimer_mgmt_status.qtimer_block_allocated++;
     ql_qtimer_mgmt_status.qtimer_block_pool--;

     //Unlock!
     return QL_STATUS_OK;
}

STATUS qtimer_release_block(QTIMER_BLOCK* qtimer_block)
{
    if(qtimer_block == NULL)
         return QL_RELEASE_QTIMER_BLOCK_PARAM_ERROR;
     else
         ;
     if( ql_qtimer_mgmt_status.qtimer_block_allocated)
          ; /*There is a previous allocation*/
     else
     {
         return QL_QTIMER_RELEASE_BLOCK_INVALID_USAGE_ERROR; /*There is no previous allocation and yet this API has been called*/
     }
     //TODO!! LOCK with semaphore!
     /*Clear the memory before putting them into the pool*/
     memset(qtimer_block, 0, sizeof(QTIMER_BLOCK));
     qtimer_block->qtimer_next_block = ql_qtimer_pool;
     ql_qtimer_pool = qtimer_block;
     ql_qtimer_mgmt_status.qtimer_block_allocated--;
     ql_qtimer_mgmt_status.qtimer_block_pool++;
     //Unlock!
     return QL_STATUS_OK;
}

/*The following blocks can be used as unit test blocks for unit testing*/

STATUS ql_qtimer_print_statistics(void)
{
    printf("Pool:%d, ", ql_qtimer_mgmt_status.qtimer_block_pool);
    printf("Allocated:%d, ", ql_qtimer_mgmt_status.qtimer_block_allocated);
    printf("Queue Status:%d\n", ql_qtimer_mgmt_status.qtimer_mgmt_error_status);
    return QL_STATUS_OK;
}


static void test_queue_mgmt_1(void)
{
      STATUS status;
      status = ql_qtimer_mem_init();
      if(status != QL_STATUS_OK)
           printf("Qtimer Init Error. Status:%d.\n", status);
      else
           printf("Qtimer Initialization Successfully Tested.\n");
      printf("Pool:%d, ", ql_qtimer_mgmt_status.qtimer_block_pool);
      printf("Allocated:%d, ", ql_qtimer_mgmt_status.qtimer_block_allocated);
      printf("Qtimer Status:%d\n", ql_qtimer_mgmt_status.qtimer_mgmt_error_status);

      return;
}
STATUS ql_add_timer_exec_list(void* ql_qtimer_exec_qfunc, void* ql_qtimer_qfunc_arg,unsigned int ql_qtimer_interval/*, QTIMER_BLOCK** ql_qtimer_exec_list*/)
{
      STATUS status;
      QTIMER_BLOCK* ql_qtimer_block;
      QTIMER_BLOCK* ql_qtimer_exec;

     //Serial.printf("HEREEEE\n");
     if(!ql_qtimer_exec_qfunc || !ql_qtimer_qfunc_arg)
         return -1;
     else
         ;
   
     status = qtimer_get_block(&ql_qtimer_block);
     if(status != QL_STATUS_OK)
         return status;
     else
         ;


     ql_qtimer_block->ql_qtimer_exec_qfunc = (void (*)(void*))ql_qtimer_exec_qfunc;
     ql_qtimer_block->ql_qtimer_qfunc_arg = ql_qtimer_qfunc_arg;
     ql_qtimer_block->ql_qtimer_interval = ql_qtimer_interval;
     ql_qtimer_block->qtimer_next_block = NULL;
     
     //ql_qtimer_exec = *ql_qtimer_exec_list;
     ql_qtimer_exec = ql_qtimer_exec_list;

     
     //Disable the interrupts before updating the list
     noInterrupts();
     if(!ql_qtimer_exec)
     {         
         ql_qtimer_exec_list = ql_qtimer_block;
     }
     else
     {
         
         while(ql_qtimer_exec->qtimer_next_block)
              ql_qtimer_exec = ql_qtimer_exec->qtimer_next_block;
         ql_qtimer_exec->qtimer_next_block = ql_qtimer_block;
     }
 
     

     interrupts();
   

     return QL_STATUS_OK;
}

enum {
  QL_QCODE_TIMER_RESET = 1,
  QL_QCODE_TIMER_INITIALIZED,
  QL_QCODE_TIMER_ERROR
};

unsigned int ql_qcode_timer_state =  QL_QCODE_TIMER_RESET;
IntervalTimer ql_qcode_timer;

/*For now there is no switching to allocate the timers dynamically. For now timer 0 is allocated for FC 003 and other codes*/
void ql_qcode_timer_stop(void)
{
  ql_qcode_timer_state = QL_QCODE_TIMER_RESET;
  ql_qcode_timer.end();
}
void ql_qcode_timer_routine()
{
    QTIMER_BLOCK* ql_qtimer_exec;
    int i =0;
    ql_qtimer_exec = ql_qtimer_exec_list;
    while(ql_qtimer_exec)
    {
        //Serial.printf("Here:%d!\n",i++);
        (ql_qtimer_exec->ql_qtimer_exec_qfunc)(ql_qtimer_exec->ql_qtimer_qfunc_arg);
        ql_qtimer_exec = ql_qtimer_exec->qtimer_next_block;
    }
    //Serial.printf("Here11111111111!\n");
    return;
}


STATUS ql_init_qcode_timer(void)
{
      STATUS ql_status;
      //Serial.printf("Here1!\n");
      switch(ql_qcode_timer_state)
      {
          case QL_QCODE_TIMER_RESET:
            ql_qcode_timer.begin(ql_qcode_timer_routine, QL_QCODE_TIMER_INTERVAL); 
            ql_qcode_timer.priority(QL_QCODE_TIMER_PRIORITY); 
            ql_qcode_timer_state = QL_QCODE_TIMER_INITIALIZED;
            break;
          case QL_QCODE_TIMER_INITIALIZED:
             break;
          case QL_QCODE_TIMER_ERROR :
             break;
          default:
            break;
      }
      //ql_qcode_timer.update(QL_QCODE_TIMER_INTERVAL); 
     return QL_STATUS_OK;
}







/*
static void test_queue_mgmt_2(void)
{
      STATUS status;
      QUEUE_BLOCK* queue_block;

      status = queue_get_block(&queue_block);
      if(status != QL_STATUS_OK)
           printf("Queue Get Block Error. Status:%d.\n", status);
      else
           printf("Queue Get Block Successfully Tested.\n");
      printf("Pool:%d, ", ql_queue_mgmt_status.queue_blocks_pool);
      printf("Allocated:%d, ", ql_queue_mgmt_status.queue_allocated);
      printf("Queue Status:%d\n", ql_queue_mgmt_status.queue_mgmt_error_status);

      status = queue_release_block(queue_block);
      if(status != QL_STATUS_OK)
           printf("Queue Release Block Error. Status:%d.\n", status);
      else
           printf("Queue Release Block Successfully Tested.\n");
      printf("Pool:%d, ", ql_queue_mgmt_status.queue_blocks_pool);
      printf("Allocated:%d, ", ql_queue_mgmt_status.queue_allocated);
      printf("Queue Status:%d\n", ql_queue_mgmt_status.queue_mgmt_error_status);
  
      return;
}
*/

/*
static void test_queue_mgmt_3(void)
{
      STATUS status;
      QUEUE_BLOCK* queue_block[QL_DEFAULT_QUEUE_BLOCKS];
      QL_UINT i = 0;
      printf("Test 3\n");
      //QL_DEFAULT_QUEUE_BLOCKS-1
      for (i=0;i< QL_DEFAULT_QUEUE_BLOCKS-1; i++)
      {
          status = queue_get_block(( QUEUE_BLOCK**) &queue_block[i]);
          printf("status:%d, %p\n", QL_STATUS_OK, queue_block[i]);
      }
      //return;
      status = queue_get_block( (QUEUE_BLOCK**) &queue_block[i]);
      if(status != QL_STATUS_OK)
            printf("Queue Get Block Error. Status:%d.\n", status);
      else
            printf("Queue Get Block Successfully Tested.\n");
      printf("Pool:%d, ", ql_queue_mgmt_status.queue_blocks_pool);
      printf("Allocated:%d, ", ql_queue_mgmt_status.queue_allocated);
      printf("Queue Status:%d\n", ql_queue_mgmt_status.queue_mgmt_error_status);
     
      /*Must give an error!*/
      /*
      status = queue_get_block( (QUEUE_BLOCK**) &queue_block[i]);
      if(status != QL_STATUS_OK)
            printf("Queue Get Block Error. Status:%d.\n", status);
      else
            printf("Queue Get Block Successfully Tested.\n");
      printf("Pool:%d, ", ql_queue_mgmt_status.queue_blocks_pool);
      printf("Allocated:%d, ", ql_queue_mgmt_status.queue_allocated);
      printf("Queue Status:%d\n", ql_queue_mgmt_status.queue_mgmt_error_status);
      
*/
      /*Release the blocks*/
  /*    for (i=0;i< QL_DEFAULT_QUEUE_BLOCKS; i++)
      {
         status = queue_release_block((QUEUE_BLOCK*) queue_block[i]);
         if(status != QL_STATUS_OK)
             printf("Queue Release Block Error. Status:%d.\n", status);
         else
             printf("Queue Release Block Successfully Tested.\n");
      }
      printf("Pool:%d, ", ql_queue_mgmt_status.queue_blocks_pool);
      printf("Allocated:%d, ", ql_queue_mgmt_status.queue_allocated);
      printf("Queue Status:%d\n", ql_queue_mgmt_status.queue_mgmt_error_status);

      return;
}

*/
/*
static void test_queue_mgmt_4(void)
{
      STATUS status;
      QUEUE_BLOCK* queue_block[QL_DEFAULT_QUEUE_BLOCKS];
      QL_UINT i = 0;
      printf("Test 3\n");
      //QL_DEFAULT_QUEUE_BLOCKS-1
      for (i=0;i< QL_DEFAULT_QUEUE_BLOCKS-1; i++)
      {
          status = queue_get_block(( QUEUE_BLOCK**) &queue_block[i]);
          printf("status:%d, %p\n", QL_STATUS_OK, queue_block[i]);
      }
      return;
}
*/
/*
void main(void)
{
    //This main is a stub!
    // Used during development to text the modules. Need to integrate the test modules into test suite.
    test_queue_mgmt_1();
    test_queue_mgmt_2(); 
    test_queue_mgmt_3(); 
    test_queue_mgmt_4();
    return;
}*/



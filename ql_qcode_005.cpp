/**************************************************************************************************************************************
Copyright - QLAYERS.B.V

This File is a confidential property of QLayers B.V. Any redistribution or use without prior legal contract will be a legal concern. 

queue_mgmt.c - This file contains the run time logic of Queues and its management between different threads.

Revision - Version x.x - Author
***************************************************************************************************************************************/
#include <SPI.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ql_qio_error.h"
#include "ql_types.h"
#include "ql_qcode.h"
#include "ql_i2c.h"
#include "ql_qcode_005.h"
#include <i2c_t3.h>
#include <Arduino.h>
//#include <HardwareSerial.h>
//#include "ql_debug.h"
#include <arm_math.h>

extern void digitalPotWrite(int address, int value);


extern STATUS ql_init_qcode_timer(void);
extern void ql_qcode_timer_stop(void);
extern STATUS ql_add_timer_exec_list(void* ql_qtimer_exec_qfunc, void* ql_qtimer_qfunc_arg,unsigned int ql_qtimer_interval/*, QTIMER_BLOCK** ql_qtimer_exec_list*/);
extern STATUS ql_qio_i2c_allocate_address(unsigned int ql_qio_block_no, unsigned int ql_qio_i2c_address_type, unsigned int ql_qio_i2c_address_req, void **ql_qio_i2c_address_ret);
STATUS ql_qio_conv_nozzle_size_air_flow(float ql_particle_size, float* ql_air_flow, QL_QCODE_005_EXEC_DATA *ql_qio_005_exec_data);
STATUS ql_qio_005_read_air_flow(float* ql_qio_air_flow);
/*Particle Size Measurement Unit*/
/*extern "C" {
STATUS ql_estimate_dominant_signal(float *signal_vector,uint32_t fft_size, float *signal_estimate);
}
*/
extern STATUS ql_estimate_dominant_signal(float32_t *signal_vector,uint32_t fft_size, float32_t *signal_estimate);
STATUS ql_qcode_005_parse_config(unsigned int *parse_next_block, unsigned int** parse_next_block_add, QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit)
{
    
     STATUS ql_status = QL_STATUS_OK;
     unsigned int ql_qio_block_no; //device type
     unsigned int ql_qio_block_type; //functionality     
     unsigned int ql_i2c_reg_type;
     unsigned int ql_qio_part_proc_reg_type;
     unsigned int ql_qio_particle_size;
     unsigned int ql_qio_part_proc_switch;
     void* ql_i2c_data_address;

     QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit_005 = NULL;
     
     QL_QCODE_005_EXEC_DATA *ql_qio_005_exec_data;
    
     QL_IO_PART_PROC_CTRL_05_CONFIG* ql_io_part_proc_ctrl_05_config;
     QL_IO_DEVICE_CONFIG_BLK* ql_io_device_config_blk;

     unsigned int ql_qcode_config_dynm_size = 0; 

     ql_qio_005_exec_data = (QL_QCODE_005_EXEC_DATA*) ql_qcode_exec_unit->ql_qcode_exec_data;
     ql_io_device_config_blk = (QL_IO_DEVICE_CONFIG_BLK*) parse_next_block;

     
     ql_io_part_proc_ctrl_05_config = (QL_IO_PART_PROC_CTRL_05_CONFIG *) &ql_io_device_config_blk->ql_qio_device;
     ql_qio_part_proc_reg_type = ql_io_part_proc_ctrl_05_config->ql_qio_part_proc_reg_type;
     ql_qio_particle_size = ql_io_part_proc_ctrl_05_config->ql_qio_particle_size;
     ql_qio_part_proc_switch = ql_io_part_proc_ctrl_05_config->ql_qio_part_proc_switch;
     //ql_qio_part_sensor_pin = ql_io_part_proc_ctrl_05_config->ql_qio_part_sensor_pin;
     //ql_qio_part_proc_unit = ql_io_part_proc_ctrl_05_config->ql_qio_part_proc_unit;
     
     ql_qio_005_exec_data->ql_qio_part_proc_reg_type = ql_qio_part_proc_reg_type;
     
     switch (ql_qio_part_proc_reg_type)
     {
            case QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE:
            case QL_IO_DEVICE_SIGNED_CHAR_ADDRESS_TYPE:
            case QL_IO_DEVICE_UNSIGNED_SHORT_ADDRESS_TYPE:
            case QL_IO_DEVICE_SIGNED_SHORT_ADDRESS_TYPE:
            case QL_IO_DEVICE_UNSIGNED_INT_ADDRESS_TYPE:
            case QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE:
            case QL_IO_DEVICE_FLOAT_ADDRESS_TYPE:
                 break;
            default :
#ifdef QL_PRINT_ENABLE 
                 Serial.printf("qblock_data_type:%d\n", ql_qio_part_proc_reg_type);
#endif
                 return QL_QCODE_INVALID_DATA_TYPE;
     }

     /*Allocate the memory for the i2c data interface*/
     ql_status = ql_qio_i2c_allocate_address(ql_io_device_config_blk->ql_qio_block_no, ql_qio_part_proc_reg_type,ql_qio_particle_size, &ql_i2c_data_address); 
     if(ql_status != QL_STATUS_OK) 
         return ql_status;
     else
         ;

     
     ql_qio_005_exec_data->ql_qio_particle_size = ql_i2c_data_address;
	 //HERE
     //Cant use an OR on the status because the error codes are not one hot encoding!
     ql_status = ql_qio_i2c_allocate_address(ql_io_device_config_blk->ql_qio_block_no ,QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE,ql_qio_part_proc_switch, &ql_i2c_data_address); 
     if(ql_status != QL_STATUS_OK)
         return ql_status;
     else
         ;
     ql_qio_005_exec_data->ql_qio_part_proc_switch = (unsigned char *) ql_i2c_data_address;  
     
     /*Pins*/
     
     
     ql_qio_005_exec_data->ql_qio_part_proc_unit = ql_io_part_proc_ctrl_05_config->ql_qio_part_proc_unit;
     ql_qio_005_exec_data->ql_qio_part_sensor_pin = ql_io_part_proc_ctrl_05_config->ql_qio_part_sensor_pin;
     ql_qio_005_exec_data->ql_qio_part_led_pin = ql_io_part_proc_ctrl_05_config->ql_qio_part_led_pin;
     ql_qio_005_exec_data->ql_qio_air_flow_ctrl_pin = ql_io_part_proc_ctrl_05_config->ql_qio_air_flow_ctrl_pin;
     ql_qio_005_exec_data->ql_qio_nozzle_radius = (float) ql_io_part_proc_ctrl_05_config->ql_qio_nozzle_radius;
     ql_qio_005_exec_data->ql_qio_nozzle_radius = ql_qio_005_exec_data->ql_qio_nozzle_radius*1e-6;
     ql_qio_005_exec_data->ql_qio_nozzle = ql_io_part_proc_ctrl_05_config->ql_qio_nozzle;
     ql_qio_005_exec_data->ql_qio_min_particle_size = ql_io_part_proc_ctrl_05_config->ql_qio_min_particle_size;
     ql_qio_005_exec_data->ql_qio_max_particle_size = ql_io_part_proc_ctrl_05_config->ql_qio_max_particle_size;
     ql_qio_005_exec_data->ql_qio_paint_density = ql_io_part_proc_ctrl_05_config->ql_qio_paint_density;
     ql_qio_005_exec_data->ql_qio_air_viscosity = ql_io_part_proc_ctrl_05_config->ql_qio_air_viscosity;

     
     ql_qio_005_exec_data->ql_qio_part_proc_exec_state = QL_PART_PROC_RESET;
     ql_qio_005_exec_data->ql_qio_part_proc_meas_state = QL_QPMEAS_QCODE_RESET;
    
     *parse_next_block_add = parse_next_block + (sizeof(QL_IO_PART_PROC_CTRL_05_CONFIG)/(sizeof(unsigned int))) + (sizeof(QL_IO_DEVICE_CONFIG_BLK) - sizeof(ql_io_device_config_blk->ql_qio_device))/(sizeof(unsigned int));

     return QL_STATUS_OK;
}

STATUS ql_qcode_005_allocate_exec_data_mem(void** ql_qcode_005_data_mem)
{
     void *ql_qcode_data_mem = NULL;
     if(!ql_qcode_005_data_mem)
          return QL_ARG_ERROR;
     else
          ;
     
     ql_qcode_data_mem = malloc(sizeof(QL_QCODE_005_EXEC_DATA));
     if(! ql_qcode_data_mem)
          return -1;
     else
          ;
     memset(ql_qcode_data_mem, 0x00, sizeof(QL_QCODE_005_EXEC_DATA));
     *ql_qcode_005_data_mem = ql_qcode_data_mem;
     return QL_STATUS_OK;
}

/*Float is not supported at the moment!*/



/*Switch is always of type Char*/

STATUS  ql_qcode_005_execute_unit(QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit)
{
    unsigned int ql_qio_part_proc_switch;
    float ql_qio_air_flow_meas = 0;   
    QL_QCODE_005_EXEC_DATA *ql_qio_005_exec_data;
    STATUS ql_status;
    float ql_signal_estimate = 0;
    float ql_part_summation = 0;
    short i = 0;
    if(!ql_qcode_exec_unit)
         return -2;
    else
         ;
    
    ql_qio_005_exec_data = (QL_QCODE_005_EXEC_DATA*) ql_qcode_exec_unit->ql_qcode_exec_data;
    ql_qio_part_proc_switch = (int) *((unsigned char *)ql_qio_005_exec_data->ql_qio_part_proc_switch);
    
    
#if 0
    switch(ql_qio_005_exec_data->ql_qio_part_proc_reg_type)
    {
         case QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE:
                //ql_qio_part = (float) *((unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow);
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow = 0;
                     *((unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_SIGNED_CHAR_ADDRESS_TYPE:
                //ql_qio_mass_flow = (float) *((unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow);
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow = 0;
                     *((unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_UNSIGNED_SHORT_ADDRESS_TYPE:
                //ql_qio_mass_flow = (float) *((unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow);
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow = 0;
                     *((unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_SIGNED_SHORT_ADDRESS_TYPE:
                //ql_qio_mass_flow = (float) *((unsigned char *)ql_qio_002_exec_data->ql_qio_mass_flow);
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow = 0;
                     *((unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_UNSIGNED_INT_ADDRESS_TYPE:
                //ql_qio_mass_flow = (float) *((unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow);
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow = 0;
                     *((unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE:
                ql_qio_mass_flow = (float) *((unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow);
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow = 0;
                     *((unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_FLOAT_ADDRESS_TYPE: 
                ql_qio_mass_flow =  *((float *)ql_qio_005_exec_data->ql_qio_mass_flow);
                if(ql_qio_mass_flow_switch != QL_QIO_MASS_FLOW_MODULE_ON)
                {
                     *(float *)ql_qio_005_exec_data->ql_qio_mass_flow = 0;
                     *((unsigned char *)ql_qio_005_exec_data->ql_qio_mass_flow_switch) = 0;
                }
                else
                     ;
                break;            
         default :
             
             return -30;
    }
#endif

    /*We dont expect the rev to be too much for now (1st version)*/
#ifdef QL_PRINT_ENABLE
    //Serial.printf("ql_qio_mass_flow_unit:%d, ql_qio_mass_flow_switch:%d, ql_qio_mass_flow:%d! ",ql_qio_005_exec_data->ql_qio_mass_flow_unit, ql_qio_mass_flow_switch,ql_qio_mass_flow);
#endif
   //ql_qio_part_proc_switch = QL_QIO_PART_PROC_MODULE_ON;//For Testing
    if(ql_qio_part_proc_switch == QL_QIO_PART_PROC_MODULE_ON)
    {
         switch(ql_qio_005_exec_data->ql_qio_part_proc_exec_state)
         {
             case QL_PART_PROC_RESET:
                 //digitalWrite( ql_qio_003_exec_data->ql_qio_shld_dist_mtr_pwr_pin, LOW );
                 //TODO check for a error and then transit to the next state
                 ql_qio_005_exec_data->ql_qio_part_proc_exec_state = QL_PART_PROC_CONFIG;
                 
                 //break;
             case QL_PART_PROC_CONFIG:
                 pinMode(ql_qio_005_exec_data->ql_qio_part_sensor_pin , INPUT );    
                 pinMode(ql_qio_005_exec_data->ql_qio_part_led_pin , OUTPUT );
                 
                 //pinMode(ql_qio_005_exec_data->ql_qio_air_flow_ctrl_pin , OUTPUT );
                 //pinMode( ql_qio_005_exec_data->ql_qio_shld_dist_mtr_step_pin  , OUTPUT ); 
                 //pinMode( ql_qio_005_exec_data->ql_qio_shld_dist_mtr_pwr_pin, OUTPUT );
                 //Serial.printf("ql_qio_005_exec_data->ql_qio_part_sensor_pin %d\n",ql_qio_005_exec_data->ql_qio_part_sensor_pin);
                 //Serial.printf("ql_qio_005_exec_data->ql_qio_part_led_pin %d\n",ql_qio_005_exec_data->ql_qio_part_led_pin);
                 ql_status = ql_init_qcode_timer();    
                 if(ql_status != QL_STATUS_OK)
                     return ql_status;
                 else
                     ;
                 
                 ql_status = ql_qio_conv_nozzle_size_air_flow(ql_qio_005_exec_data->ql_qio_min_particle_size, &ql_qio_005_exec_data->ql_qio_min_air_flow,ql_qio_005_exec_data);
                 if(ql_status != QL_STATUS_OK)
                      return -20;
                 else
                      ;
                 
                 ql_status = ql_qio_conv_nozzle_size_air_flow(ql_qio_005_exec_data->ql_qio_max_particle_size, &ql_qio_005_exec_data->ql_qio_max_air_flow,ql_qio_005_exec_data);
                 if(ql_status != QL_STATUS_OK)
                      return -20;
                 else
                      ;
                                        
                  
                 ql_qio_005_exec_data->ql_qio_part_proc_flow_incr = (ql_qio_005_exec_data->ql_qio_max_air_flow - ql_qio_005_exec_data->ql_qio_max_air_flow)/QL_QIO_PART_PROC_AIR_FLOW_INCR_STEPS;
                 ql_status = ql_add_timer_exec_list((void *)ql_qcode_005_part_meas_routine, (void *) ql_qio_005_exec_data, 1);
                 if(ql_status != QL_STATUS_OK)
                 {
                        ql_qio_005_exec_data->ql_qio_part_proc_meas_state = QL_QPMEAS_ERROR;
                        return ql_status;
                 }
                 else
                       ;
                 ql_qio_005_exec_data->ql_qio_part_proc_meas_state = QL_QPMEAS_QCODE_RESET; //It will be changed anyways in QL_PART_PROC_AIR_FLOW_CTRL. But this is an initialization!
                 //break;
                 ql_qio_005_exec_data->ql_qio_meas_steps = 0;
                 //ql_qio_005_exec_data->ql_qio_part_bin_ctr = 0;
                 ql_qio_005_exec_data->ql_qio_part_proc_exec_state = QL_PART_PROC_AIR_FLOW_CTRL;

                 SPI.begin();
                 
                 digitalPotWrite(0, 240);
                 
                 delay(10);
                 digitalPotWrite(1, 240);
                 delay(10);
                 digitalPotWrite(2, 239);
                 delay(10);
                 digitalPotWrite(3, 240);
                 delay(10);
                 digitalPotWrite(4, 1);
                 delay(10);
                 digitalPotWrite(5, 1);
                 delay(10);
                 //Serial.printf("Config\n");
              
             case QL_PART_PROC_AIR_FLOW_CTRL:
                 
                 ql_qio_005_exec_data->ql_qio_air_flow_set=(int) 100*(ql_qio_005_exec_data->ql_qio_max_air_flow-ql_qio_005_exec_data->ql_qio_meas_steps*ql_qio_005_exec_data->ql_qio_part_proc_flow_incr);       //Unit: LPM*100
                 //ql_qio_air_flow= int(airFlow);

                 if (ql_qio_005_exec_data->ql_qio_air_flow_set < ql_qio_005_exec_data->ql_qio_min_air_flow)
                 {
                    ql_qio_005_exec_data->ql_qio_air_flow_set = (int) ql_qio_005_exec_data->ql_qio_min_air_flow;
                 }

                  ql_qio_005_exec_data->ql_qio_air_flow_reg_ctr = 0;    
                  ql_qio_005_exec_data->ql_qio_part_proc_exec_state = QL_PART_PROC_REGULATE_AIR_FLOW;
                  
                 
                  //break;
             case QL_PART_PROC_REGULATE_AIR_FLOW:
                  /*First try to regulate the air flow at the required threshold*/
                  
 #if 0
                  ql_status = ql_qio_005_read_air_flow(&ql_qio_air_flow_meas);
                  if(ql_status != QL_STATUS_OK)
                       return ql_status;
                  else
                      ;
 #endif
                  
 #if 0
                  ql_status = ql_qio_PID(ql_qio_005_exec_data->ql_qio_air_flow_set, ql_qio_air_flow_meas, ql_qio_005_exec_data->ql_qio_air_flow_ctrl_pin,  
                                                                                                    &ql_qio_005_exec_data->ql_qio_pid_integral, &ql_qio_005_exec_data->ql_qio_pid_residual_error);
                  if(ql_status != QL_STATUS_OK)
                        return ql_status;
                  else
                        ;
#endif
#if 0 
                  if(ql_qio_005_exec_data->ql_qio_pid_residual_error < .2) //Change this hardcoding to a MAcro. Later, after testing
                  {
                        ql_qio_005_exec_data->ql_qio_part_proc_exec_state = QL_PART_PROC_MEASUREMENT_STATE;
                        ql_qio_005_exec_data->ql_qio_part_proc_meas_state = QL_QPMEAS_START_PROCESSING;  
                  }
                  else
                        ;
                  
                  if(ql_qio_005_exec_data->ql_qio_air_flow_reg_ctr > QL_QIO_AIR_FLOW_REG_MAX_COUNTS)
                  {
                       return -30;
                  }
                  else
                       ;
#endif                       
                
                 ql_qio_005_exec_data->ql_qio_part_proc_exec_state = QL_PART_PROC_MEASUREMENT_STATE;
                 ql_qio_005_exec_data->ql_qio_part_proc_meas_state = QL_QPMEAS_START_PROCESSING;  
                  /*Once that is achieved, then we switch to the measurement state*/
                  break;
             case QL_PART_PROC_MEASUREMENT_STATE:
                
                 //Serial.println(D);

                 switch(ql_qio_005_exec_data->ql_qio_part_proc_meas_state)
                 {
                  
                      case QL_QPMEAS_QCODE_RESET:
                         /*
                         ql_status = ql_add_timer_exec_list((void *)ql_qcode_005_part_meas_routine, (void *) ql_qio_005_exec_data, 1);
                         if(ql_status != QL_STATUS_OK)
                         {
                             ql_qio_005_exec_data->ql_qio_part_proc_meas_state = QL_QPMEAS_ERROR;
                              return ql_status;
                         }
                         else
                              ;
                         */
                         ql_qio_005_exec_data->ql_qio_part_proc_meas_state = QL_QPMEAS_START_PROCESSING;     
                         break;
                      case QL_QPMEAS_QCODE_WAITING:
                      
                         //DO the FFT and get the particle size
                         if(ql_qio_005_exec_data->ql_qio_meas_steps < QL_QIO_PART_PROC_AIR_FLOW_INCR_STEPS-1)
                         {
                             //Serial.printf("FFT begin\n");
                             ql_signal_estimate = 0;
                             ql_status = ql_estimate_dominant_signal(ql_qio_005_exec_data->ql_qio_part_sensor_meas, QL_QIO_PART_PROCESSING_FFT_SIZE, &ql_signal_estimate);
                             //Serial.printf("FFT end\n");
                             ql_part_summation = 0;
                            
                             Serial.printf("ql_qio_005_exec_data->ql_qio_meas_steps:%d\n", ql_qio_005_exec_data->ql_qio_meas_steps);
                             for(i = 0;i<((short) ql_qio_005_exec_data->ql_qio_meas_steps)-1;i++)
                             {
                                 Serial.printf("i:%d\n",i);
                                 ql_part_summation+=ql_qio_005_exec_data->ql_qio_part_size_est[i];
                             }
                             ql_qio_005_exec_data->ql_qio_part_size_est[ql_qio_005_exec_data->ql_qio_meas_steps]= ql_signal_estimate - ql_part_summation;
                             ql_qio_005_exec_data->ql_qio_meas_steps += 1;
                         }
                         else
                         {
                             //Serial.printf("FFT begin\n");
                             ql_signal_estimate = 0;
                             ql_status = ql_estimate_dominant_signal(ql_qio_005_exec_data->ql_qio_part_sensor_meas, QL_QIO_PART_PROCESSING_FFT_SIZE, &ql_signal_estimate);
                             
                             //Serial.printf("FFT end\n");
                             ql_qio_005_exec_data->ql_qio_part_size_est[ql_qio_005_exec_data->ql_qio_meas_steps]= ql_signal_estimate;
                             //Serial.printf("Particle Size estimate:%f\n",ql_signal_estimate*3.3/(1024*QL_QIO_PART_PROCESSING_FFT_SIZE));
                             Serial.printf("Particle Size estimate:%f\n",ql_signal_estimate/(QL_QIO_PART_PROCESSING_FFT_SIZE));
                             /*Update the list*/
                             //TODO!!
                             memset(ql_qio_005_exec_data->ql_qio_part_size_est, 0x00, sizeof(ql_qio_005_exec_data->ql_qio_part_size_est));
                             *(float *)ql_qio_005_exec_data->ql_qio_particle_size = ql_signal_estimate/(QL_QIO_PART_PROCESSING_FFT_SIZE); // Change this later
                             ql_qio_005_exec_data->ql_qio_meas_steps = 0;
                             ql_qcode_timer_stop();
                             ql_init_qcode_timer();
                         }
                         ql_qio_005_exec_data->ql_qio_part_proc_exec_state = QL_PART_PROC_AIR_FLOW_CTRL;
                         break;
                      case QL_QPMEAS_COUNT_MGMT_STATE:
                      case QL_QPMEAS_SWITCH_ON_LED:
                      case QL_QPMEAS_SWITCH_OFF_LED:
                      /**/
                         
                         break;
                      case QL_QPMEAS_START_PROCESSING:
                      case QL_QPMEAS_ERROR:
                         break;
                      default:
                          ;
                 }
#if 0
                 ql_status = ql_qio_PID(ql_qio_005_exec_data->ql_qio_air_flow_set, ql_qio_air_flow_meas, ql_qio_005_exec_data->ql_qio_air_flow_ctrl_pin,  
                                                                                                    &ql_qio_005_exec_data->ql_qio_pid_integral, &ql_qio_005_exec_data->ql_qio_pid_residual_error);
                 if(ql_status != QL_STATUS_OK)
                        return ql_status;
                 else
                        ;
#endif            
                 break;
             default:
                 break;
             
         }


      
#ifdef QL_PRINT_ENABLE
        Serial.printf("Performing Particle Measurements\n");
#endif
#ifdef QL_PRINT_ENABLE
       Serial.printf("Completed!\n");
#endif
    }
#ifdef QL_PRINT_ENABLE
    Serial.printf("Code 05 Executed!\n");
#endif
    return QL_STATUS_OK;
}
STATUS ql_qcode_005_config_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_005_config_size)
{
     unsigned int qblock_inputs = 0;
     unsigned int qblock_outputs = 0;
     unsigned int qblock_number = 0;
     unsigned int qblock_parameters = 0;
     unsigned int qblock_inputs_config_size = 0;
     unsigned int qblock_config_param_size = 0;
     QL_QCODE_CONFIG_BLOCK *ql_qcode_config_block = NULL;
     if((!ql_qcode_config) || (!ql_qcode_005_config_size))
           return QL_ARG_ERROR;
     else
           ;
     *ql_qcode_005_config_size = sizeof(QL_IO_PART_PROC_CTRL_05_CONFIG);
     
     return QL_STATUS_OK;
}


STATUS ql_qio_PID(float ql_qio_ctrl_set_point, float ql_qio_ctrl_process_var, unsigned int ql_qio_ctrl_pin,  float* ql_qio_pid_integral, float* ql_qio_pid_residual_error)
{
    int imax, imin, umin, umax ;
    float Te, Ti, Kp, Td, e, e0,u; 
    STATUS ql_status = 0;
    
    Te=25; //millisecond
   
    Kp=1;
    Ti=1;
    Td=0;
    imin=0;
    imax=400;
    umin=0;
    umax=255;
    

    
    //y=ReadFlow(); // save the actual temp in y
    //Wire.begin(8);
    //Wire.onReceive(receiveEvent);
    e=ql_qio_ctrl_set_point-ql_qio_ctrl_process_var; // calculate the actual error
    
    if(e>100)
       e=*ql_qio_pid_residual_error;
    else
       ;
    *ql_qio_pid_integral = *ql_qio_pid_integral + Ti*Te*e; // upgade the integral values of the PID
    if(*ql_qio_pid_integral<imin)
    {
        *ql_qio_pid_integral=imin;//calculate the good alimentation with all parameters of the PID
    }
    if(*ql_qio_pid_integral>imax)
    {
        *ql_qio_pid_integral=imax;
    }
    u = Kp*(e+(*ql_qio_pid_integral)+Td*(e-(*ql_qio_pid_residual_error))/Te); //calculate the good alimentation with all parameters of the PID
    if(u<umin)
    {
        u=umin;  
    }
    if(u>umax)
    {
        u=umax;
    }
    *ql_qio_pid_residual_error=e; // save the step(n)'s error  to calculate the derivate parameter at the step(n+1)
    analogWrite( ql_qio_ctrl_pin, u );
    //delay(Te);
    //Serial.println(ref);
    return QL_STATUS_OK;
}


STATUS ql_qio_005_read_air_flow(float* ql_qio_air_flow)
{
    int reading;
    Wire.requestFrom(ADR, 2);           // request 2 bytes from slave device (Honeywell flowsensor)
    if (2 <= Wire.available()) {        // if two bytes were received
    reading = Wire.read();            // receive high byte (overwrites previous reading)
    reading = reading << 8;           // shift high byte to be high 8 bits
    reading |= Wire.read();           // receive low byte as lower 8 bits
    }
    else
        return -23;
    float val= reading;

    
    *ql_qio_air_flow=(15*((val/16384)-0.1)/0.8); //équation donnée par le constructeur Full Scale Flow *[((Digital Output Code)/16384)- 0.1]/0.8   
   
    return QL_STATUS_OK; // the function return the values of the flow ( liter per minutes)
   
}
 


STATUS ql_qio_conv_nozzle_size_air_flow(float ql_particle_size,  float* ql_air_flow, QL_QCODE_005_EXEC_DATA *ql_qio_005_exec_data)
{
  //particleSize = particleSize*100;
     if(!ql_qio_005_exec_data)
         return -20;
     else
         ;
     int intParticleSize = int(ql_particle_size*100);
     //nozzleRadius = nozzleRadius*10000;
     
     int intNozzleRadius = int(ql_qio_005_exec_data->ql_qio_nozzle_radius*10000);
     //int paintDensity = 1620;                  //Density of paint to be measured
     //int airViscosity = 181;                   //Viscosity of travel medium in the impactor (generally air)
     float airFlow = ((ql_qio_005_exec_data->ql_qio_air_viscosity/pow(10,7)*13.57168*pow(intNozzleRadius,3)/pow(10,12))/(ql_qio_005_exec_data->ql_qio_paint_density*pow(intParticleSize,2)/pow(10,16)))*60000;//Need to change this Bart code adaptation. Later!
     *ql_air_flow = airFlow;    
     return QL_STATUS_OK;
}
void ql_qcode_005_part_meas_routine(void *ql_qio_exec_data)
{
    QL_QCODE_005_EXEC_DATA *ql_qio_005_exec_data;
    ql_qio_005_exec_data = (QL_QCODE_005_EXEC_DATA *) ql_qio_exec_data;
    int i=0;
    //digitalWrite(ledPower,LOW);
    //delayMicroseconds(samplingTime);
    
    switch(ql_qio_005_exec_data->ql_qio_part_proc_meas_state)
    {
          case QL_QPMEAS_QCODE_RESET:    
                break;
           case QL_QPMEAS_QCODE_WAITING:
                
                break;
           case QL_QPMEAS_START_PROCESSING:
                ql_qio_005_exec_data->ql_qio_part_sensor_idx = 0;
                ql_qio_005_exec_data->ql_qio_part_sensor_ctr = 0;
                ql_qio_005_exec_data->ql_qio_part_proc_meas_state = QL_QPMEAS_SWITCH_ON_LED;
                digitalWrite(21,HIGH);
                pinMode(ql_qio_005_exec_data->ql_qio_part_led_pin, OUTPUT);
                digitalWrite(ql_qio_005_exec_data->ql_qio_part_led_pin,HIGH);
                
                //Serial.printf("Here2\n");
                break;
                //ql_qio_005_exec_data->ql_qio_part_sensor_meas[ql_qio_005_exec_data->ql_qio_part_sensor_idx] = analogRead(ql_qio_005_exec_data->ql_qio_part_sensor_pin);
                //ql_qio_005_exec_data->ql_qio_part_sensor_idx += 1;
           case QL_QPMEAS_SWITCH_ON_LED:
                 //Serial.printf("Led On\n");
                 if(ql_qio_005_exec_data->ql_qio_part_sensor_ctr == QL_QIO_PART_PROC_LED_TIME_COUNTS-1) //No use of mod or div, Its computationally expensive
                      ql_qio_005_exec_data->ql_qio_part_proc_meas_state = QL_QPMEAS_SWITCH_OFF_LED;
                 else
                 {
                      ql_qio_005_exec_data->ql_qio_part_sensor_ctr+=1;
                 }
                break;
           case QL_QPMEAS_SWITCH_OFF_LED:
                //Serial.printf("Led Off\n");
                  
                if(ql_qio_005_exec_data->ql_qio_part_sensor_idx >= QL_QIO_PART_PROC_MEASUREMENT_BUFFER)
                      ql_qio_005_exec_data->ql_qio_part_proc_meas_state = QL_QPMEAS_ERROR;
                else
                      ;
                      //need to change this
                pinMode(ql_qio_005_exec_data->ql_qio_part_sensor_pin, INPUT);
                ql_qio_005_exec_data->ql_qio_part_sensor_meas[ql_qio_005_exec_data->ql_qio_part_sensor_idx] = (float) analogRead(ql_qio_005_exec_data->ql_qio_part_sensor_pin);
                //Serial.printf("%d\n",analogRead(ql_qio_005_exec_data->ql_qio_part_sensor_pin));
                
                //pinMode(16, INPUT);
                //Serial.printf("%d\n",analogRead(16) );
                //Serial.printf("ql_qio_005_exec_data->ql_qio_part_sensor_meas[ql_qio_005_exec_data->ql_qio_part_sensor_idx]:%f,::%d\n",ql_qio_005_exec_data->ql_qio_part_sensor_meas[ql_qio_005_exec_data->ql_qio_part_sensor_idx],ql_qio_005_exec_data->ql_qio_part_sensor_idx);
                ql_qio_005_exec_data->ql_qio_part_sensor_idx += 1;
                
                //pinMode(ql_qio_005_exec_data->ql_qio_part_led_pin, OUTPUT);
                digitalWrite(ql_qio_005_exec_data->ql_qio_part_led_pin,LOW);
                //Serial.printf("Off:%d ",micros());
                ql_qio_005_exec_data->ql_qio_part_sensor_ctr = 0;
                ql_qio_005_exec_data->ql_qio_part_proc_meas_state = QL_QPMEAS_COUNT_MGMT_STATE;
           case QL_QPMEAS_COUNT_MGMT_STATE:
                
                 
                 if(ql_qio_005_exec_data->ql_qio_part_sensor_ctr == QL_QIO_PART_PROC_TIMER_COUNT-1) //No use of mod or div, Its computationally expensive
                 {
                      //Serial.printf("Count Mgmt State\n");
                      ql_qio_005_exec_data->ql_qio_part_sensor_ctr = 0;
                      pinMode(ql_qio_005_exec_data->ql_qio_part_led_pin, OUTPUT);
                      digitalWrite(ql_qio_005_exec_data->ql_qio_part_led_pin,HIGH);
                      //Serial.printf("On:%d \n",micros());
                      ql_qio_005_exec_data->ql_qio_part_proc_meas_state = QL_QPMEAS_SWITCH_ON_LED;
                 }
                 else
                      ql_qio_005_exec_data->ql_qio_part_sensor_ctr+=1;

                 if(ql_qio_005_exec_data->ql_qio_part_sensor_idx == QL_QIO_PART_PROC_MEAS_PER_CYCLE)
                 {
                     
                         //Serial.printf("ql_qio_005_exec_data->ql_qio_part_sensor_meas[ql_qio_005_exec_data->ql_qio_part_sensor_idx]:%f,::%d\n",ql_qio_005_exec_data->ql_qio_part_sensor_meas[ql_qio_005_exec_data->ql_qio_part_sensor_idx-1],ql_qio_005_exec_data->ql_qio_part_sensor_idx);

                     ql_qio_005_exec_data->ql_qio_part_proc_meas_state =  QL_QPMEAS_QCODE_WAITING;
                     ql_qio_005_exec_data->ql_qio_part_sensor_idx = 0;
                     digitalWrite(ql_qio_005_exec_data->ql_qio_part_led_pin,LOW);
                     //Serial.printf("End of Cycle!\n");
                 }
                 else
                      ;//Serial.printf("Here :%d,%d\n",ql_qio_005_exec_data->ql_qio_part_sensor_idx,QL_QIO_PART_PROC_MEAS_PER_CYCLE);
                 break;
           case QL_QPMEAS_ERROR:
                 break;
            default:
                  break;
       }    
       return ;
}

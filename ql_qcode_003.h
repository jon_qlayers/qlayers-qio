
typedef struct ql_qcode_003_DATA {
     unsigned int ql_qcode_output;
     void* ql_qcode_data; /*Incase of scratchpad!*/
} QL_QCODE_003_DATA;

typedef struct ql_qcode_003_EXEC_DATA {
     unsigned int ql_qio_shld_dist_ctrl_reg_type;
     unsigned int ql_qio_shld_dist_ctrl_unit;
     void* ql_qio_shld_distance;
     unsigned char* ql_qio_shld_dist_ctrl_switch;  /*On and Off*/
     unsigned char ql_qio_shld_dist_ctrl_state;
     unsigned int ql_qio_shld_curr_dist;
     unsigned int ql_qio_micro_steps;
     unsigned int ql_qio_mtr_rtn_tmr_cntr;
     unsigned char ql_qio_qcode_003_timer_state;
     unsigned char ql_qio_shld_dist_ctrl_pin; /*ADC Pin*/
     unsigned char ql_qio_shld_dist_mtr_dir_pin;
     unsigned char ql_qio_shld_dist_mtr_step_pin;
     unsigned char ql_qio_shld_dist_mtr_pwr_pin;
} QL_QCODE_003_EXEC_DATA;

typedef struct ql_io_shld_distance_ctrl_03_config {
     unsigned int ql_qio_shld_dist_ctrl_reg_type;
     unsigned int ql_qio_shld_dist_ctrl_unit;
     unsigned int ql_qio_shld_distance;
     
     unsigned int ql_qio_shld_dist_ctrl_switch;
     unsigned int ql_qio_shld_dist_ctrl_pin;
     unsigned int ql_qio_shld_dist_mtr_dir_pin;
     unsigned int ql_qio_shld_dist_mtr_step_pin;
     unsigned int ql_qio_shld_dist_mtr_pwr_pin;
} QL_IO_SHLD_DISTANCE_CTRL_03_CONFIG;


enum {
  QL_DIST_CTRL_RESET = 1,
  QL_DIST_CTRL_CONFIG,
  QL_DIST_CTRL_ACTIVE,
};

enum {
   QL_QTIMER_QCODE_RESET,
   QL_QTIMER_QCODE_WAITING,
   QL_QTIMER_PROCESSING,
   QL_QTIMER_START_PROCESSING,
   QL_QTIMER_ERROR
};

#define QL_QIO_MTR_RTN_TIME   125

#define QL_QIO_SHLD_DISTANCE_CTRL_MODULE_OFF   0x00
#define QL_QIO_SHLD_DISTANCE_CTRL_MODULE_ON   0x01



#define    QL_I2C_MOTOR_X_REV  0x10
    /*Note the cmd should be the third element and should be for both CMD and Poll Cmd*/
//This is only a template of the shortest packet
typedef struct __attribute__((__packed__)) qlayers_i2c_command {
  unsigned char ql_packet_size;
  unsigned char ql_packet_number;
  unsigned char ql_i2c_cmd;
  unsigned char ql_i2c_register_address;
  unsigned char ql_i2c_data;
  unsigned short ql_i2c_crc;
} QL_I2C_CMD;

typedef struct __attribute__((__packed__)) qlayers_i2c_ack {
  unsigned char ql_i2c_packet_size;
  unsigned char ql_packet_number;
  unsigned char ql_i2c_status;
  unsigned short ql_i2c_crc;
} QL_I2C_ACK;

typedef struct __attribute__((__packed__)) qlayers_i2c_poll {
    unsigned char ql_packet_size;
    unsigned char ql_packet_number;
    unsigned char ql_i2c_cmd;
    unsigned char ql_i2c_register_address;
    unsigned short ql_i2c_crc;
} QL_I2C_POLL;

typedef struct __attribute__((__packed__)) qlayers_i2c_poll_ack {
    unsigned char ql_packet_size;
    unsigned char ql_packet_number;
    unsigned char ql_i2c_cmd;
    unsigned short ql_i2c_crc;
} QL_I2C_POLL_ACK;

typedef struct __attribute__((__packed__)) qlayers_i2c_data {
  unsigned char ql_packet_size;
  unsigned char ql_packet_number;
  unsigned char ql_i2c_data;
  unsigned short ql_i2c_crc;
} QL_I2C_DATA;



typedef struct ql_i2c_handle{
     unsigned char* ql_i2c_send_packet;
     unsigned char* ql_i2c_receive_packet;
     unsigned char* ql_i2c_handle_arg;
     unsigned char* ql_i2c_device;
     unsigned char* ql_i2c_write_ack;
     unsigned char ql_i2c_address;
     unsigned char ql_i2c_cmd;
     unsigned char ql_packet_number;
     unsigned char ql_i2c_send_packet_size;
     unsigned char ql_i2c_receive_packet_size;
     unsigned char ql_i2c_register_address;
     unsigned short ql_i2c_crc;
     unsigned char ql_i2c_state;
     unsigned int ql_i2c_retransmits;
     unsigned int ql_i2c_max_retries;
     unsigned int ql_i2c_timeout;
     unsigned char ql_i2c_error;
     unsigned char ql_i2c_slave_exp_rply_sz;
     unsigned int ql_i2c_frame_data;
     int ql_i2c_fd;
} QL_I2C_HANDLE;


typedef struct qlayers_i2c_x_motor_rev {
  unsigned long qlayers_x_motor_rev;
} QLAYERS_I2C_X_M_REV;
typedef struct qlayers_i2c_y_motor_rev {
  unsigned long qlayers_y_motor_rev;
} QLAYERS_I2C_Y_M_REV;
typedef struct qlayers_i2c_z_motor_rev {
  unsigned long qlayers_z_motor_rev;
} QLAYERS_I2C_Z_M_REV;
typedef struct qlayers_i2c_xyz_motor_rev {
  unsigned long qlayers_x_motor_rev;
  unsigned long qlayers_y_motor_rev;
  unsigned long qlayers_z_motor_rev;
} QLAYERS_I2C_XYZ_M_REV;


typedef struct ql_io_device_i2c_uchar
{
    unsigned char ql_i2c_mem_state;
    unsigned char ql_i2c_config_blk;
    unsigned char ql_io_device_uc_reg;
} QL_IO_DEVICE_I2C_UCHAR;

typedef struct ql_io_device_i2c_char
{
    unsigned char ql_i2c_mem_state;
    unsigned char ql_i2c_config_blk;
    char ql_io_device_c_reg;
} QL_IO_DEVICE_I2C_CHAR;

typedef struct ql_io_device_i2c_ushort
{
    unsigned char ql_i2c_mem_state;
    unsigned char ql_i2c_config_blk;
    unsigned short ql_io_device_us_reg;
} QL_IO_DEVICE_I2C_USHORT;

typedef struct ql_io_device_i2c_short
{
    unsigned char ql_i2c_mem_state;
    unsigned char ql_i2c_config_blk;
    short ql_io_device_s_reg;
} QL_IO_DEVICE_I2C_SHORT;

typedef struct ql_io_device_i2c_uint
{
    unsigned char ql_i2c_mem_state;
    unsigned char ql_i2c_config_blk;
    unsigned int ql_io_device_ui_reg;
} QL_IO_DEVICE_I2C_UINT;

typedef struct ql_io_device_i2c_int
{
    unsigned char ql_i2c_mem_state;
    unsigned char ql_i2c_config_blk;
    int ql_io_device_i_reg;
} QL_IO_DEVICE_I2C_INT;

typedef struct ql_io_device_i2c_float
{
    unsigned char ql_i2c_mem_state;
    unsigned char ql_i2c_config_blk;
    float ql_io_device_f_reg;
} QL_IO_DEVICE_I2C_FLOAT;

typedef struct ql_qio_i2c_cmd_mgmt {
  unsigned char ql_qio_i2c_cmd;
  unsigned char ql_qio_i2c_packet_number;
  unsigned int ql_qio_register_address;
  unsigned int ql_qio_read_size;
} QL_QIO_I2C_CMD_MGMT;

typedef struct ql_io_device_i2c_add {
  //QL_IO_DEVICE_I2C_DATA ql_io_device_i2c_data;
   QL_IO_DEVICE_I2C_UCHAR ql_io_device_i2c_uchar;
   QL_IO_DEVICE_I2C_CHAR ql_io_device_i2c_char;
   QL_IO_DEVICE_I2C_USHORT ql_io_device_i2c_ushort;
   QL_IO_DEVICE_I2C_SHORT ql_io_device_i2c_short;
   QL_IO_DEVICE_I2C_UINT ql_io_device_i2c_uint;
   QL_IO_DEVICE_I2C_INT ql_io_device_i2c_int;
   QL_IO_DEVICE_I2C_FLOAT ql_io_device_i2c_float;
} QL_IO_DEVICE_I2C_ADD;




enum {
     QL_I2C_MASTER_READ_RESET=0,
     QL_I2C_MASTER_READ_SEND,
     QL_I2C_MASTER_READ_PROCESSING,
     QL_I2C_MASTER_READ_TIMEOUT,
     QL_I2C_MASTER_READ_CRC_ERROR, 
     QL_I2C_MASTER_READ_COMPLETE,
     QL_I2C_MASTER_WRITE_RESET,
     QL_I2C_MASTER_WRITE_SEND,
     QL_I2C_MASTER_WRITE_TIMEOUT,
     QL_I2C_MASTER_WRITE_FEEDBACK_SEND,
     QL_I2C_MASTER_READ_SLAVE,
     QL_I2C_MASTER_WRITE_SLAVE,
     QL_I2C_MASTER_SLAVE_REPLY_PROCESSING,
     QL_I2C_MASTER_SLAVE_REPLY_CRC_ERROR,

     QL_I2C_MASTER_WRITE_FEEDBACK_READ,
     QL_I2C_MASTER_WRITE_FEEDBACK_PROCESSING,
     QL_I2C_MASTER_WRITE_FEEDBACK_TIMEOUT,
     QL_I2C_MASTER_WRITE_FEEDBACK_COMPLETE,
     QL_I2C_MASTER_WRITE_ERROR,
     QL_I2C_MASTER_WRITE_FEEDBACK_CRC_ERROR,
     

     QL_I2C_MASTER_WRITE_COMPLETE
} QL_I2C_MASTER_STATES;


#define QL_I2C_ADDRESS_SPACE   256
#define QL_QIO_I2C_ADDRESS   0x59


#define QL_I2C_CMD_UNSIGNED_CHAR_REGISTER_READ          0
#define QL_I2C_CMD_UNSIGNED_CHAR_REGISTER_WRITE         1
#define QL_I2C_CMD_SIGNED_CHAR_REGISTER_READ            2
#define QL_I2C_CMD_SIGNED_CHAR_REGISTER_WRITE           3
#define QL_I2C_CMD_UNSIGNED_SHORT_REGISTER_READ         4
#define QL_I2C_CMD_UNSIGNED_SHORT_REGISTER_WRITE        5
#define QL_I2C_CMD_SIGNED_SHORT_REGISTER_READ           6
#define QL_I2C_CMD_SIGNED_SHORT_REGISTER_WRITE          7



#define QL_I2C_CMD_UNSIGNED_INT_REGISTER_READ           8
#define QL_I2C_CMD_UNSIGNED_INT_REGISTER_WRITE          9
#define QL_I2C_CMD_SIGNED_INT_REGISTER_READ             10
#define QL_I2C_CMD_SIGNED_INT_REGISTER_WRITE            11
#define QL_I2C_CMD_FLOAT_REGISTER_READ                  12
#define QL_I2C_CMD_FLOAT_REGISTER_WRITE                 13
#define QL_I2C_CMD_POLL_ACK                             14
#define QL_I2C_CMD_WRITE_ACK                            15



#define QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE       0x01
#define QL_IO_DEVICE_SIGNED_CHAR_ADDRESS_TYPE         0x02
#define QL_IO_DEVICE_UNSIGNED_SHORT_ADDRESS_TYPE      0x03
#define QL_IO_DEVICE_SIGNED_SHORT_ADDRESS_TYPE        0x04
#define QL_IO_DEVICE_UNSIGNED_INT_ADDRESS_TYPE        0x05
#define QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE          0x06
#define QL_IO_DEVICE_FLOAT_ADDRESS_TYPE               0x07

STATUS ql_qio_i2c_get_cmd_mgmt(QL_QIO_I2C_CMD_MGMT **ql_qio_I2C_cmd_mgmt);
STATUS ql_qio_i2c_generate_write_ack_frame(unsigned char* ql_i2c_read_frame, unsigned char *ql_qio_i2c_frame_size, unsigned char ql_i2c_packet_number);

enum 
{ 
   QL_IO_DEVICE_MEM_BLK_FREE = 0,
   QL_IO_DEVICE_MEM_BLK_OCCUPIED
};
enum 
{
   QL_IO_DEVICE_MEM_UNIT_UNLOCKED = 0,
   QL_IO_DEVICE_MEM_UNIT_LOCKED
};

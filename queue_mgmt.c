/**************************************************************************************************************************************
Copyright - QLAYERS.B.V

This File is a confidential property of QLayers B.V. Any redistribution or use without prior legal contract will be a legal concern. 

queue_mgmt.c - This file contains the run time logic of Queues and its management between different threads.

Revision - Version x.x - Author
***************************************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ql_qio_error.h"
#include "ql_types.h"
#include "queue_mgmt.h"
  




static QUEUE_BLOCK *ql_queue=NULL;
static QUEUE_BLOCK ql_queue_blocks[QL_DEFAULT_QUEUE_BLOCKS];
static QUEUE_STATUS ql_queue_mgmt_status;

/*Initialize the Queue memory blocks and the counters*/
 STATUS ql_queue_mem_init (void)
{
     QL_UINT i = 0; 
     if(ql_queue != NULL)
        return QL_QUEUE_INIT_ERROR;
     else
        ;
     for (i=0; i<QL_DEFAULT_QUEUE_BLOCKS; i++)
     {
         ql_queue_blocks[i].queue_next_block = ql_queue;
         /*Set the block to point at the previously inserted queue*/
         memset( &ql_queue_blocks[i].queue_mem_buff[0],0, sizeof(ql_queue_blocks[i].queue_mem_buff));
        /*Clear the memory*/ 
	
        /*Inset into the queue!*/
        ql_queue = &ql_queue_blocks[i];
         
     }
     ql_queue_mgmt_status.queue_blocks_pool = i;
     ql_queue_mgmt_status.queue_allocated = 0;
     ql_queue_mgmt_status.queue_mgmt_error_status = QL_QUEUE_STATUS_OK;
     return QL_STATUS_OK;
}

STATUS queue_get_block(QUEUE_BLOCK** queue_block)
{
     if(queue_block == NULL)
         return QL_GET_QUEUE_BLOCK_PARAM_ERROR;
     else
         ;
     if(ql_queue == NULL)
     {
         /*Queue Blocks have been allocated! No memory Left!*/
         return QL_GET_QUEUE_BLOCK_MEMORY_ERROR;
     }
     else
         ;
     //TODO!! LOCK with semaphore!
     
     *queue_block = (QUEUE_BLOCK*)ql_queue;
     printf("Debug:%p\n", queue_block);
     ql_queue = ql_queue->queue_next_block;
     ql_queue_mgmt_status.queue_allocated++;
     ql_queue_mgmt_status.queue_blocks_pool--;
     //Unlock!
     return QL_STATUS_OK;
}

STATUS queue_release_block(QUEUE_BLOCK* queue_block)
{
    if(queue_block == NULL)
         return QL_RELEASE_QUEUE_BLOCK_PARAM_ERROR;
     else
         ;
     if( ql_queue_mgmt_status.queue_allocated)
          ; /*There is a previous allocation*/
     else
     {
         return QL_QUEUE_RELEASE_BLOCK_INVALID_USAGE; /*There is no previous allocation and yet this API has been called*/
     }
     //TODO!! LOCK with semaphore!
     /*Clear the memory before putting them into the pool*/
     memset(queue_block, 0, sizeof(QUEUE_BLOCK));
     queue_block->queue_next_block = ql_queue;
     ql_queue = queue_block;
     ql_queue_mgmt_status.queue_allocated--;
     ql_queue_mgmt_status.queue_blocks_pool++;
     //Unlock!
     return QL_STATUS_OK;
}

/*The following blocks can be used as unit test blocks for unit testing*/

STATUS ql_queue_print_statistics(void)
{
    printf("Pool:%d, ", ql_queue_mgmt_status.queue_blocks_pool);
    printf("Allocated:%d, ", ql_queue_mgmt_status.queue_allocated);
    printf("Queue Status:%d\n", ql_queue_mgmt_status.queue_mgmt_error_status);
    return QL_STATUS_OK;
}


static void test_queue_mgmt_1(void)
{
      STATUS status;
      status = ql_queue_mem_init();
      if(status != QL_STATUS_OK)
           printf("Queue Init Error. Status:%d.\n", status);
      else
           printf("Queue Initialization Successfully Tested.\n");
      printf("Pool:%d, ", ql_queue_mgmt_status.queue_blocks_pool);
      printf("Allocated:%d, ", ql_queue_mgmt_status.queue_allocated);
      printf("Queue Status:%d\n", ql_queue_mgmt_status.queue_mgmt_error_status);

      return;
}

static void test_queue_mgmt_2(void)
{
      STATUS status;
      QUEUE_BLOCK* queue_block;

      status = queue_get_block(&queue_block);
      if(status != QL_STATUS_OK)
           printf("Queue Get Block Error. Status:%d.\n", status);
      else
           printf("Queue Get Block Successfully Tested.\n");
      printf("Pool:%d, ", ql_queue_mgmt_status.queue_blocks_pool);
      printf("Allocated:%d, ", ql_queue_mgmt_status.queue_allocated);
      printf("Queue Status:%d\n", ql_queue_mgmt_status.queue_mgmt_error_status);

      status = queue_release_block(queue_block);
      if(status != QL_STATUS_OK)
           printf("Queue Release Block Error. Status:%d.\n", status);
      else
           printf("Queue Release Block Successfully Tested.\n");
      printf("Pool:%d, ", ql_queue_mgmt_status.queue_blocks_pool);
      printf("Allocated:%d, ", ql_queue_mgmt_status.queue_allocated);
      printf("Queue Status:%d\n", ql_queue_mgmt_status.queue_mgmt_error_status);
  
      return;
}

static void test_queue_mgmt_3(void)
{
      STATUS status;
      QUEUE_BLOCK* queue_block[QL_DEFAULT_QUEUE_BLOCKS];
      QL_UINT i = 0;
      printf("Test 3\n");
      //QL_DEFAULT_QUEUE_BLOCKS-1
      for (i=0;i< QL_DEFAULT_QUEUE_BLOCKS-1; i++)
      {
          status = queue_get_block(( QUEUE_BLOCK**) &queue_block[i]);
          printf("status:%d, %p\n", QL_STATUS_OK, queue_block[i]);
      }
      //return;
      status = queue_get_block( (QUEUE_BLOCK**) &queue_block[i]);
      if(status != QL_STATUS_OK)
            printf("Queue Get Block Error. Status:%d.\n", status);
      else
            printf("Queue Get Block Successfully Tested.\n");
      printf("Pool:%d, ", ql_queue_mgmt_status.queue_blocks_pool);
      printf("Allocated:%d, ", ql_queue_mgmt_status.queue_allocated);
      printf("Queue Status:%d\n", ql_queue_mgmt_status.queue_mgmt_error_status);
     
      /*Must give an error!*/
      status = queue_get_block( (QUEUE_BLOCK**) &queue_block[i]);
      if(status != QL_STATUS_OK)
            printf("Queue Get Block Error. Status:%d.\n", status);
      else
            printf("Queue Get Block Successfully Tested.\n");
      printf("Pool:%d, ", ql_queue_mgmt_status.queue_blocks_pool);
      printf("Allocated:%d, ", ql_queue_mgmt_status.queue_allocated);
      printf("Queue Status:%d\n", ql_queue_mgmt_status.queue_mgmt_error_status);
      

      /*Release the blocks*/
      for (i=0;i< QL_DEFAULT_QUEUE_BLOCKS; i++)
      {
         status = queue_release_block((QUEUE_BLOCK*) queue_block[i]);
         if(status != QL_STATUS_OK)
             printf("Queue Release Block Error. Status:%d.\n", status);
         else
             printf("Queue Release Block Successfully Tested.\n");
      }
      printf("Pool:%d, ", ql_queue_mgmt_status.queue_blocks_pool);
      printf("Allocated:%d, ", ql_queue_mgmt_status.queue_allocated);
      printf("Queue Status:%d\n", ql_queue_mgmt_status.queue_mgmt_error_status);

      return;
}
static void test_queue_mgmt_4(void)
{
      STATUS status;
      QUEUE_BLOCK* queue_block[QL_DEFAULT_QUEUE_BLOCKS];
      QL_UINT i = 0;
      printf("Test 3\n");
      //QL_DEFAULT_QUEUE_BLOCKS-1
      for (i=0;i< QL_DEFAULT_QUEUE_BLOCKS-1; i++)
      {
          status = queue_get_block(( QUEUE_BLOCK**) &queue_block[i]);
          printf("status:%d, %p\n", QL_STATUS_OK, queue_block[i]);
      }
      return;
}
/*
void main(void)
{
    //This main is a stub!
    // Used during development to text the modules. Need to integrate the test modules into test suite.
    test_queue_mgmt_1();
    test_queue_mgmt_2(); 
    test_queue_mgmt_3(); 
    test_queue_mgmt_4();
    return;
}*/



/**************************************************************************************************************************************
Copyright - QLAYERS.B.V

This File is a confidential property of QLayers B.V. Any redistribution or use without prior legal contract will be a legal concern. 

queue_mgmt.c - This file contains the run time logic of Queues and its management between different threads.

Revision - Version x.x - Author
***************************************************************************************************************************************/
#include<SPI.h>
#include <stdlib.h>
#include "ql_types.h"
#include "ql_i2c.h"

// set pin 10 as the slave select for the digital pot:
const int slaveSelectPin = 10;

void ql_qio_init_peripherals(void)
{
    pinMode (slaveSelectPin, OUTPUT);
    pinMode(23,OUTPUT);
    pinMode(22,OUTPUT);
    pinMode(21,OUTPUT);
    pinMode(20,OUTPUT);
    
    digitalWrite (slaveSelectPin, HIGH);
 // initialize SPI:
    SPI.begin();
    
    digitalWrite(23,LOW);
    digitalWrite(22,LOW);
    digitalWrite(21,LOW);
    digitalWrite(20,LOW);

}


void digitalPotWrite(int address, int value) {
 // gain control of the SPI port
 // and configure settings
 SPI.beginTransaction(SPISettings(4000000, MSBFIRST, SPI_MODE0));
 // take the SS pin low to select the chip:
 digitalWrite(slaveSelectPin,LOW);
 //  send in the address and value via SPI:
 SPI.transfer(address);
 SPI.transfer(value);
 // take the SS pin high to de-select the chip:
 digitalWrite(slaveSelectPin,HIGH);
 // release control of the SPI port
 SPI.endTransaction();
}

/**************************************************************************************************************************************
Copyright - QLAYERS.B.V

This File is a confidential property of QLayers B.V. Any redistribution or use without prior legal contract will be a legal concern. 

queue_mgmt.c - This file contains the run time logic of Queues and its management between different threads.

Revision - Version x.x - Author
***************************************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ql_qio_error.h"
#include "ql_types.h"
#include "ql_qcode.h"
#include "ql_i2c.h"
#include "ql_qcode_004.h"
//#include <i2c_t3.h>
#include <Arduino.h>
//#include <HardwareSerial.h>
//#include "ql_debug.h"


extern STATUS ql_qio_i2c_allocate_address(unsigned int ql_qio_block_no, unsigned int ql_qio_i2c_address_type, unsigned int ql_qio_i2c_address_req, void **ql_qio_i2c_address_ret);


/*Motor Control**/
STATUS ql_qcode_004_config_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_004_config_size)
{
     if((!ql_qcode_config) || (!ql_qcode_004_config_size))
           return QL_ARG_ERROR;
     else
           ;
     *ql_qcode_004_config_size = sizeof(QL_IO_MOTOR_CTRL_04_CONFIG);
     
     return QL_STATUS_OK;
}

STATUS ql_qcode_004_parse_config(unsigned int *parse_next_block, unsigned int** parse_next_block_add, QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit)
{
     STATUS status = QL_STATUS_OK;    
     unsigned int ql_i2c_reg_type;
     unsigned int ql_mtr_rev;
     unsigned int ql_mtr_rev_rate;
     unsigned int ql_mtr_rev_update;
     unsigned int ql_mtr_trigger;
     void* ql_i2c_data_address;
     unsigned int ql_mtr_unit = 0;
     
     QL_QCODE_004_EXEC_DATA *ql_qio_004_exec_data;
    
     QL_IO_MOTOR_CTRL_04_CONFIG* ql_io_mtr_ctrl_04_config;
     QL_IO_DEVICE_CONFIG_BLK* ql_io_device_config_blk;

     ql_qio_004_exec_data = (QL_QCODE_004_EXEC_DATA*) ql_qcode_exec_unit->ql_qcode_exec_data;
     ql_io_device_config_blk = (QL_IO_DEVICE_CONFIG_BLK*) parse_next_block;

     
     ql_io_mtr_ctrl_04_config = (QL_IO_MOTOR_CTRL_04_CONFIG *) &ql_io_device_config_blk->ql_qio_device;
     ql_i2c_reg_type = ql_io_mtr_ctrl_04_config->ql_i2c_reg_type;
     ql_mtr_rev = ql_io_mtr_ctrl_04_config->ql_mtr_rev;
     ql_mtr_rev_rate = ql_io_mtr_ctrl_04_config->ql_mtr_rev_rate;
     ql_mtr_rev_update = ql_io_mtr_ctrl_04_config->ql_mtr_rev_update;
     ql_mtr_unit = ql_io_mtr_ctrl_04_config->ql_mtr_unit;
     ql_mtr_trigger = ql_io_mtr_ctrl_04_config->ql_mtr_trigger;     
     ql_qio_004_exec_data->ql_qio_i2c_data_type = ql_i2c_reg_type;
     switch (ql_i2c_reg_type)
     {
            case QL_QCODE_UNSIGNED_INT:
            case QL_QCODE_INT:
            case QL_QCODE_FLOAT:
            case QL_QCODE_UNSIGNED_SHORT:
            case QL_QCODE_SHORT:
            case QL_QCODE_UNSIGNED_CHAR:
            case QL_QCODE_CHAR:
                 break;
            default :
#ifdef QL_PRINT_ENABLE 
                 Serial.printf("qblock_data_type:%d\n", ql_i2c_reg_type);
#endif
                 return QL_QCODE_INVALID_DATA_TYPE;
     }

     /*Allocate the memory for the i2c data interface*/
     status = ql_qio_i2c_allocate_address(ql_io_device_config_blk->ql_qio_block_no, ql_i2c_reg_type,ql_mtr_rev, &ql_i2c_data_address); 
     if(status != QL_STATUS_OK) 
         return status;
     else
         ;

     ql_qio_004_exec_data->ql_qio_mtr_unit = ql_mtr_unit;
     ql_qio_004_exec_data->ql_mtr_rev = ql_i2c_data_address;
     //Cant use an OR on the status because the error codes are not one hot encoding!
     status = ql_qio_i2c_allocate_address(ql_io_device_config_blk->ql_qio_block_no ,ql_i2c_reg_type,ql_mtr_rev_rate, &ql_i2c_data_address); 
     if(status != QL_STATUS_OK)
         return status;
     else
         ;
     ql_qio_004_exec_data->ql_mtr_rev_rate = ql_i2c_data_address;
     
     status = ql_qio_i2c_allocate_address(ql_io_device_config_blk->ql_qio_block_no ,ql_i2c_reg_type,ql_mtr_rev_update, &ql_i2c_data_address);     
     if(status != QL_STATUS_OK)
         return status;
     else
         ;
     ql_qio_004_exec_data->ql_mtr_rev_update = ql_i2c_data_address;

     status = ql_qio_i2c_allocate_address(ql_io_device_config_blk->ql_qio_block_no ,ql_i2c_reg_type,ql_mtr_trigger, &ql_i2c_data_address);     
     if(status != QL_STATUS_OK)
         return status;
     else
         ;
     ql_qio_004_exec_data->ql_mtr_trigger = ql_i2c_data_address;

     *parse_next_block_add = parse_next_block + (sizeof(QL_IO_MOTOR_CTRL_04_CONFIG)/(sizeof(unsigned int))) + (sizeof(QL_IO_DEVICE_CONFIG_BLK) - sizeof(ql_io_device_config_blk->ql_qio_device))/(sizeof(unsigned int));

     return QL_STATUS_OK;
}

STATUS ql_qcode_004_allocate_exec_data_mem(void** ql_qcode_004_data_mem)
{
     void *ql_qcode_data_mem = NULL;
     if(!ql_qcode_004_data_mem)
          return QL_ARG_ERROR;
     else
          ;
     
     ql_qcode_data_mem = malloc(sizeof(QL_QCODE_004_EXEC_DATA));
     if(! ql_qcode_data_mem)
          return -1;
     else
          ;
     memset(ql_qcode_data_mem, 0x00, sizeof(QL_QCODE_004_EXEC_DATA));
     *ql_qcode_004_data_mem = ql_qcode_data_mem;
     return QL_STATUS_OK;
}

/*Float is not supported at the moment!*/





STATUS  ql_qcode_004_execute_unit(QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit)
{
    
    int ql_mtr_rev = 0;
    int ql_mtr_rev_rate = 0;
    int ql_mtr_trigger = 0;
    unsigned int ql_mtr_step_pin = 0;
    unsigned int ql_mtr_dir_pin = 0;
    
    QL_QCODE_004_EXEC_DATA *ql_qio_004_exec_data;
    
    if(!ql_qcode_exec_unit)
         return -2;
    else
         ;
   
    ql_qio_004_exec_data = (QL_QCODE_004_EXEC_DATA*) ql_qcode_exec_unit->ql_qcode_exec_data;
    switch(ql_qio_004_exec_data->ql_qio_i2c_data_type)
    {
         case QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE:
                ql_mtr_rev = (int) *((unsigned char *)ql_qio_004_exec_data->ql_mtr_rev);
                ql_mtr_rev_rate = (int) *((unsigned char *)ql_qio_004_exec_data->ql_mtr_rev_rate);
                ql_mtr_trigger = (int) *((unsigned char *)ql_qio_004_exec_data->ql_mtr_trigger);
                if(ql_mtr_trigger != 0)
                {
                     *(unsigned char *)ql_qio_004_exec_data->ql_mtr_rev = 0;
                     *((unsigned char *)ql_qio_004_exec_data->ql_mtr_rev_rate) = 0;
                     *((unsigned char *)ql_qio_004_exec_data->ql_mtr_trigger) = 0;
                }
                else
                     ;
 
                break;
         case QL_IO_DEVICE_SIGNED_CHAR_ADDRESS_TYPE:
                ql_mtr_rev = *(char *)ql_qio_004_exec_data->ql_mtr_rev;
                ql_mtr_rev_rate = *(char *)ql_qio_004_exec_data->ql_mtr_rev_rate;
                ql_mtr_trigger = *((char *)ql_qio_004_exec_data->ql_mtr_trigger);
                if(ql_mtr_trigger != 0)
                {
                     *(char *)ql_qio_004_exec_data->ql_mtr_rev = 0;
                     *(char *)ql_qio_004_exec_data->ql_mtr_rev_rate = 0;
                     *((char *)ql_qio_004_exec_data->ql_mtr_trigger) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_UNSIGNED_SHORT_ADDRESS_TYPE:
                ql_mtr_rev = *(unsigned short *)ql_qio_004_exec_data->ql_mtr_rev;
                ql_mtr_rev_rate = *(unsigned short *)ql_qio_004_exec_data->ql_mtr_rev_rate;
                ql_mtr_trigger = *((unsigned short *)ql_qio_004_exec_data->ql_mtr_trigger);
                if(ql_mtr_trigger != 0)
                {
                     *(unsigned short*)ql_qio_004_exec_data->ql_mtr_rev = 0;
                     *(unsigned short *)ql_qio_004_exec_data->ql_mtr_rev_rate = 0;
                     *((unsigned short *)ql_qio_004_exec_data->ql_mtr_trigger) = 0;
                }
                else
                     ;
 
                break;
         case QL_IO_DEVICE_SIGNED_SHORT_ADDRESS_TYPE:
                ql_mtr_rev = *(short *)ql_qio_004_exec_data->ql_mtr_rev;
                ql_mtr_rev_rate = *(short *)ql_qio_004_exec_data->ql_mtr_rev_rate;
                ql_mtr_trigger = *((short *)ql_qio_004_exec_data->ql_mtr_trigger);
                if(ql_mtr_trigger != 0)
                {
                     *(short *)ql_qio_004_exec_data->ql_mtr_rev = 0;
                     *(short *)ql_qio_004_exec_data->ql_mtr_rev_rate = 0;
                     *((short *)ql_qio_004_exec_data->ql_mtr_trigger) = 0;
                }
                else
                     ;
 
                break;
         case QL_IO_DEVICE_UNSIGNED_INT_ADDRESS_TYPE:
                ql_mtr_rev = *(unsigned int *)ql_qio_004_exec_data->ql_mtr_rev;
                ql_mtr_rev_rate = *(unsigned int *)ql_qio_004_exec_data->ql_mtr_rev_rate;
                ql_mtr_trigger = *((unsigned int *)ql_qio_004_exec_data->ql_mtr_trigger);
                if(ql_mtr_trigger != 0)
                {
                     *(unsigned int *)ql_qio_004_exec_data->ql_mtr_rev = 0;
                     *(unsigned int *)ql_qio_004_exec_data->ql_mtr_rev_rate = 0;
                     *((unsigned int *)ql_qio_004_exec_data->ql_mtr_trigger) = 0;
                }
                else
                     ;

                break;
         case QL_IO_DEVICE_SIGNED_INT_ADDRESS_TYPE:
                ql_mtr_rev = *(int *)ql_qio_004_exec_data->ql_mtr_rev;
                ql_mtr_rev_rate = *(int *)ql_qio_004_exec_data->ql_mtr_rev_rate;
                ql_mtr_trigger = *((int *)ql_qio_004_exec_data->ql_mtr_trigger);
                if(ql_mtr_trigger != 0)
                {
                     *(int *)ql_qio_004_exec_data->ql_mtr_rev = 0;
                     *(int *)ql_qio_004_exec_data->ql_mtr_rev_rate = 0;
                     *((int *)ql_qio_004_exec_data->ql_mtr_trigger) = 0;
                }
                else
                     ;
                break;
         case QL_IO_DEVICE_FLOAT_ADDRESS_TYPE:             
         default :
             return -30;
    }
    /*We dont expect the rev to be too much for now (1st version)*/
#ifdef QL_PRINT_ENABLE
    Serial.printf("ql_qio_mtr_unit:%d, ql_mtr_rev:%d, ql_mtr_rev_rate:%d, ql_mtr_trigger:%d! ",ql_qio_004_exec_data->ql_qio_mtr_unit, ql_mtr_rev,ql_mtr_rev_rate, ql_mtr_trigger);
#endif
    if(ql_mtr_trigger)
    {
#ifdef QL_PRINT_ENABLE
        Serial.printf("\nStarting the Motor Actuation on Motor :%d...\n",ql_qio_004_exec_data->ql_qio_mtr_unit );
#endif
        switch(ql_qio_004_exec_data->ql_qio_mtr_unit)
        {
            case 1:
                 ql_mtr_step_pin = QL_MTR_01_STEP_PIN;
                 ql_mtr_dir_pin = QL_MTR_01_DIR_PIN;
            break;
            case 2:
                 ql_mtr_step_pin = QL_MTR_02_STEP_PIN;
                 ql_mtr_dir_pin = QL_MTR_02_DIR_PIN;
            break;
            default:
#ifdef QL_PRINT_ENABLE
                Serial.printf("Undefined Motor Unit!\n");
#endif
              return -5;
        }
        pinMode(ql_mtr_step_pin,OUTPUT);
        pinMode(ql_mtr_dir_pin,OUTPUT);
        
        digitalWrite(ql_mtr_dir_pin,HIGH); // Enables the motor to move in a particular direction
        // Makes 200 pulses for making one full cycle rotation
        for(int x = 0; x < 400*ql_mtr_rev; x++) 
        {
             digitalWrite(ql_mtr_step_pin,HIGH);
             delayMicroseconds(20000/ql_mtr_rev_rate);
             digitalWrite(ql_mtr_step_pin,LOW);
             delayMicroseconds(20000/ql_mtr_rev_rate);
        }
#ifdef QL_PRINT_ENABLE  
       Serial.printf("Completed!\n");
#endif
    }
    else
        ;
#ifdef QL_PRINT_ENABLE
    Serial.printf("Code 04 Executed!\n");
#endif
    return QL_STATUS_OK;
}


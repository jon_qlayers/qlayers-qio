/**************************************************************************************************************************************
Copyright - QLAYERS.B.V

This File is a confidential property of QLayers B.V. Any redistribution or use without prior legal contract will be a legal concern. 

queue_mgmt.c - This file contains the run time logic of Queues and its management between different threads.

Revision - Version x.x - Author
***************************************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SPI.h>
#include "ql_qio_error.h"
#include "ql_types.h"
#include "ql_qcode.h"
#include "ql_i2c.h"
#include "ql_qcode_001.h"
#include <Arduino.h>


extern void digitalPotWrite(int address, int value);


extern STATUS ql_qio_i2c_allocate_address(unsigned int ql_qio_block_no, unsigned int ql_qio_i2c_address_type, unsigned int ql_qio_i2c_address_req, void **ql_qio_i2c_address_ret);

/**Temperature Control**/


STATUS ql_qcode_001_parse_config(unsigned int *parse_next_block, unsigned int** parse_next_block_add, QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit)
{
     unsigned int i = 0, j = 0;
     unsigned int ql_data_index = 0;
     STATUS status = QL_STATUS_OK;
     unsigned int ql_qio_block_no; //device type
     unsigned int ql_qio_block_type; //functionality
     
     unsigned int ql_i2c_reg_type;
     unsigned int ql_qio_temp_reg_type;
     unsigned int ql_qio_temp_set;
     unsigned int ql_qio_temp_switch;
     void* ql_i2c_data_address;
     unsigned int ql_qio_temp_unit = 0;
     
     QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit_001 = NULL;
     
     QL_QCODE_001_EXEC_DATA *ql_qio_001_exec_data;
    
     QL_IO_TEMP_CTRL_01_CONFIG* ql_io_temp_ctrl_01_config;
     QL_IO_DEVICE_CONFIG_BLK* ql_io_device_config_blk;

     unsigned int ql_qcode_config_dynm_size = 0; 

     ql_qio_001_exec_data = (QL_QCODE_001_EXEC_DATA*) ql_qcode_exec_unit->ql_qcode_exec_data;
     ql_io_device_config_blk = (QL_IO_DEVICE_CONFIG_BLK*) parse_next_block;

     
     ql_io_temp_ctrl_01_config = (QL_IO_TEMP_CTRL_01_CONFIG *) &ql_io_device_config_blk->ql_qio_device;
     ql_qio_temp_reg_type = ql_io_temp_ctrl_01_config->ql_qio_temp_reg_type;
     ql_qio_temp_set = ql_io_temp_ctrl_01_config->ql_qio_temp_set;
     ql_qio_temp_switch = ql_io_temp_ctrl_01_config->ql_qio_temp_switch;
     
     ql_qio_temp_unit = ql_io_temp_ctrl_01_config->ql_qio_temp_unit;
     ql_qio_001_exec_data->ql_qio_temp_reg_type = ql_qio_temp_reg_type;
     
     switch (ql_qio_temp_reg_type)
     {
            case QL_QCODE_FLOAT:
            case QL_QCODE_UNSIGNED_CHAR:
            case QL_QCODE_CHAR:
                 break;
            default :
#ifdef QL_PRINT_ENABLE 
                 Serial.printf("qblock_data_type:%d\n", ql_qio_temp_reg_type);
#endif
                 return QL_QCODE_INVALID_DATA_TYPE;
     }
     ql_qio_001_exec_data->ql_qio_temp_unit = ql_qio_temp_unit;
     /*Allocate the memory for the i2c data interface*/
     status = ql_qio_i2c_allocate_address(ql_io_device_config_blk->ql_qio_block_no, ql_qio_temp_reg_type,ql_qio_temp_set, &ql_i2c_data_address); 
     if(status != QL_STATUS_OK) 
         return status;
     else
         ;

     
     ql_qio_001_exec_data->ql_qio_temp_set = ql_i2c_data_address;

     status = ql_qio_i2c_allocate_address(ql_io_device_config_blk->ql_qio_block_no, ql_qio_temp_reg_type, ql_io_temp_ctrl_01_config->ql_qio_temp_fb, &ql_i2c_data_address); 
     if(status != QL_STATUS_OK) 
         return status;
     else
         ;

     ql_qio_001_exec_data->ql_qio_temp_fb = ql_i2c_data_address;
     //Cant use an OR on the status because the error codes are not one hot encoding!
     status = ql_qio_i2c_allocate_address(ql_io_device_config_blk->ql_qio_block_no ,QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE,ql_qio_temp_switch, &ql_i2c_data_address); 
     if(status != QL_STATUS_OK)
         return status;
     else
         ;
     ql_qio_001_exec_data->ql_qio_temp_switch = (unsigned char *) ql_i2c_data_address;
     
     /*Pins*/
     ql_qio_001_exec_data->ql_qio_temp_thermistor_pin = ql_io_temp_ctrl_01_config->ql_qio_temp_thermistor_pin;
     ql_qio_001_exec_data->ql_qio_temp_heater_pin = ql_io_temp_ctrl_01_config->ql_qio_temp_heater_pin;
     
     /*Change it later*/
     Serial.printf("ql_qio_001_exec_data->ql_qio_temp_thermistor_pin:%d\n",ql_qio_001_exec_data->ql_qio_temp_thermistor_pin);
     Serial.printf("ql_qio_001_exec_data->ql_qio_temp_heater_pin:%d\n",ql_qio_001_exec_data->ql_qio_temp_heater_pin);
     analogReference(EXTERNAL);
     //pinMode(ql_qio_001_exec_data->ql_qio_temp_thermistor_pin,INPUT);
     //pinMode(ql_qio_001_exec_data->ql_qio_temp_heater_pin, OUTPUT);  
     pinMode(0,OUTPUT); 
     pinMode(1,OUTPUT); 
     pinMode(2,OUTPUT); 
     
     SPI.begin();
     int channel = 2;
     int level= 255;
     digitalPotWrite(channel, level);
     
     *parse_next_block_add = parse_next_block + (sizeof(QL_IO_TEMP_CTRL_01_CONFIG)/(sizeof(unsigned int))) + (sizeof(QL_IO_DEVICE_CONFIG_BLK) - sizeof(ql_io_device_config_blk->ql_qio_device))/(sizeof(unsigned int));

     return QL_STATUS_OK;
}

STATUS ql_qcode_001_allocate_exec_data_mem(void** ql_qcode_001_data_mem)
{
     void *ql_qcode_data_mem = NULL;
     if(!ql_qcode_001_data_mem)
          return QL_ARG_ERROR;
     else
          ;
     
     ql_qcode_data_mem = malloc(sizeof(QL_QCODE_001_EXEC_DATA));
     if(! ql_qcode_data_mem)
          return -1;
     else
          ;
     memset(ql_qcode_data_mem, 0x00, sizeof(QL_QCODE_001_EXEC_DATA));
     *ql_qcode_001_data_mem = ql_qcode_data_mem;
     return QL_STATUS_OK;
}

/*Float is not supported at the moment!*/



/*Switch is always of type Char*/

STATUS  ql_qcode_001_execute_unit(QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit)
{
    
    float ql_qio_temp_set;
    unsigned int ql_qio_temp_switch;    
    QL_QCODE_001_EXEC_DATA *ql_qio_001_exec_data;
    uint8_t i;
    float average;
    
    if(!ql_qcode_exec_unit)
         return -2;
    else
         ;
    
    ql_qio_001_exec_data = (QL_QCODE_001_EXEC_DATA*) ql_qcode_exec_unit->ql_qcode_exec_data;
    switch(ql_qio_001_exec_data->ql_qio_temp_reg_type)
    {
         case QL_IO_DEVICE_UNSIGNED_CHAR_ADDRESS_TYPE:
                ql_qio_temp_set = (float) *((unsigned char *)ql_qio_001_exec_data->ql_qio_temp_set);
                ql_qio_temp_switch = (int) *((unsigned char *)ql_qio_001_exec_data->ql_qio_temp_switch);
                if(ql_qio_temp_switch != QL_QIO_TEMPERATURE_MODULE_ON)
                {
                     *(unsigned char *)ql_qio_001_exec_data->ql_qio_temp_set = 0;
                     *((unsigned char *)ql_qio_001_exec_data->ql_qio_temp_switch) = 0;
                }
                else
                     ;
                
                
                break;
         case QL_IO_DEVICE_FLOAT_ADDRESS_TYPE: 
                ql_qio_temp_set =  *((float *)ql_qio_001_exec_data->ql_qio_temp_set);
                ql_qio_temp_switch = (int) *((unsigned char *)ql_qio_001_exec_data->ql_qio_temp_switch);
                if(ql_qio_temp_switch != QL_QIO_TEMPERATURE_MODULE_ON)
                {
                     *(float *)ql_qio_001_exec_data->ql_qio_temp_set = 0;
                     *((unsigned char *)ql_qio_001_exec_data->ql_qio_temp_switch) = 0;
                }
                else
                     ;
                break;            
         default :
             
             return -30;
    }
    /*We dont expect the rev to be too much for now (1st version)*/
#ifdef QL_PRINT_ENABLE
    Serial.printf("ql_qio_temp_unit:%d, ql_qio_temp_switch:%d, ql_qio_temperature:%d! \n",ql_qio_001_exec_data->ql_qio_temp_unit, ql_qio_temp_switch,ql_qio_temp_set);
#endif
    Serial.printf("ql_qio_temp_unit:%d, ql_qio_temp_switch:%d, ql_qio_temperature:%d! \n",ql_qio_001_exec_data->ql_qio_temp_unit, ql_qio_temp_switch,ql_qio_temp_set);
    if(ql_qio_temp_switch == QL_QIO_TEMPERATURE_MODULE_ON)
    {
#ifdef QL_PRINT_ENABLE
        Serial.printf("Performing Temperature Control\n");
#endif
      analogReference(EXTERNAL);
      //Serial.begin(9600);// Begin setup
      pinMode(ql_qio_001_exec_data->ql_qio_temp_thermistor_pin,INPUT); //0
      pinMode(ql_qio_001_exec_data->ql_qio_temp_heater_pin, OUTPUT);   
      pinMode(0,OUTPUT); //0
      pinMode(1,OUTPUT); //0
      pinMode(2,OUTPUT); //0
      
      digitalWrite(1, HIGH); 

     Serial.printf("ql_qio_001_exec_data->ql_qio_temp_thermistor_pin:%d\n",ql_qio_001_exec_data->ql_qio_temp_thermistor_pin);
     Serial.printf("ql_qio_001_exec_data->ql_qio_temp_heater_pin:%d\n",ql_qio_001_exec_data->ql_qio_temp_heater_pin);
     Serial.printf("ql_qio_001_exec_data->ql_qio_temp_set:%f\n", *(float *)ql_qio_001_exec_data->ql_qio_temp_set);
      // take N samples in a row, with a slight delay
      for (i=0; i< QL_QIO_TEMP_SAMPLES; i++) {
       ql_qio_001_exec_data->ql_qio_temp_samples[i] = analogRead(ql_qio_001_exec_data->ql_qio_temp_thermistor_pin);
       delay(10);
      }
     
      // average all the samples out
      average = 0;
      for (i=0; i< QL_QIO_TEMP_SAMPLES; i++) {
         average += ql_qio_001_exec_data->ql_qio_temp_samples[i];
      }
      average /= QL_QIO_TEMP_SAMPLES;
//      //Vo=average;
//     
 // Serial.print("Average analog reading "); 
//  Serial.println(average);
 
  // convert the value to resistance
  average = average/1023;
  average = SERIESRESISTOR / average;
  Serial.print("Thermistor resistance "); 
  Serial.println(average);
 
  float steinhart;
  steinhart = average / THERMISTORNOMINAL;     // (R/Ro)
  steinhart = log(steinhart);                  // ln(R/Ro)
  steinhart /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
  steinhart += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
  steinhart = 1.0 / steinhart;                 // Invert
  steinhart -= 177.15;                         // convert to C
 
  Serial.print("Temperature "); 
  Serial.print(steinhart);
  Serial.println(" *C");
  ql_qio_001_exec_data->ql_qio_Tc=steinhart;
  
  
    *(float *)ql_qio_001_exec_data->ql_qio_temp_fb = ql_qio_001_exec_data->ql_qio_Tc;
    if(ql_qio_001_exec_data->ql_qio_Tc > *(float *)ql_qio_001_exec_data->ql_qio_temp_set ) 
     {                     
      digitalWrite(ql_qio_001_exec_data->ql_qio_temp_heater_pin, LOW); // turn off heater
      digitalWrite(2, HIGH);    
      digitalWrite(0, LOW);   
      Serial.println(ql_qio_001_exec_data->ql_qio_Tc);
       
     }
     
    if(ql_qio_001_exec_data->ql_qio_Tc < *(float *)ql_qio_001_exec_data->ql_qio_temp_set ) 
     {                     
      digitalWrite(ql_qio_001_exec_data->ql_qio_temp_heater_pin, HIGH); // turn on heatert 
      digitalWrite(2, LOW); 
      digitalWrite(0, HIGH);   
      Serial.println(ql_qio_001_exec_data->ql_qio_Tc);
     }
     delay(1000);


#ifdef QL_PRINT_ENABLE
       Serial.printf("Completed!\n");
#endif
    }
#ifdef QL_PRINT_ENABLE
    Serial.printf("Code 01 Executed!\n");
#endif
    return QL_STATUS_OK;
}
STATUS ql_qcode_001_config_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_001_config_size)
{
     unsigned int qblock_inputs = 0;
     unsigned int qblock_outputs = 0;
     unsigned int qblock_number = 0;
     unsigned int qblock_parameters = 0;
     unsigned int qblock_inputs_config_size = 0;
     unsigned int qblock_config_param_size = 0;
     QL_QCODE_CONFIG_BLOCK *ql_qcode_config_block = NULL;
     if((!ql_qcode_config) || (!ql_qcode_001_config_size))
           return QL_ARG_ERROR;
     else
           ;
     *ql_qcode_001_config_size = sizeof(QL_IO_TEMP_CTRL_01_CONFIG);
     
     return QL_STATUS_OK;
}



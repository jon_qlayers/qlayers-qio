
/**************************************************************************************************************************************
Copyright - QLAYERS.B.V

This File is a confidential property of QLayers B.V. Any redistribution or use without prior legal contract will be a legal concern. 

slave_temperature.ino - Audrino ide file for implementing temperature control

Revision - Version x.x - Author
***************************************************************************************************************************************/

#include <i2c_t3.h>
#include <SPI.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ql_types.h"
#include "ql_qio_error.h"
#include "ql_types.h"
#include "ql_i2c.h"
#include "ql_qcode.h"
#include "ql_qcode_005.h"
#include "arm_math.h"


extern STATUS ql_qio_initialize_fft_lib(unsigned int fft_size);
extern STATUS ql_qtimer_mem_init (void);
extern STATUS ql_estimate_dominant_signal(float32_t *signal_vector,uint32_t fft_size, float32_t *signal_estimate);
extern void ql_init_i2c_env(void);
extern void requestEvent(void);
extern void ql_qio_init_peripherals(void);
void digitalPotWrite(int address, int value);
extern void ql_qio_parse_exec(void);
extern void ql_i2c_receive_handle(unsigned int numBytes);



void loop() 
{
    Serial.print("Up!\n");
    delay(5000);
}

void setup() 
{
    analogReadResolution(10);
    ql_qio_initialize_fft_lib(QL_QIO_PART_PROCESSING_FFT_SIZE);
    ql_qio_init_peripherals();
    ql_init_audr_env();
    ql_qio_parse_exec();
}

STATUS ql_init_audr_env(void)
{
    STATUS ql_status = 0;
    Serial.begin(9600);
    Serial.print("Initializing QIO Module...\n");
    Wire.begin(QL_QIO_I2C_ADDRESS);                // join i2c bus with address #8
    Wire.onRequest(requestEvent); // register event
    Wire.onReceive(ql_i2c_receive_handle);
    delay(2000);
    ql_init_i2c_env();
    ql_qtimer_mem_init();
    return QL_STATUS_OK;
}


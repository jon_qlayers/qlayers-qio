/**************************************************************************************************************************************
Copyright - QLAYERS.B.V

This File is a confidential property of QLayers B.V. Any redistribution or use without prior legal contract will be a legal concern. 

queue_mgmt.c - This file contains the run time logic of Queues and its management between different threads.

Revision - Version x.x - Author
***************************************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SPI.h>
#include "ql_qio_error.h"
#include "ql_types.h"
#include "ql_qcode.h"
#include "ql_i2c.h"
#include "ql_qcode_001.h"
//#include <i2c_t3.h>
#include <Arduino.h>


static QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit_list = NULL;
static QL_QCODE_BLOCK_OUTPUT_DB *ql_qcode_parse_block_db = NULL;

extern STATUS ql_qcode_004_parse_config(unsigned int *parse_next_block, unsigned int** parse_next_block_add, QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit);
extern STATUS ql_qcode_004_allocate_exec_data_mem(void** ql_qcode_004_data_mem);
extern STATUS  ql_qcode_004_execute_unit(QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit);
extern STATUS ql_qcode_004_config_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_004_config_size);
extern STATUS ql_qcode_001_parse_config(unsigned int *parse_next_block, unsigned int** parse_next_block_add, QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit);
extern STATUS ql_qcode_001_allocate_exec_data_mem(void** ql_qcode_001_data_mem);
extern STATUS  ql_qcode_001_execute_unit(QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit);
extern STATUS ql_qcode_001_config_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_001_config_size);
extern STATUS ql_qcode_002_parse_config(unsigned int *parse_next_block, unsigned int** parse_next_block_add, QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit);
extern STATUS ql_qcode_002_allocate_exec_data_mem(void** ql_qcode_002_data_mem);
extern STATUS  ql_qcode_002_execute_unit(QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit);
extern STATUS ql_qcode_002_config_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_002_config_size);
extern STATUS ql_qcode_003_parse_config(unsigned int *parse_next_block, unsigned int** parse_next_block_add, QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit);
extern STATUS ql_qcode_003_allocate_exec_data_mem(void** ql_qcode_003_data_mem);
extern STATUS  ql_qcode_003_execute_unit(QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit);
extern STATUS ql_qcode_003_config_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_003_config_size);

extern STATUS ql_qcode_005_parse_config(unsigned int *parse_next_block, unsigned int** parse_next_block_add, QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit);
extern STATUS ql_qcode_005_allocate_exec_data_mem(void** ql_qcode_005_data_mem);
extern STATUS  ql_qcode_005_execute_unit(QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit);
extern STATUS ql_qcode_005_config_size_calc(unsigned int* ql_qcode_config, unsigned int* ql_qcode_005_config_size);
extern STATUS ql_qio_005_read_air_flow(float* ql_qio_air_flow);

/**********************Configuration***********************/


void ql_qio_parse_exec(void)
{
    unsigned int* ql_qcodes;
    STATUS status = QL_STATUS_ERROR;
    unsigned int i = 0;
    QL_QCODE_EXEC_UNIT **ql_qcode_execution_lane = NULL;
    Serial.printf("QL Code Test Engine..\n");
     delay(100);    

    
    
    ql_qcodes = (unsigned int *)ql_get_qcode_configuration();
    status = ql_parse_qcode(ql_qcodes);
    if(status != QL_STATUS_OK)
    {
        Serial.printf("QCODE Parse Error :%d!!\n", status);
        return ;
    }
    else
          Serial.printf("QCODE Parsed! Status:%d!!\n", status);
    
    ql_qcode_execution_lane =  ql_get_exec_unit_db();
    Serial.printf("Performing QLAYERS QIO Control...\n");
    //for(i=0;i<1000;i++)
    while(1)
    {

    //digitalWrite(20,LOW);   
    #if 1
       status = ql_execute_exection_lane(*ql_qcode_execution_lane);
       if(status != QL_STATUS_OK)
       {
            printf("Execution Failed!\n");
       }
       else
          ;
     #endif
       delay(5);
       
    }
    

    Serial.printf("Parsed and Test Completed!\n");
    // delay(100);
    return;
}

/**********************************************************************************
*Function:ql_execute_exection_lane
*Inputs:QL_QCODE_EXEC_UNIT* ql_qcode_execution_lane
*Outputs:STATUS
*Functionality: This Function call is used to execute the QCODE execution list.
*
*
*************************************************************************************/

STATUS ql_execute_exection_lane(QL_QCODE_EXEC_UNIT* ql_qcode_execution_lane)
{
    QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit=NULL; /*Current Execution Unit*/
    STATUS status = QL_STATUS_ERROR;             /*Status variable*/
    if((!ql_qcode_execution_lane))
         return QL_ARG_ERROR;
     else
         ;
    ql_qcode_exec_unit = ql_qcode_execution_lane;
    /*Process the list of exec units to be executed*/
    while(ql_qcode_exec_unit)
    {
        /*Select the qcode handler*/
        switch(ql_qcode_exec_unit->ql_qcode_block_type)
        {
             case 1:
                   status = ql_qcode_001_execute_unit(ql_qcode_exec_unit);
                   break;
             case 2:
                   status = ql_qcode_002_execute_unit(ql_qcode_exec_unit);
                   break;
             case 3:
                   status = ql_qcode_003_execute_unit(ql_qcode_exec_unit);
                   break;
             case 4:
                   status = ql_qcode_004_execute_unit(ql_qcode_exec_unit);
                   break;
             case 5:
                   status = ql_qcode_005_execute_unit(ql_qcode_exec_unit);
                   break;
             default:
                   Serial.printf("Unsupported OP Code! Block type %d, Block Number:%d, exec unit:%p!\n", ql_qcode_exec_unit->ql_qcode_block_type, ql_qcode_exec_unit->ql_qcode_block_number, ql_qcode_exec_unit);
                  
                   break;
         }
         /*Change to the next block to be processed*/
         ql_qcode_exec_unit = ql_qcode_exec_unit->ql_qcode_exec_next;
    }
    return QL_STATUS_OK;
}

/*This is currently the simplest implementation! Certainly not good for large lists!! But in a controller code, we dont expect huge number of blocks. Nevertheless, Needs to be improved in future.
*/





STATUS ql_qcode_fetch_io_config_size(QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit, unsigned int* ql_qcode_config, unsigned int *ql_qcode_config_dynm_size)
{
     STATUS status = QL_STATUS_ERROR;
     if((!ql_qcode_exec_unit) || (!ql_qcode_config) || (!ql_qcode_config_dynm_size))
          return QL_ARG_ERROR;
     else
          ; 

     switch(ql_qcode_exec_unit->ql_qcode_block_type)
     {
          case 1:
               status = ql_qcode_001_config_size_calc(ql_qcode_config, ql_qcode_config_dynm_size);
               break;
          case 2:
               status = ql_qcode_002_config_size_calc(ql_qcode_config, ql_qcode_config_dynm_size);
               break;
          case 3:
               status = ql_qcode_003_config_size_calc(ql_qcode_config, ql_qcode_config_dynm_size);
               break;
          case 4:
               status = ql_qcode_004_config_size_calc(ql_qcode_config, ql_qcode_config_dynm_size);
               break;
          case 5:
               status = ql_qcode_005_config_size_calc(ql_qcode_config, ql_qcode_config_dynm_size);
               break;
          //case 5:
              // status = ql_qcode_005_config_dynamic_size_calc(ql_qcode_config, ql_qcode_config_dynm_size);
            //   break;
          case 50:
              // status = ql_qcode_050_config_dynamic_size_calc(ql_qcode_config, ql_qcode_config_dynm_size);
               break;
          default:
                  printf("1Unsupported OP Code %d!\n", ql_qcode_exec_unit->ql_qcode_block_type);
               return status;
     }
     return status;
}

STATUS ql_qcode_allocate_exec_data_mem(QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit)
{

     STATUS status = QL_STATUS_ERROR;
     void* ql_qcode_exec_data_mem = NULL;
     if((!ql_qcode_exec_unit) )
          return QL_ARG_ERROR;
     else
          ; 
    
     switch(ql_qcode_exec_unit->ql_qcode_block_type)
     {
          case 1:
               status = ql_qcode_001_allocate_exec_data_mem(&ql_qcode_exec_data_mem);
               break;
          case 2:
               status = ql_qcode_002_allocate_exec_data_mem(&ql_qcode_exec_data_mem);
               break;
           case 3:
               status = ql_qcode_003_allocate_exec_data_mem(&ql_qcode_exec_data_mem);
               break;         
          case 4:
               status = ql_qcode_004_allocate_exec_data_mem(&ql_qcode_exec_data_mem);
               break;
          case 5:
               status = ql_qcode_005_allocate_exec_data_mem(&ql_qcode_exec_data_mem);
               break;
          default:
                  printf("Unsupported OP Code %d!\n", ql_qcode_exec_unit->ql_qcode_block_type);
               return status;
     }
    
     ql_qcode_exec_unit->ql_qcode_exec_data = (QL_QCODE_EXEC_DATA *)ql_qcode_exec_data_mem;
     return status;
}


STATUS ql_qcode_parse_config_update_db(QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit, unsigned int *parse_next_block, unsigned int** parse_next_block_add)
{
     STATUS status = QL_STATUS_OK;
     QL_QCODE_BLOCK_OUTPUT_DB **ql_qcode_block_db = NULL;
     unsigned int ql_qcode_config_dynm_size = 0;
     unsigned int ql_qio_block_no; //device type
     unsigned int ql_qio_block_type; //functionality
     
     QL_IO_DEVICE_CONFIG_BLK* ql_io_device_config_blk;
     ql_qcode_block_db = ql_get_parse_block_db();

     ql_io_device_config_blk = (QL_IO_DEVICE_CONFIG_BLK*) parse_next_block;
     ql_qio_block_no = ql_io_device_config_blk->ql_qio_block_no;
     ql_qio_block_type = ql_io_device_config_blk->ql_qio_block_type;
     ql_qcode_exec_unit->ql_qcode_block_type = ql_qio_block_type;
     ql_qcode_exec_unit->ql_qcode_block_number = ql_qio_block_no;
     
 
     status = ql_qcode_allocate_exec_data_mem(ql_qcode_exec_unit);
     if(status != QL_STATUS_OK)
           return status;
     else
           ;

     switch(ql_qio_block_type)
     {
         case 1:
              status = ql_qcode_001_parse_config(parse_next_block, parse_next_block_add, ql_qcode_exec_unit);
             break;
         case 2:
              status = ql_qcode_002_parse_config(parse_next_block, parse_next_block_add, ql_qcode_exec_unit);
             break;
         case 3:
              status = ql_qcode_003_parse_config(parse_next_block, parse_next_block_add, ql_qcode_exec_unit);
             break;
         case 4:
              status = ql_qcode_004_parse_config(parse_next_block, parse_next_block_add, ql_qcode_exec_unit);
             break;
         case 5:
              status = ql_qcode_005_parse_config(parse_next_block, parse_next_block_add, ql_qcode_exec_unit);
             break;
         default:
             return -10;
             break;
     }
     return status;
}


STATUS ql_parse_qcode_form_execution_lane(unsigned int *ql_qcodes)
{
    unsigned int total_blocks = 0;
    unsigned int i = 0;
    QL_QCODE_EXEC_UNIT *ql_qcode_exec_unit;
    unsigned int * parse_next_block;
    QL_QCODE_CONFIG_BLOCK *ql_qcode_config_block;
    STATUS status = QL_STATUS_OK;
    QL_QCODE_EXEC_UNIT **ql_qcode_exec_unit_db_ptr;
    QL_QCODE_CONFIGURATION *ql_qcode_config;
 
    ql_qcode_config = (QL_QCODE_CONFIGURATION *) ql_qcodes;
    total_blocks = ql_qcode_config->qcode_total_blocks; 
    ql_qcode_config_block = (QL_QCODE_CONFIG_BLOCK *) &ql_qcode_config->qcode_blocks_start;
    parse_next_block = &ql_qcode_config->qcode_blocks_start;    

    ql_qcode_exec_unit_db_ptr = ql_get_exec_unit_db();
    
    for(i = 0; i < total_blocks; i++)
    {
         
         ql_qcode_config_block = (QL_QCODE_CONFIG_BLOCK *) parse_next_block;
         ql_qcode_exec_unit = (QL_QCODE_EXEC_UNIT *)malloc(sizeof(QL_QCODE_EXEC_UNIT)); // Need to change this later!
          if(!ql_qcode_exec_unit)
              return QL_MEMORY_ALLOC_ERROR;
          else
              ;
          
          memset((void *) ql_qcode_exec_unit, 0x00, sizeof(QL_QCODE_EXEC_UNIT));
          
          status = ql_qcode_insert_block_execution_list(ql_qcode_exec_unit_db_ptr, ql_qcode_exec_unit);
          if(status != QL_STATUS_OK)
               return status;
          else
               ;

          status = ql_qcode_parse_config_update_db(ql_qcode_exec_unit, parse_next_block, &parse_next_block);

          if(status != QL_STATUS_OK)
               return status;
          else
               ;

    }
    
    return QL_STATUS_OK;
}

/*Simplest Version!*/
/*Searches the list and updates the blocks that it needs to be connected to. Also puts it a list that needs to be updated in the future.*/
STATUS ql_qcode_insert_block_execution_list(QL_QCODE_EXEC_UNIT **ql_qcode_exec_unit_list,QL_QCODE_EXEC_UNIT* ql_qcode_exec_unit)
{
     QL_QCODE_EXEC_UNIT *ql_qcode_exec_units_db;
     QL_QCODE_EXEC_UNIT *ql_qcode_exec_new_node;
     if(!ql_qcode_exec_unit_list)
         return QL_ARG_ERROR;
     else
         ;
     ql_qcode_exec_units_db = *ql_qcode_exec_unit_list;
     if(!(*ql_qcode_exec_unit_list))
     {
          //Head Node
       
        *ql_qcode_exec_unit_list = ql_qcode_exec_unit;
     }
     else
     {

        while((ql_qcode_exec_units_db)->ql_qcode_exec_next)
            ql_qcode_exec_units_db = ql_qcode_exec_units_db->ql_qcode_exec_next;
       
         ql_qcode_exec_units_db->ql_qcode_exec_next = ql_qcode_exec_unit;
         
     }
     ql_qcode_exec_unit->ql_qcode_exec_next = NULL;
     return QL_STATUS_OK;
}





STATUS ql_parse_qcode(unsigned int *ql_qcodes)
{
    STATUS status = QL_STATUS_ERROR;
    
    status = ql_parse_qcode_form_execution_lane(ql_qcodes);
    if (status != QL_STATUS_OK)
         return status;
    else
         ;
    
    return QL_STATUS_OK;
}
QL_QCODE_BLOCK_OUTPUT_DB ** ql_get_parse_block_db(void)
{
    return (&ql_qcode_parse_block_db);
}

QL_QCODE_EXEC_UNIT ** ql_get_exec_unit_db(void)
{
    return (&ql_qcode_exec_unit_list);
}

STATUS ql_qcode_parse_search_retrieve_blocks_db (QL_QCODE_BLOCK_OUTPUT_DB **ql_qcode_block_db,  QL_QCODE_EXEC_UNIT** ql_qcode_exec_unit, unsigned int ql_qcode_search_block_number)
{
     QL_QCODE_BLOCK_OUTPUT_DB *ql_qcode_block_db_node = NULL;
     QL_QCODE_BLOCK_OUTPUT_DB *ql_qcode_block_db_node_prev = NULL;

     if((!ql_qcode_exec_unit) || (!ql_qcode_block_db) || (!ql_qcode_exec_unit))
          return QL_ARG_ERROR;
     else
          ;
     *ql_qcode_exec_unit = NULL;
     ql_qcode_block_db_node = *ql_qcode_block_db;
     
     while(ql_qcode_block_db_node)
     {
          
          if(ql_qcode_block_db_node->ql_qcode_block_number == ql_qcode_search_block_number)
          {
               /*Return the exec unit pointer*/
               *ql_qcode_exec_unit = ql_qcode_block_db_node->ql_qcode_exec_unit;
               return QL_STATUS_OK;
          }
          else
              ;
         
          ql_qcode_block_db_node_prev = ql_qcode_block_db_node;        
          ql_qcode_block_db_node = ql_qcode_block_db_node->ql_qcode_block_list_next_node;  
     }

     return QL_PARSE_DB_BLOCK_NOT_FOUND; 
}


